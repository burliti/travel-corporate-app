import { Setor } from './../../configuracoes/setor/setor.model';
import { Perfil } from './../../configuracoes/perfil/perfil.model';
import { Funcao } from './../funcao/funcao.model';
import { Empresa } from '../../configuracoes/empresa/empresa.model';
import { CentroCusto } from '../centro-custo/centro-custo.model';

export class Funcionario {
  id?: string;
  nome?: string;
  administrador?: string;
  resetarSenha?: string;
  status?: string;
  celular?: string;
  email?: string;
  senha?: string;
  confirmacaoSenha?: string;
  perfil?: Perfil;
  saldo?: number;
  setor?: Setor = {};
  filial?: Empresa = {};
  centroCusto?: CentroCusto;
}
