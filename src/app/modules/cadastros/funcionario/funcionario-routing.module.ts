import { FuncionarioCadastroComponent } from './funcionario-cadastro/funcionario-cadastro.component';
import { FuncionarioConsultaComponent } from './funcionario-consulta/funcionario-consulta.component';
import { AutenticacaoGuard } from './../../../security/autenticacao.guard';
import { FuncionarioComponent } from './funcionario.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{
  path: 'cadastros/funcionario', component: FuncionarioComponent, canActivate: [AutenticacaoGuard], children: [
    { path: '', component: FuncionarioConsultaComponent },
    {
      path: ':acao', children: [
        { path: '', component: FuncionarioCadastroComponent },
        { path: ':id', component: FuncionarioCadastroComponent }
      ]
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FuncionarioRoutingModule { }
