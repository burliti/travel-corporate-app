import { BlockUiService } from './../../../../plugins/block-ui/block-ui.service';
import { SelectGroupComponent } from './../../../../plugins/forms/select-group/select-group.component';
import { MensagemService } from './../../../../plugins/forms/mensagem.service';
import { SelectGroupOption } from './../../../../plugins/forms/select-group/select-group-option';
import { Subscription } from 'rxjs/Rx';
import { FuncionarioService } from './../funcionario.service';
import { Funcionario } from './../funcionario.model';
import { Component, OnInit } from '@angular/core';
import { AbstractCadastroComponent } from '../../../../components/geral/crud/abstract-cadastro.component';
import { EnumUtils } from '../../../../plugins/shared';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-funcionario-cadastro',
  templateUrl: './funcionario-cadastro.component.html',
  styleUrls: ['./funcionario-cadastro.component.css']
})
export class FuncionarioCadastroComponent extends AbstractCadastroComponent<Funcionario, FuncionarioService> {

  inscricoes: Subscription[] = [];

  // Colecoes
  itensStatus: SelectGroupOption[] = EnumUtils.getOptionsAtivo();
  itensSimNao: SelectGroupOption[] = EnumUtils.getOptionsSimNao();
  itensPerfil: SelectGroupOption[] = [];
  itensSetor: SelectGroupOption[] = [];
  itensFilial: SelectGroupOption[] = [];
  itensCentroCusto: SelectGroupOption[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private blockService: BlockUiService,
    private funcionarioService: FuncionarioService,
    private msg: MensagemService) {
    super(route, router, blockService, msg);
  }

  ngOnInit() {
    const s1 = this.funcionarioService.getDados().subscribe((retorno) => {
      this.itensPerfil.push({ label: 'Selecione um perfil', value: '' });
      this.itensSetor.push({ label: 'Selecione o setor', value: '' });
      this.itensFilial.push({ label: '[Selecione a filial]', value: '' });
      this.itensCentroCusto.push({ label: '[Selecione o centro de custo]', value: '' });

      retorno.perfis.forEach((perfil) => this.itensPerfil.push({ label: perfil.nome, value: perfil.id }));
      retorno.setores.forEach((setor) => this.itensSetor.push({ label: setor.nome, value: setor.id }));
      retorno.empresas.forEach((filial) => this.itensFilial.push({ label: filial.nome, value: filial.id }));
      retorno.centrosCusto.forEach((centroCusto) => this.itensCentroCusto.push({ label: centroCusto.codigo + ' - ' + centroCusto.nome, value: centroCusto.id.toString() }));

      super.ngOnInit();
    }, (error) => {
      console.error(error);
    });

    this.inscricoes.push(s1);
  }

  beforeVisualizar(vo: Funcionario) {
    // vo.confirmacaoSenha = vo.senha;
    vo.senha = '';

    if (!vo.setor) {
      vo.setor = {};
    }

    if (!vo.perfil) {
      vo.perfil = {};
    }

    if (!vo.filial) {
      vo.filial = {};
    }

    if (!vo.centroCusto) {
      vo.centroCusto = {};
    }
  }

  getService(): FuncionarioService {
    return this.funcionarioService;
  }

  getVoNewInstance(): Funcionario {
    return {
      status: 'Ativo',
      administrador: 'Não',
      resetarSenha: 'Não',
      perfil: {},
      setor: {},
      filial: {},
      centroCusto: {}
    };
  }
}
