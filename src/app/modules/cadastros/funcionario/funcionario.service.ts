import { CentroCusto } from './../centro-custo/centro-custo.model';
import { Empresa } from './../../configuracoes/empresa/empresa.model';
import { Funcao } from './../funcao/funcao.model';
import { Perfil } from './../../configuracoes/perfil/perfil.model';
import { Observable } from 'rxjs';
import { ConsultaResponse } from './../../../components/model/consulta-response.model';
import { Funcionario } from './funcionario.model';
import { Injectable, Inject } from '@angular/core';
import { AbstractCrudService } from '../../../components/services/abstract-crud.service';
import { Http } from '@angular/http';
import { LoginService } from '../../../security/login.service';
import { Setor } from './../../configuracoes/setor/setor.model';
import { SessaoUsuario } from '../../../security/sessao.model';
import { ExportacaoDadosFiltro } from '../../relatorios/exportacao-dados/exportacao-dados-filtro.model';

@Injectable()
export class FuncionarioService extends AbstractCrudService<Funcionario> {

  constructor( @Inject(Http) _http: Http, @Inject(LoginService) loginService: LoginService) {
    super(_http, loginService);
  }

  getEntityName(): string {
    return "funcionario";
  }

  getListAtivos(): Observable<ConsultaResponse<Funcionario>> {
    return this.getList({
      filtro: { "ativo": "Ativo" },
      order: { "nome": "asc" },
      pageSize: null,
      pageNumber: null
    });
  }

  getDados(): Observable<ExportacaoDadosFiltro> {
    return this._http.post(this.getUrl() + '/dados',
      null,
      { headers: this.getHeaders() })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  // getPerfis(): Observable<Perfil[]> {
  //   return this._http.post(this.getUrl() + '/perfis',
  //     null,
  //     { headers: this.getHeaders() })
  //     .map(res => res.json())
  //     .catch((error: any) => Observable.throw(error || 'Server error'));
  // }

  // getFuncoes(): Observable<Funcao[]> {
  //   return this._http.post(this.getUrl() + '/funcoes',
  //     null,
  //     { headers: this.getHeaders() })
  //     .map(res => res.json())
  //     .catch((error: any) => Observable.throw(error || 'Server error'));
  // }

  // getSetores(): Observable<Setor[]> {
  //   return this._http.post(this.getUrl() + '/setores',
  //     null,
  //     { headers: this.getHeaders() })
  //     .map(res => res.json())
  //     .catch((error: any) => Observable.throw(error || 'Server error'));
  // }

  // getFiliais(): Observable<Empresa[]> {
  //   return this._http.post(this.getUrl() + '/filiais',
  //     null,
  //     { headers: this.getHeaders() })
  //     .map(res => res.json())
  //     .catch((error: any) => Observable.throw(error || 'Server error'));
  // }

  // getCentrosCusto(): Observable<CentroCusto[]> {
  //   return this._http.post(this.getUrl() + '/centroCusto',
  //     null,
  //     { headers: this.getHeaders() })
  //     .map(res => res.json())
  //     .catch((error: any) => Observable.throw(error || 'Server error'));
  // }

}
