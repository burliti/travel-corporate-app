import { BlockUiService } from './../../../../plugins/block-ui/block-ui.service';
import { DataPaginationEvent } from './../../../../plugins/plugins.api';
import { FuncionarioService } from './../funcionario.service';
import { MensagemService } from './../../../../plugins/forms/mensagem.service';
import { ActivatedRoute, Router } from '@angular/router';
import { EnumUtils } from './../../../../plugins/shared';
import { Subscription } from 'rxjs/Rx';
import { DataTableComponent } from './../../../../plugins/data-table/data-table.component';
import { ConsultaResponse } from './../../../../components/model/consulta-response.model';
import { Funcionario } from './../funcionario.model';
import { SelectGroupOption } from './../../../../plugins/forms/select-group/select-group-option';
import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-funcionario-consulta',
  templateUrl: './funcionario-consulta.component.html',
  styleUrls: ['./funcionario-consulta.component.css']
})
export class FuncionarioConsultaComponent implements OnDestroy, OnInit, AfterViewInit {

  // Colecoes
  itensStatus: SelectGroupOption[] = EnumUtils.getOptionsAtivo("TODOS");
  itensPerfil: SelectGroupOption[] = [];
  itensSetor: SelectGroupOption[] = [];
  itensFilial: SelectGroupOption[] = [];
  itensCentroCusto: SelectGroupOption[] = [];

  dados: ConsultaResponse<Funcionario>;

  filtro: any = {
    "ativo": ""
  };
  order: any = { "nome": "asc" };

  private inscricoes: Subscription[] = [];

  // Recupera o componente dataTable
  @ViewChild(DataTableComponent) dt: DataTableComponent<Funcionario>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private msg: MensagemService,
    private blockService: BlockUiService,
    private funcionarioService: FuncionarioService) { }

  ngOnInit() {
    const s1 = this.funcionarioService.getDados().subscribe((retorno) => {
      this.itensPerfil.push({ label: 'Selecione um perfil', value: '' });
      this.itensSetor.push({ label: 'Selecione o setor', value: '' });
      this.itensFilial.push({ label: '[Selecione a filial]', value: '' });
      this.itensCentroCusto.push({ label: '[Selecione o centro de custo]', value: '' });

      retorno.perfis.forEach((perfil) => this.itensPerfil.push({ label: perfil.nome, value: perfil.id }));
      retorno.setores.forEach((setor) => this.itensSetor.push({ label: setor.nome, value: setor.id }));
      retorno.empresas.forEach((filial) => this.itensFilial.push({ label: filial.nome, value: filial.id }));
      retorno.centrosCusto.forEach((centroCusto) => this.itensCentroCusto.push({ label: centroCusto.codigo + ' - ' + centroCusto.nome, value: centroCusto.id.toString() }));
    }, (error) => {
      console.error(error);
    });

    this.inscricoes.push(s1);
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
    for (let s of this.inscricoes) {
      s.unsubscribe();
    }
  }

  novo() {
    this.router.navigate(['novo'], { relativeTo: this.route });
  }

  editar() {
    if (this.dt.getSelectedItem()) {
      this.router.navigate(['editar', this.dt.getSelectedItem().id], { relativeTo: this.route });
    } else {
      this.msg.nofiticationWarning('Nenhum item selecionado para alterar!');
    }
  }

  excluir() {
    if (this.dt.getSelectedItem()) {
      this.router.navigate(['excluir', this.dt.getSelectedItem().id], { relativeTo: this.route });
    } else {
      this.msg.nofiticationWarning('Nenhum item selecionado para excluir!');
    }
  }

  pesquisar() {
    this.dt.load(1);
  }

  limpar() {
    this.filtro = {
      "ativo": ""
    };
    this.pesquisar();
  }

  paginacao(event: DataPaginationEvent) {
    this.blockService.block('.table-consulta');

    let s = this.funcionarioService.getList({
      filtro: this.filtro,
      order: this.order,
      pageSize: event.pageSize,
      pageNumber: event.page
    }).subscribe((dados) => {
      this.dados = dados;

      this.blockService.unBlock('.table-consulta');
    }, (error) => {
      this.dados = {
        lista: [],
        total: 0
      };

      this.blockService.unBlock('.table-consulta');
    });
  }
}
