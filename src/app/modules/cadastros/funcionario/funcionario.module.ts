import { FuncaoModule } from './../funcao/funcao.module';
import { FuncionarioService } from './funcionario.service';
import { FormsModule } from '@angular/forms';
import { FuncionarioRoutingModule } from './funcionario-routing.module';
import { FuncionarioConsultaComponent } from './funcionario-consulta/funcionario-consulta.component';
import { FuncionarioCadastroComponent } from './funcionario-cadastro/funcionario-cadastro.component';

import { PluginsModule } from './../../../plugins/plugins.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FuncionarioComponent } from './funcionario.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PluginsModule,
    FuncionarioRoutingModule,
    FuncaoModule
  ],
  exports: [],
  declarations: [FuncionarioComponent, FuncionarioCadastroComponent, FuncionarioConsultaComponent],
  providers: [FuncionarioService],
})
export class FuncionarioModule { }
