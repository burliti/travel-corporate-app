import { BlockUiService } from './../../../../plugins/block-ui/block-ui.service';
import { PoliticaService } from './../politica.service';
import { Politica } from './../politica.model';
import { MensagemService } from './../../../../plugins/forms/mensagem.service';
import { SelectGroupOption } from './../../../../plugins/forms/select-group/select-group-option';
import { Subscription } from 'rxjs/Rx';
import { Component, OnInit } from '@angular/core';
import { AbstractCadastroComponent } from '../../../../components/geral/crud/abstract-cadastro.component';
import { EnumUtils } from '../../../../plugins/shared';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-politica-cadastro',
  templateUrl: './politica-cadastro.component.html',
  styleUrls: ['./politica-cadastro.component.css']
})
export class PoliticaCadastroComponent extends AbstractCadastroComponent<Politica, PoliticaService> {

  inscricoes: Subscription[] = [];

  // Colecoes
  itensStatus: SelectGroupOption[] = EnumUtils.getOptionsAtivo();
  itensSimNao: SelectGroupOption[] = EnumUtils.getOptionsSimNao();
  itensTipoLancamento: SelectGroupOption[] = EnumUtils.getOptionsTipoLancamento("Selecione uma opção");

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private blockService: BlockUiService,
    private politicaService: PoliticaService,
    private msg: MensagemService) {
    super(route, router, blockService, msg);
  }

  getService(): PoliticaService {
    return this.politicaService;
  }

  getVoNewInstance(): Politica {
    return {
      status: 'Ativo',
    };
  }
}
