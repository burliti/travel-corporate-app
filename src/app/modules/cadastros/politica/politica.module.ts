import { PoliticaService } from './politica.service';
import { PluginsModule } from './../../../plugins/plugins.module';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PoliticaRoutingModule } from './politica-routing.module';
import { PoliticaComponent } from './politica.component';
import { PoliticaCadastroComponent } from './politica-cadastro/politica-cadastro.component';
import { PoliticaConsultaComponent } from './politica-consulta/politica-consulta.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PluginsModule,
    PoliticaRoutingModule
  ],
  providers : [PoliticaService],
  declarations: [PoliticaComponent, PoliticaCadastroComponent, PoliticaConsultaComponent]
})
export class PoliticaModule { }
