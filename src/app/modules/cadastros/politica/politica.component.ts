import { TituloPaginaService } from './../../../components/services/titulo-pagina.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-politica',
  templateUrl: './politica.component.html',
  styleUrls: ['./politica.component.css']
})
export class PoliticaComponent implements OnInit {

  constructor(private tituloPaginaService: TituloPaginaService) { }

  ngOnInit() {
    this.tituloPaginaService.setTitulo("Políticas de viagem");
  }

}
