import { PoliticaCadastroComponent } from './politica-cadastro/politica-cadastro.component';
import { PoliticaConsultaComponent } from './politica-consulta/politica-consulta.component';
import { AutenticacaoGuard } from './../../../security/autenticacao.guard';
import { PoliticaComponent } from './politica.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{
  path: 'cadastros/politica', component: PoliticaComponent, canActivate: [AutenticacaoGuard], children: [
    { path: '', component: PoliticaConsultaComponent },
    {
      path: ':acao', children: [
        { path: '', component: PoliticaCadastroComponent },
        { path: ':id', component: PoliticaCadastroComponent }
      ]
    }
  ]
}];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PoliticaRoutingModule { }
