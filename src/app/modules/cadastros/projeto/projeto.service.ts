import { CentroCusto } from './../centro-custo/centro-custo.model';
import { Empresa } from './../../configuracoes/empresa/empresa.model';
import { Projeto } from './projeto.model';
import { Observable } from 'rxjs';
import { ConsultaResponse } from './../../../components/model/consulta-response.model';
import { Injectable, Inject } from '@angular/core';
import { AbstractCrudService } from '../../../components/services/abstract-crud.service';
import { Http } from '@angular/http';
import { LoginService } from '../../../security/login.service';


@Injectable()
export class ProjetoService extends AbstractCrudService<Projeto> {

  constructor( @Inject(Http) _http: Http, @Inject(LoginService) loginService: LoginService) {
    super(_http, loginService);
  }

  getEntityName(): string {
    return "projeto";
  }

  getListAtivos(): Observable<ConsultaResponse<Projeto>> {
    return this.getList({
      filtro: { "status": "Ativo" },
      order: { "nome": "asc" },
      pageSize: null,
      pageNumber: null
    });
  }

  getFiliais(): Observable<Empresa[]> {
    return this._http.post(this.getUrl() + '/filiais',
      null,
      { headers: this.getHeaders() })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  getCentrosCusto(): Observable<CentroCusto[]> {
    return this._http.post(this.getUrl() + '/centroCusto',
      null,
      { headers: this.getHeaders() })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }
}
