import { ProjetoCadastroComponent } from './projeto-cadastro/projeto-cadastro.component';
import { ProjetoConsultaComponent } from './projeto-consulta/projeto-consulta.component';
import { AutenticacaoGuard } from './../../../security/autenticacao.guard';
import { ProjetoComponent } from './projeto.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{
  path: 'cadastros/projeto', component: ProjetoComponent, canActivate: [AutenticacaoGuard], children: [
    { path: '', component: ProjetoConsultaComponent },
    {
      path: ':acao', children: [
        { path: '', component: ProjetoCadastroComponent },
        { path: ':id', component: ProjetoCadastroComponent }
      ]
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjetoRoutingModule { }
