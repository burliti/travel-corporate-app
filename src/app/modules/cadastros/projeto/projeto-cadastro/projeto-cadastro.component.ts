import { BlockUiService } from './../../../../plugins/block-ui/block-ui.service';
import { IndexComponent } from './../../../geral/index/index.component';
import { Cliente } from './../../cliente/cliente.model';
import { ClienteService } from './../../cliente/cliente.service';
import { ProjetoService } from './../projeto.service';
import { Projeto } from './../projeto.model';
import { MensagemService } from './../../../../plugins/forms/mensagem.service';
import { SelectGroupOption } from './../../../../plugins/forms/select-group/select-group-option';
import { Subscription } from 'rxjs/Rx';
import { Component, OnInit } from '@angular/core';
import { AbstractCadastroComponent } from '../../../../components/geral/crud/abstract-cadastro.component';
import { EnumUtils } from '../../../../plugins/shared';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-projeto-cadastro',
  templateUrl: './projeto-cadastro.component.html',
  styleUrls: ['./projeto-cadastro.component.css']
})
export class ProjetoCadastroComponent extends AbstractCadastroComponent<Projeto, ProjetoService> {

  inscricoes: Subscription[] = [];

  // Colecoes
  itensStatus: SelectGroupOption[] = EnumUtils.getOptionsAtivo();
  itensSimNao: SelectGroupOption[] = EnumUtils.getOptionsSimNao();
  itensCentroCusto: SelectGroupOption[] = [];
  clientes: Cliente[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private blockService: BlockUiService,
    private projetoService: ProjetoService,
    private clienteService: ClienteService,
    private msg: MensagemService) {
    super(route, router, blockService, msg);
  }

  ngOnInit() {
    this.itensCentroCusto.push({ label: '[Selecione o centro de custo]', value: '' });

    const s1 = this.clienteService.getListAtivos().subscribe((retorno) => {
      this.clientes = retorno.lista;

      const s2 = this.getService().getCentrosCusto().subscribe((retorno1) => {

        retorno1.forEach((c) => this.itensCentroCusto.push({ label: c.codigo + ' - ' + c.nome, value: c.id.toString() }));

        super.ngOnInit();
      });

      this.inscricoes.push(s2);

    });

    this.inscricoes.push(s1);
  }

  beforeVisualizar(vo: Projeto) {
    if (!vo.centroCusto) {
      vo.centroCusto = {};
    }
  }

  adicionarCliente(cliente: Cliente) {
    if (!this.isExcluir()) {
      this.vo.clientes.push(cliente);
    }
  }

  removerCliente(cliente: Cliente) {
    if (!this.isExcluir()) {
      var index = this.getClienteIndex(this.vo.clientes, cliente); //this.vo.clientes.indexOf(cliente, 0);
      if (index > -1) {
        this.vo.clientes.splice(index, 1);
      }
    }
  }

  getClientesNaoSelecionados() {
    if (this.clientes != null) {
      return this.clientes.filter(cliente => {
        var index = this.getClienteIndex(this.vo.clientes, cliente); //this.vo.clientes.indexOf(cliente, 0);
        if (index < 0) {
          return cliente;
        }
      });
    }
    return null;
  }

  getClienteIndex(clientes: Cliente[], cliente: Cliente) {
    let i = -1;
    for (let c of clientes) {
      i++;
      if (c.id === cliente.id) {
        return i;
      }
    }
    return -1;
  }

  getService(): ProjetoService {
    return this.projetoService;
  }

  getVoNewInstance(): Projeto {
    return {
      status: 'Ativo',
      centroCusto: {},
      clientes: []
    };
  }
}
