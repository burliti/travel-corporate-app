import { TituloPaginaService } from './../../../components/services/titulo-pagina.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-projeto',
  templateUrl: './projeto.component.html',
  styleUrls: ['./projeto.component.css']
})
export class ProjetoComponent implements OnInit {

  constructor(private tituloPaginaService: TituloPaginaService) { }

  ngOnInit() {
    this.tituloPaginaService.setTitulo("Projetos");
  }

}
