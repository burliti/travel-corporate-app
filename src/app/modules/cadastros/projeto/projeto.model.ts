import { CentroCusto } from './../centro-custo/centro-custo.model';
import { Empresa } from './../../configuracoes/empresa/empresa.model';
import { Cliente } from './../cliente/cliente.model';

export class Projeto {
  id?: string;
  nome?: string;
  status?: string;
  valor?: number;
  percentualAlertaGastos?: number;
  percentualLimiteGastos?: number;
  clientes?: Cliente[] = [];
  filial?: Empresa;
  centroCusto?: CentroCusto;
}
