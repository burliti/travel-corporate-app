import { ClienteModule } from './../cliente/cliente.module';
import { ProjetoService } from './projeto.service';
import { PluginsModule } from './../../../plugins/plugins.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjetoRoutingModule } from './projeto-routing.module';
import { ProjetoComponent } from './projeto.component';
import { ProjetoCadastroComponent } from './projeto-cadastro/projeto-cadastro.component';
import { ProjetoConsultaComponent } from './projeto-consulta/projeto-consulta.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    PluginsModule,
    FormsModule,
    ProjetoRoutingModule,
    ClienteModule
  ],
  providers: [ProjetoService],
  declarations: [ProjetoComponent, ProjetoCadastroComponent, ProjetoConsultaComponent]
})
export class ProjetoModule { }
