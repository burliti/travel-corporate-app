import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FornecedorRoutingModule } from './fornecedor-routing.module';
import { FornecedorComponent } from './fornecedor.component';
import { FornecedorCadastroComponent } from './fornecedor-cadastro/fornecedor-cadastro.component';
import { FornecedorConsultaComponent } from './fornecedor-consulta/fornecedor-consulta.component';

@NgModule({
  imports: [
    CommonModule,
    FornecedorRoutingModule
  ],
  declarations: [FornecedorComponent, FornecedorCadastroComponent, FornecedorConsultaComponent]
})
export class FornecedorModule { }
