import { ClienteService } from './cliente.service';
import { PluginsModule } from './../../../plugins/plugins.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClienteRoutingModule } from './cliente-routing.module';
import { ClienteComponent } from './cliente.component';
import { ClienteCadastroComponent } from './cliente-cadastro/cliente-cadastro.component';
import { ClienteConsultaComponent } from './cliente-consulta/cliente-consulta.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    PluginsModule,
    FormsModule,
    ClienteRoutingModule
  ],
  providers: [ClienteService],
  declarations: [ClienteComponent, ClienteCadastroComponent, ClienteConsultaComponent]
})
export class ClienteModule { }
