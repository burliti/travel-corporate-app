import { TituloPaginaService } from './../../../components/services/titulo-pagina.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit {

  constructor(private tituloPaginaService : TituloPaginaService) { }

  ngOnInit() {
    this.tituloPaginaService.setTitulo("Clientes");
  }

}
