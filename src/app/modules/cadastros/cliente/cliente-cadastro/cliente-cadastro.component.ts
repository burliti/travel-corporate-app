import { BlockUiService } from './../../../../plugins/block-ui/block-ui.service';
import { Projeto } from './../../projeto/projeto.model';
import { ClienteService } from './../cliente.service';
import { Cliente } from './../cliente.model';
import { MensagemService } from './../../../../plugins/forms/mensagem.service';
import { SelectGroupOption } from './../../../../plugins/forms/select-group/select-group-option';
import { Subscription } from 'rxjs/Rx';
import { Component, OnInit } from '@angular/core';
import { AbstractCadastroComponent } from '../../../../components/geral/crud/abstract-cadastro.component';
import { EnumUtils } from '../../../../plugins/shared';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-cliente-cadastro',
  templateUrl: './cliente-cadastro.component.html',
  styleUrls: ['./cliente-cadastro.component.css']
})
export class ClienteCadastroComponent extends AbstractCadastroComponent<Cliente, ClienteService> {

  inscricoes: Subscription[] = [];
  itensCentroCusto: SelectGroupOption[] = [];

  // Colecoes
  itensStatus: SelectGroupOption[] = EnumUtils.getOptionsAtivo();
  itensSimNao: SelectGroupOption[] = EnumUtils.getOptionsSimNao();
  itensFuncao: SelectGroupOption[] = [];
  projetos: Projeto[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private blockService: BlockUiService,
    private clienteService: ClienteService,
    private msg: MensagemService) {
    super(route, router, blockService, msg);
  }

  ngOnInit() {
    this.itensCentroCusto.push({ label: '[NENHUM]', value: '' });

    let s2 = this.getService()
      .getCentrosCusto()
      .subscribe((response) => {
        response.forEach((item) => this.itensCentroCusto.push({ label: item.codigo + ' - ' + item.nome, value: item.id.toString() }))

        super.ngOnInit();
      });

    this.inscricoes.push(s2);
  }

  beforeVisualizar(vo: Cliente) {
    if (!vo.centroCusto) {
      vo.centroCusto = {};
    }
  }

  getService(): ClienteService {
    return this.clienteService;
  }

  getVoNewInstance(): Cliente {
    return {
      status: 'Ativo',
      centroCusto: {}
    };
  }
}
