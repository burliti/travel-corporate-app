import { Projeto } from './../projeto/projeto.model';
import { CentroCusto } from '../centro-custo/centro-custo.model';

export class Cliente {
  id?: string;
  nome?: string;
  status?: string;
  cidade?: string;
  uf?: string;
  observacoes?: string;
  centroCusto?: CentroCusto;
}
