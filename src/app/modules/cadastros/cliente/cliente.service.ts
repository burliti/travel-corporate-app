import { CentroCusto } from './../centro-custo/centro-custo.model';
import { Projeto } from './../projeto/projeto.model';
import { Cliente } from './cliente.model';
import { Observable } from 'rxjs';
import { ConsultaResponse } from './../../../components/model/consulta-response.model';
import { Injectable, Inject } from '@angular/core';
import { AbstractCrudService } from '../../../components/services/abstract-crud.service';
import { Http } from '@angular/http';
import { LoginService } from '../../../security/login.service';


@Injectable()
export class ClienteService extends AbstractCrudService<Cliente> {

  constructor( @Inject(Http) _http: Http, @Inject(LoginService) loginService: LoginService) {
    super(_http, loginService);
  }

  getEntityName(): string {
    return "cliente";
  }

  getListAtivos(): Observable<ConsultaResponse<Cliente>> {
    return this.getList({
      filtro: { "status": "Ativo" },
      order: { "nome": "asc" },
      pageSize: null,
      pageNumber: null
    });
  }

  getProjetos() : Observable<Projeto[]> {
    return this._http.post(this.getUrl() + '/projetos',
    null,
    { headers: this.getHeaders() })
    .map(res => res.json())
    .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  getCentrosCusto(): Observable<CentroCusto[]> {
    return this._http.post(this.getUrl() + '/centroCusto',
      null,
      { headers: this.getHeaders() })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }
}
