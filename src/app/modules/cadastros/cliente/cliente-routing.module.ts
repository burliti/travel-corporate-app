import { ClienteCadastroComponent } from './cliente-cadastro/cliente-cadastro.component';
import { ClienteConsultaComponent } from './cliente-consulta/cliente-consulta.component';
import { ClienteComponent } from './cliente.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AutenticacaoGuard } from '../../../security/autenticacao.guard';

const routes: Routes = [{
  path: 'cadastros/cliente', component: ClienteComponent, canActivate: [AutenticacaoGuard], children: [
    { path: '', component: ClienteConsultaComponent },
    {
      path: ':acao', children: [
        { path: '', component: ClienteCadastroComponent },
        { path: ':id', component: ClienteCadastroComponent }
      ]
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClienteRoutingModule { }
