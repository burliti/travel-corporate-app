import { LoginService } from './../../../security/login.service';
import { Http } from '@angular/http';
import { Injectable, Inject } from '@angular/core';
import { AbstractCrudService } from '../../../components/services/abstract-crud.service';
import { CentroCusto } from './centro-custo.model';
@Injectable()
export class CentroCustoService extends AbstractCrudService<CentroCusto> {

  constructor( @Inject(Http) _http: Http, @Inject(LoginService) loginService: LoginService) {
    super(_http, loginService);
  }

  getEntityName(): string {
    return "centroCusto";
  }

}
