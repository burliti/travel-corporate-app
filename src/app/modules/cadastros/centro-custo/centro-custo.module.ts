import { CentroCustoService } from './centro-custo.service';
import { PluginsModule } from './../../../plugins/plugins.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CentroCustoRoutingModule } from './centro-custo-routing.module';
import { CentroCustoComponent } from './centro-custo.component';
import { CentroCustoCadastroComponent } from './centro-custo-cadastro/centro-custo-cadastro.component';
import { CentroCustoConsultaComponent } from './centro-custo-consulta/centro-custo-consulta.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PluginsModule,
    CentroCustoRoutingModule
  ],
  providers :[CentroCustoService],
  declarations: [CentroCustoComponent, CentroCustoCadastroComponent, CentroCustoConsultaComponent]
})
export class CentroCustoModule { }
