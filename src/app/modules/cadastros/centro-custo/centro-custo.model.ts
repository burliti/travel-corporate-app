export class CentroCusto {
  id?: number;
  codigo?: string;
  nome?: string;
  status?: string;
  pai?: CentroCusto;
}
