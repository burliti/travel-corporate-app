import { TituloPaginaService } from './../../../components/services/titulo-pagina.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-centro-custo',
  templateUrl: './centro-custo.component.html',
  styleUrls: ['./centro-custo.component.scss']
})
export class CentroCustoComponent implements OnInit {

  constructor(private tituloPaginaService: TituloPaginaService) { }

  ngOnInit() {
    this.tituloPaginaService.setTitulo("Centro de Custo");
  }

}
