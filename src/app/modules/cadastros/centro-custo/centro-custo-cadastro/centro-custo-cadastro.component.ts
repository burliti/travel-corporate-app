import { BlockUiService } from './../../../../plugins/block-ui/block-ui.service';
import { SelectGroupComponent } from './../../../../plugins/forms/select-group/select-group.component';
import { MensagemService } from './../../../../plugins/forms/mensagem.service';
import { SelectGroupOption } from './../../../../plugins/forms/select-group/select-group-option';
import { Subscription } from 'rxjs/Rx';
import { Component, OnInit } from '@angular/core';
import { AbstractCadastroComponent } from '../../../../components/geral/crud/abstract-cadastro.component';
import { EnumUtils } from '../../../../plugins/shared';
import { ActivatedRoute, Router } from '@angular/router';
import { CentroCusto } from '../centro-custo.model';
import { CentroCustoService } from '../centro-custo.service';

@Component({
  selector: 'app-centro-custo-cadastro',
  templateUrl: './centro-custo-cadastro.component.html',
  styleUrls: ['./centro-custo-cadastro.component.scss']
})
export class CentroCustoCadastroComponent extends AbstractCadastroComponent<CentroCusto, CentroCustoService> {

  inscricoes: Subscription[] = [];

  // Colecoes
  itensStatus: SelectGroupOption[] = EnumUtils.getOptionsAtivo();
  itensCentroCusto: SelectGroupOption[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private blockService: BlockUiService,
    private centroCustoService: CentroCustoService,
    private msg: MensagemService) {
    super(route, router, blockService, msg);
  }

  ngOnInit() {
    super.ngOnInit();

    this.itensCentroCusto.push({ label: 'NENHUM', value : '' });

    let s = this.getService().getList({
      filtro: { "status": "ativo" },
      order: { "codigo": "ASC" }
    }).subscribe((response) => {
      if (response && response.lista) {
        // Adiciona todos os itens à lista, exceto ele mesmo.
        response.lista.forEach((item) => item.id === this.vo.id ? null : this.itensCentroCusto.push({ label: item.codigo + ' - ' + item.nome, value: item.id.toString() }));
      }
    });

    this.inscricoes.push(s);
  }

  beforeVisualizar(vo: CentroCusto) {
    if (!vo.pai) {
      vo.pai = {};
    }
  }

  getService(): CentroCustoService {
    return this.centroCustoService;
  }

  getVoNewInstance(): CentroCusto {
    return {
      status: 'Ativo',
      pai: {}
    };
  }
}
