import { CentroCustoService } from './../centro-custo.service';
import { BlockUiService } from './../../../../plugins/block-ui/block-ui.service';
import { DataPaginationEvent } from './../../../../plugins/plugins.api';
import { MensagemService } from './../../../../plugins/forms/mensagem.service';
import { ActivatedRoute, Router } from '@angular/router';
import { EnumUtils } from './../../../../plugins/shared';
import { Subscription } from 'rxjs/Rx';
import { DataTableComponent } from './../../../../plugins/data-table/data-table.component';
import { ConsultaResponse } from './../../../../components/model/consulta-response.model';
import { SelectGroupOption } from './../../../../plugins/forms/select-group/select-group-option';
import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild } from '@angular/core';
import { CentroCusto } from '../centro-custo.model';

@Component({
  selector: 'app-centro-custo-consulta',
  templateUrl: './centro-custo-consulta.component.html',
  styleUrls: ['./centro-custo-consulta.component.scss']
})
export class CentroCustoConsultaComponent implements OnDestroy, AfterViewInit {

  // Colecoes
  itensStatus: SelectGroupOption[] = EnumUtils.getOptionsAtivo("TODOS");

  dados: ConsultaResponse<CentroCusto>;

  filtro: any = {
    "status": "Ativo"
  };
  order: any = { "codigo": "asc" };

  private inscricoes: Subscription[] = [];

  // Recupera o componente dataTable
  @ViewChild(DataTableComponent) dt: DataTableComponent<CentroCusto>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private msg: MensagemService,
    private blockService: BlockUiService,
    private centroCustoService: CentroCustoService) { }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
    for (let s of this.inscricoes) {
      s.unsubscribe();
    }
  }

  novo() {
    this.router.navigate(['novo'], { relativeTo: this.route });
  }

  editar() {
    if (this.dt.getSelectedItem()) {
      this.router.navigate(['editar', this.dt.getSelectedItem().id], { relativeTo: this.route });
    } else {
      this.msg.nofiticationWarning('Nenhum item selecionado para alterar!');
    }
  }

  excluir() {
    if (this.dt.getSelectedItem()) {
      this.router.navigate(['excluir', this.dt.getSelectedItem().id], { relativeTo: this.route });
    } else {
      this.msg.nofiticationWarning('Nenhum item selecionado para excluir!');
    }
  }

  pesquisar() {
    this.dt.load(1);
  }

  limpar() {
    this.filtro = {
      "status": "Ativo"
    };
    this.pesquisar();
  }

  paginacao(event: DataPaginationEvent) {
    this.blockService.block('.table-consulta');

    let s = this.centroCustoService.getList({
      filtro: this.filtro,
      order: this.order,
      pageSize: event.pageSize,
      pageNumber: event.page
    }).subscribe((dados) => {
      this.dados = dados;

      this.blockService.unBlock('.table-consulta');
    }, (error) => {
      this.dados = {
        lista: [],
        total: 0
      };

      this.blockService.unBlock('.table-consulta');
    });
  }

}

