import { CentroCustoCadastroComponent } from './centro-custo-cadastro/centro-custo-cadastro.component';
import { CentroCustoConsultaComponent } from './centro-custo-consulta/centro-custo-consulta.component';
import { AutenticacaoGuard } from './../../../security/autenticacao.guard';
import { CentroCustoComponent } from './centro-custo.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{
  path: 'cadastros/centro-custo', component: CentroCustoComponent, canActivate: [AutenticacaoGuard], children: [
    { path: '', component: CentroCustoConsultaComponent },
    {
      path: ':acao', children: [
        { path: '', component: CentroCustoCadastroComponent },
        { path: ':id', component: CentroCustoCadastroComponent }
      ]
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CentroCustoRoutingModule { }
