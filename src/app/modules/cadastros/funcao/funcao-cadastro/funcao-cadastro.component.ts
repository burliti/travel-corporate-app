import { BlockUiService } from './../../../../plugins/block-ui/block-ui.service';
import { Funcao } from './../funcao.model';
import { FuncaoService } from './../../funcao/funcao.service';
import { MensagemService } from './../../../../plugins/forms/mensagem.service';
import { SelectGroupOption } from './../../../../plugins/forms/select-group/select-group-option';
import { Subscription } from 'rxjs/Rx';
import { Component, OnInit } from '@angular/core';
import { AbstractCadastroComponent } from '../../../../components/geral/crud/abstract-cadastro.component';
import { EnumUtils } from '../../../../plugins/shared';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-funcao-cadastro',
  templateUrl: './funcao-cadastro.component.html',
  styleUrls: ['./funcao-cadastro.component.css']
})
export class FuncaoCadastroComponent extends AbstractCadastroComponent<Funcao, FuncaoService> {

  inscricoes: Subscription[] = [];

  // Colecoes
  itensStatus: SelectGroupOption[] = EnumUtils.getOptionsAtivo();
  itensSimNao: SelectGroupOption[] = EnumUtils.getOptionsSimNao();
  itensFuncao: SelectGroupOption[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private blockService : BlockUiService,
    private funcaoService: FuncaoService,
    private msg: MensagemService) {
    super(route, router, blockService, msg);
  }

  getService(): FuncaoService {
    return this.funcaoService;
  }

  getVoNewInstance(): Funcao {
    return {
      status: 'Ativo',
    };
  }
}
