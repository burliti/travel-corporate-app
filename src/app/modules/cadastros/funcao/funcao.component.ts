import { TituloPaginaService } from './../../../components/services/titulo-pagina.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-funcao',
  templateUrl: './funcao.component.html',
  styleUrls: ['./funcao.component.css']
})
export class FuncaoComponent implements OnInit {

  constructor(private tituloPaginaService : TituloPaginaService) { }

  ngOnInit() {
    this.tituloPaginaService.setTitulo("Cargos / Funções");
  }

}
