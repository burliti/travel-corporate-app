import { FuncaoCadastroComponent } from './funcao-cadastro/funcao-cadastro.component';
import { FuncaoConsultaComponent } from './funcao-consulta/funcao-consulta.component';
import { FuncaoComponent } from './funcao.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AutenticacaoGuard } from '../../../security/autenticacao.guard';

const routes: Routes = [{
  path: 'cadastros/funcao', component: FuncaoComponent, canActivate: [AutenticacaoGuard], children: [
    { path: '', component: FuncaoConsultaComponent },
    {
      path: ':acao', children: [
        { path: '', component: FuncaoCadastroComponent },
        { path: ':id', component: FuncaoCadastroComponent }
      ]
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FuncaoRoutingModule { }
