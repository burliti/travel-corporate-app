import { FormsModule } from '@angular/forms';
import { FuncaoService } from './funcao.service';
import { PluginsModule } from './../../../plugins/plugins.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FuncaoRoutingModule } from './funcao-routing.module';
import { FuncaoComponent } from './funcao.component';
import { FuncaoCadastroComponent } from './funcao-cadastro/funcao-cadastro.component';
import { FuncaoConsultaComponent } from './funcao-consulta/funcao-consulta.component';

@NgModule({
  imports: [
    CommonModule,
    PluginsModule,
    FormsModule,
    FuncaoRoutingModule
  ],
  providers: [FuncaoService],
  declarations: [FuncaoComponent, FuncaoCadastroComponent, FuncaoConsultaComponent]
})
export class FuncaoModule { }
