import { FileDownloadHelper } from './../../../plugins/file-download-helper';
import { Lancamento } from './../../financeiro/lancamento/lancamento.model';
import { ConsultaResponse } from './../../../components/model/consulta-response.model';
import { ConsultaRequest } from './../../../components/model/consulta-request.model';
import { Http, Headers, Response } from '@angular/http';
import { ExportacaoDadosFiltro } from './exportacao-dados-filtro.model';
import { LoginService } from './../../../security/login.service';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { Promise } from 'q';
import { Observable } from 'rxjs/Observable';
import { Viagem } from '../../financeiro/viagem/viagem.model';

@Injectable()
export class ExportacaoDadosService {

  constructor(
    private http: Http,
    private loginService: LoginService) {
  }

  getHeaders(): Headers {
    // cria uma instância de Headers
    const headers = new Headers();
    // Adiciona o tipo de conteúdo application/json
    headers.append('Content-Type', 'application/json');
    // Adiciona o token de autenticacao
    if (this.loginService.getUsuarioAutenticado() != null) {
      headers.append('AUTHENTICATION_TOKEN', this.loginService.getUsuarioAutenticado().chaveSessao);
    }

    return headers;
  }

  getUrl() {
    return '/api/reports/exportacao/v1';
  }

  /**
   * Retorna filtros para a tela de exportação de dados
   */
  getDadosFiltros(): Observable<ExportacaoDadosFiltro> {
    return this.http.get(this.getUrl() + '/dados',
      { headers: this.getHeaders() })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }


  /**
   * Retorna uma lista de viagens para o DataGrid
   * @param consultaRequest parametros da consulta
   */
  getListViagem(consultaRequest: ConsultaRequest): Observable<ConsultaResponse<Viagem>> {
    return this.http.post(this.getUrl() + '/preview/viagem',
      JSON.stringify(consultaRequest),
      { headers: this.getHeaders() })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  /**
   * Retorna uma lista de lancamentos para o DataGrid
   * @param consultaRequest parametros da consulta
   */
  getListLancamento(consultaRequest: ConsultaRequest): Observable<ConsultaResponse<Lancamento>> {
    return this.http.post(this.getUrl() + '/preview/lancamento',
      JSON.stringify(consultaRequest),
      { headers: this.getHeaders() })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  /**
   * Baixa o arquivo de Viagens
   * @param consultaRequest parametros da consulta
   */
  downloadArquivoViagem(consultaRequest: ConsultaRequest, callback: any, callbackError: any) {
    this.http.post(this.getUrl() + '/download/viagem',
      JSON.stringify(consultaRequest),
      { headers: this.getHeaders(), responseType: 3 }).subscribe((res) => {
        console.log(res);

        let fileName = FileDownloadHelper.getFileNameFromResponseContentDisposition(res);
        FileDownloadHelper.saveFile(res.blob(), fileName);

        callback();
      }, (error) => {
        callbackError(error);
      });
  }

  /**
   * Baixa o arquivo de Lançamentos
   * @param consultaRequest parametros da consulta
   */
  downloadArquivoLancamento(consultaRequest: ConsultaRequest, callback: any, callbackError: any) {
    this.http.post(this.getUrl() + '/download/lancamento',
      JSON.stringify(consultaRequest),
      { headers: this.getHeaders(), responseType: 3 }).subscribe((res) => {

        console.log(res);

        let fileName = FileDownloadHelper.getFileNameFromResponseContentDisposition(res);
        FileDownloadHelper.saveFile(res.blob(), fileName);

        callback();
      }, (error) => {
        callbackError(error);
      });
  }

}
