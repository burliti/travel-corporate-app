import { Lancamento } from './../../financeiro/lancamento/lancamento.model';
import { ConsultaResponse } from './../../../components/model/consulta-response.model';
import { Viagem } from './../../financeiro/viagem/viagem.model';
import { DataTableComponent } from './../../../plugins/data-table/data-table.component';
import { DataPaginationEvent } from './../../../plugins/plugins.api';
import { EnumUtils } from './../../../plugins/shared';
import { SelectGroupOption } from './../../../plugins/forms/select-group/select-group-option';
import { ExportacaoDadosService } from './exportacao-dados-service';
import { FuncionarioService } from './../../cadastros/funcionario/funcionario.service';
import { BlockUiService } from './../../../plugins/block-ui/block-ui.service';
import { MensagemService } from './../../../plugins/forms/mensagem.service';
import { Subscription } from 'rxjs/Rx';
import { TituloPaginaService } from './../../../components/services/titulo-pagina.service';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';

@Component({
  selector: 'app-exportacao-dados',
  templateUrl: './exportacao-dados.component.html',
  styleUrls: ['./exportacao-dados.component.scss']
})
export class ExportacaoDadosComponent implements OnInit, OnDestroy {

  filtro: any = {};
  order: any = { "id": "desc" };

  dadosViagem: ConsultaResponse<Viagem>;
  dadosLancamento: ConsultaResponse<Lancamento>;

  public tipoRelatorio: string = 'VIAGENS';

  public inscricoes: Subscription[] = [];

  public itensEmpresa: SelectGroupOption[] = [];
  public itensCategoria: SelectGroupOption[] = [];
  public itensFuncionario: SelectGroupOption[] = [];
  public itensSetor: SelectGroupOption[] = [];
  public itensStatusViagem: SelectGroupOption[] = [];
  public itensCentroCusto: SelectGroupOption[] = [];
  public itensTipoSaida: SelectGroupOption[] = [];

  // Recupera o componente dataTable
  @ViewChild('dtViagem') dtViagem: DataTableComponent<Viagem>;
  @ViewChild('dtLancamento') dtLancamento: DataTableComponent<Viagem>;

  constructor(
    private ts: TituloPaginaService,
    private msg: MensagemService,
    private blockService: BlockUiService,
    private exportacaoDadosService: ExportacaoDadosService) { }

  ngOnInit() {
    this.ts.setTitulo('Exportação de dados');

    this.itensStatusViagem = EnumUtils.getOptionsStatusViagem('TODOS');

    this.itensTipoSaida.push({ label: 'Viagens', value: 'VIAGENS' });
    this.itensTipoSaida.push({ label: 'Lançamentos', value: 'LANCAMENTOS' });

    this.tipoRelatorio = 'VIAGENS';

    let s = this.exportacaoDadosService.getDadosFiltros().subscribe((retorno) => {

      this.itensEmpresa.push({ label: 'TODOS' });
      this.itensCategoria.push({ label: 'TODOS' });
      this.itensFuncionario.push({ label: 'TODOS' });
      this.itensSetor.push({ label: 'TODOS' });
      this.itensCentroCusto.push({ label: 'TODOS' });

      retorno.empresas ? retorno.empresas.forEach((e) => this.itensEmpresa.push({ label: e.nome, value: e.id })) : null;
      retorno.categorias ? retorno.categorias.forEach((c) => this.itensCategoria.push({ label: c.nome, value: c.id })) : null;
      retorno.funcionarios ? retorno.funcionarios.forEach((f) => this.itensFuncionario.push({ label: f.nome, value: f.id })) : null;
      retorno.setores ? retorno.setores.forEach((s) => this.itensSetor.push({ label: s.nome, value: s.id })) : null;
      retorno.centrosCusto ? retorno.centrosCusto.forEach((c) => this.itensCentroCusto.push({ label: c.nome, value: c.id.toString() })) : null;

    }, (error) => {
      console.error(error);
    });

    this.inscricoes.push(s);
  }

  ngOnDestroy() {
    this.inscricoes.forEach((s) => s.unsubscribe());
  }

  mudarTipo() {
    // this.pesquisar();
    this.limpar();
  }

  pesquisar() {
    if (this.tipoRelatorio === 'VIAGENS') {
      if (this.dtViagem)
        this.dtViagem.load(1);
    } else if (this.tipoRelatorio === 'LANCAMENTOS') {
      if (this.dtLancamento)
        this.dtLancamento.load(1);
    }
  }

  paginacao(event: DataPaginationEvent) {

    if (this.tipoRelatorio == 'VIAGENS') {
      this.blockService.block('.table-consulta');

      let s = this.exportacaoDadosService.getListViagem({
        filtro: this.filtro,
        order: this.order,
        pageSize: event.pageSize,
        pageNumber: event.page
      }).subscribe((dados) => {
        this.dadosViagem = dados;

        this.blockService.unBlock('.table-consulta');
      }, (error) => {
        this.dadosViagem = {
          lista: [],
          total: 0
        };

        this.blockService.unBlock('.table-consulta');
      });
    } else if (this.tipoRelatorio == 'LANCAMENTOS') {
      this.blockService.block('.table-consulta');

      let s = this.exportacaoDadosService.getListLancamento({
        filtro: this.filtro,
        order: this.order,
        pageSize: event.pageSize,
        pageNumber: event.page
      }).subscribe((dados) => {
        this.dadosLancamento = dados;

        this.blockService.unBlock('.table-consulta');
      }, (error) => {
        this.dadosLancamento = {
          lista: [],
          total: 0
        };

        this.blockService.unBlock('.table-consulta');
      });
    }
  }

  processar() {
    if (this.isViagem()) {
      this.msg.confirm('Serão gerados ' + this.dadosViagem.total + ' registros, agrupados por viagem. Deseja continuar?')
        .then(() => {
          this.blockService.block();

          // Download do arquivo
          this.exportacaoDadosService.downloadArquivoViagem({
            filtro: this.filtro,
            order: this.order
          }, () => this.blockService.unBlock(), (error) => {
            this.blockService.unBlock();
            console.error(error)
          });
        });
    } else if (this.isLancamento()) {
      this.msg.confirm('Serão gerados ' + this.dadosLancamento.total + ' registros, detalhados por lançamento.. Deseja continuar?')
        .then(() => {
          // Download do arquivo
          this.exportacaoDadosService.downloadArquivoLancamento({
            filtro: this.filtro,
            order: this.order
          }, () => this.blockService.unBlock(), (error) => {
            this.blockService.unBlock()
            console.error(error)
          });
        });
    }
  }

  limpar() {
    this.filtro = {};
  }

  isViagem() {
    return this.tipoRelatorio == 'VIAGENS';
  }

  isLancamento() {
    return this.tipoRelatorio == 'LANCAMENTOS';
  }
}
