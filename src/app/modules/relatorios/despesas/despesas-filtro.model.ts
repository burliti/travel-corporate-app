import { CentroCusto } from './../../cadastros/centro-custo/centro-custo.model';
import { Cliente } from './../../cadastros/cliente/cliente.model';
import { Categoria } from './../../configuracoes/categoria/categoria.model';
import { Empresa } from './../../configuracoes/empresa/empresa.model';
import { Funcionario } from './../../cadastros/funcionario/funcionario.model';
import { Setor } from '../../configuracoes/setor/setor.model';
import { Projeto } from '../../cadastros/projeto/projeto.model';

export class DespesasFiltro {
  funcionarios?: Funcionario[];
  setores?: Setor[];
  empresas?: Empresa[];
  categorias?: Categoria[];
  clientes?: Cliente[];
  projetos?: Projeto[];
  centrosCusto?: CentroCusto[];
}
