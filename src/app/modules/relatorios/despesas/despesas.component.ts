import { DespesasService } from './despesas.service';
import { Lancamento } from './../../financeiro/lancamento/lancamento.model';
import { ConsultaResponse } from './../../../components/model/consulta-response.model';
import { Viagem } from './../../financeiro/viagem/viagem.model';
import { DataTableComponent } from './../../../plugins/data-table/data-table.component';
import { DataPaginationEvent } from './../../../plugins/plugins.api';
import { EnumUtils } from './../../../plugins/shared';
import { SelectGroupOption } from './../../../plugins/forms/select-group/select-group-option';
import { FuncionarioService } from './../../cadastros/funcionario/funcionario.service';
import { BlockUiService } from './../../../plugins/block-ui/block-ui.service';
import { MensagemService } from './../../../plugins/forms/mensagem.service';
import { Subscription } from 'rxjs/Rx';
import { TituloPaginaService } from './../../../components/services/titulo-pagina.service';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';

@Component({
  selector: 'app-despesas',
  templateUrl: './despesas.component.html',
  styleUrls: ['./despesas.component.scss']
})
export class DespesasComponent implements OnInit {

  filtro: any = {};
  order: any = { "id": "desc" };

  dadosViagem: ConsultaResponse<Viagem>;
  dadosLancamento: ConsultaResponse<Lancamento>;

  public tipoRelatorio: string = 'VIAGENS';

  public inscricoes: Subscription[] = [];

  public itensEmpresa: SelectGroupOption[] = [];
  public itensCategoria: SelectGroupOption[] = [];
  public itensFuncionario: SelectGroupOption[] = [];
  public itensSetor: SelectGroupOption[] = [];
  public itensStatusViagem: SelectGroupOption[] = [];
  public itensCentroCusto: SelectGroupOption[] = [];
  public itensTipoRelatorio: SelectGroupOption[] = [];

  constructor(
    private ts: TituloPaginaService,
    private msg: MensagemService,
    private blockService: BlockUiService,
    private despesasService: DespesasService) { }

  ngOnInit() {
    this.ts.setTitulo('Relatório de despesas');

    this.itensTipoRelatorio.push({ label: 'Despesas por dia', value: 'DESPESAS_DIA' });
    this.itensTipoRelatorio.push({ label: 'Despesas por dia / categoria', value: 'DESPESAS_DIA_CATEGORIA' });
    this.itensTipoRelatorio.push({ label: 'Despesas por semana', value: 'DESPESAS_SEMANA' });
    this.itensTipoRelatorio.push({ label: 'Despesas detalhadas', value: 'DESPESAS_DETALHADA' });

    this.filtro.tipoRelatorio = 'DESPESAS_DIA';

    let s = this.despesasService.getDadosFiltros().subscribe((retorno) => {

      this.itensEmpresa.push({ label: 'TODOS' });
      this.itensCategoria.push({ label: 'TODOS' });
      this.itensFuncionario.push({ label: 'TODOS' });
      this.itensSetor.push({ label: 'TODOS' });
      this.itensCentroCusto.push({ label: 'TODOS' });

      retorno.empresas ? retorno.empresas.forEach((e) => this.itensEmpresa.push({ label: e.nome, value: e.id })) : null;
      retorno.categorias ? retorno.categorias.forEach((c) => this.itensCategoria.push({ label: c.nome, value: c.id })) : null;
      retorno.funcionarios ? retorno.funcionarios.forEach((f) => this.itensFuncionario.push({ label: f.nome, value: f.id })) : null;
      retorno.setores ? retorno.setores.forEach((s) => this.itensSetor.push({ label: s.nome, value: s.id })) : null;
      retorno.centrosCusto ? retorno.centrosCusto.forEach((c) => this.itensCentroCusto.push({ label: c.nome, value: c.id.toString() })) : null;

      this.itensStatusViagem = EnumUtils.getOptionsStatusViagem('TODOS');
    }, (error) => {
      console.error(error);
    });

    this.inscricoes.push(s);
  }

  ngOnDestroy() {
    this.inscricoes.forEach((s) => s.unsubscribe());
  }

  processar() {
    this.blockService.block();

    // Download do arquivo
    this.despesasService.downloadPdf({
      filtro: this.filtro,
      order: this.order
    }, () => this.blockService.unBlock(), (error) => {
      this.blockService.unBlock();
      console.error(error)
    });
  }

  pesquisar() {
    // TODO: Ainda nao sei pra que servirá
  }

  mudarTipo() {
    // TODO: Tipo de relatório
  }

  limpar() {
    let tipoRelatorio = this.filtro.tipoRelatorio;
    this.filtro = {};
    this.filtro.tipoRelatorio = tipoRelatorio;
  }

}
