import { DespesasService } from './despesas/despesas.service';
import { ExportacaoDadosService } from './exportacao-dados/exportacao-dados-service';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExportacaoDadosComponent } from './exportacao-dados/exportacao-dados.component';
import { RelatoriosRoutingModule } from './relatorios-routing.module';
import { PluginsModule } from '../../plugins/plugins.module';
import { DespesasComponent } from './despesas/despesas.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PluginsModule,
    RelatoriosRoutingModule
  ],
  providers : [ExportacaoDadosService, DespesasService],
  declarations: [ExportacaoDadosComponent, DespesasComponent]
})
export class RelatoriosModule { }
