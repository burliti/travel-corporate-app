import { DespesasComponent } from './despesas/despesas.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExportacaoDadosComponent } from './exportacao-dados/exportacao-dados.component';

const routes: Routes = [
  {
    path: 'relatorios', children: [
      { path: 'exportacao-dados', component: ExportacaoDadosComponent },
      { path: 'despesas', component: DespesasComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RelatoriosRoutingModule { }
