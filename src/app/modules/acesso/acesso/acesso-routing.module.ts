import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DadosFuncionarioComponent } from '../dados-funcionario/dados-funcionario.component';
import { AutenticacaoGuard } from './../../../security/autenticacao.guard';

const routes: Routes = [
  { path: 'dados-funcionario', component: DadosFuncionarioComponent, canActivate : [AutenticacaoGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AcessoRoutingModule { }
