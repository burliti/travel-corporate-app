import { FormsModule } from '@angular/forms';
import { PluginsModule } from './../../../plugins/plugins.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DadosFuncionarioComponent } from '../dados-funcionario/dados-funcionario.component';
import { AcessoRoutingModule } from './acesso-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PluginsModule,
    AcessoRoutingModule
  ],
  declarations: [DadosFuncionarioComponent]
})
export class AcessoModule { }
