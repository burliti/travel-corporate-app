import { Utils } from './../../../plugins/shared';
import { MensagemService } from './../../../plugins/forms/mensagem.service';
import { Subscription } from 'rxjs/Rx';
import { LoginService } from './../../../security/login.service';
import { Funcionario } from './../../cadastros/funcionario/funcionario.model';
import { TituloPaginaService } from './../../../components/services/titulo-pagina.service';
import { FuncionarioService } from './../../cadastros/funcionario/funcionario.service';
import { Component, OnInit } from '@angular/core';

declare var $: any;

@Component({
  selector: 'app-dados-funcionario',
  templateUrl: './dados-funcionario.component.html',
  styleUrls: ['./dados-funcionario.component.css']
})
export class DadosFuncionarioComponent implements OnInit {

  inscricoes: Subscription[] = [];
  novaSenha: string;
  confirmacaoNovaSenha: string;
  funcionario: Funcionario = {};

  constructor(
    private loginService: LoginService,
    private funcionarioService: FuncionarioService,
    private tituloPaginaService: TituloPaginaService,
    private msg: MensagemService) {
  }

  ngOnInit() {
    this.tituloPaginaService.setTitulo("Meus dados");
    this.funcionario = this.loginService.getUsuarioAutenticado().funcionario;
  }

  alterarSenha() {
    const s = this.loginService.alterarSenha('', this.novaSenha, this.confirmacaoNovaSenha).subscribe((retorno) => {
      if (retorno.success) {
        this.msg.nofiticationInfo(retorno.message);

        this.novaSenha = '';
        this.confirmacaoNovaSenha = '';
      } else {
        this.msg.error(Utils.buildErrorMessage(retorno)).then(() => {
          $("#internal_novaSenha").focus();
        });
      }
    });

    this.inscricoes.push(s);
  }

  ngOnDestroy() {
    // Remove as inscricoes
    for (const s of this.inscricoes) {
      s.unsubscribe();
    }
  }

  getFotoFuncionario() {
    return this.loginService.getFotoFuncionario(this.loginService.getUsuarioAutenticado());
  }
}
