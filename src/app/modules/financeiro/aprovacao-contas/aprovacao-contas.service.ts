import { Viagem } from './../viagem/viagem.model';
import { CadastroResponse } from './../../../components/model/cadastro-response.model';
import { Lancamento } from './../../financeiro/lancamento/lancamento.model';
import { Observable } from 'rxjs';
import { ConsultaResponse } from './../../../components/model/consulta-response.model';
import { Injectable, Inject } from '@angular/core';
import { AbstractCrudService } from '../../../components/services/abstract-crud.service';
import { Http } from '@angular/http';
import { LoginService } from '../../../security/login.service';

@Injectable()
export class AprovacaoContasService extends AbstractCrudService<Viagem> {

  constructor( @Inject(Http) _http: Http, @Inject(LoginService) loginService: LoginService) {
    super(_http, loginService);
  }

  getEntityName(): string {
    return "aprovacao-contas";
  }

  /**
   * Aprova o Relatório de Viagem
   * @param vo Entidade de fechamento
   */
  aprovar(vo: Viagem): Observable<CadastroResponse<Viagem>> {
    return this._http.post(this.getUrl() + '/aprovar', JSON.stringify(vo), { headers: this.getHeaders() })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  /**
  * Aprova o Relatório de Viagem
  * @param vo Entidade de fechamento
  */
  rejeitar(vo: Viagem): Observable<CadastroResponse<Viagem>> {
    return this._http.post(this.getUrl() + '/rejeitar', JSON.stringify(vo), { headers: this.getHeaders() })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

}
