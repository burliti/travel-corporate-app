import { MinhasViagensService } from './../../../colaborador/minhas-viagens/minhas-viagens.service';
import { FormatUtils } from './../../../../plugins/format-utils';
import { BlockUiService } from './../../../../plugins/block-ui/block-ui.service';
import { AprovacaoContasService } from './../aprovacao-contas.service';
import { ModalComponent } from './../../../../plugins/modal/modal.component';
import { Recibo } from './../../../financeiro/recibo/recibo.model';
import { Utils } from './../../../../plugins/shared';
import { Lancamento } from './../../../financeiro/lancamento/lancamento.model';
import { LoginService } from './../../../../security/login.service';
import { CategoriaService } from './../../../configuracoes/categoria/categoria.service';
import { MensagemService } from './../../../../plugins/forms/mensagem.service';
import { SelectGroupOption } from './../../../../plugins/forms/select-group/select-group-option';
import { Subscription } from 'rxjs/Rx';
import { Component, OnInit, ViewChild } from '@angular/core';
import { AbstractCadastroComponent } from '../../../../components/geral/crud/abstract-cadastro.component';
import { EnumUtils } from '../../../../plugins/shared';
import { ActivatedRoute, Router } from '@angular/router';
import { Viagem } from '../../../financeiro/viagem/viagem.model';
import { Categoria } from '../../../configuracoes/categoria/categoria.model';

declare var $: any;

@Component({
  selector: 'app-aprovacao-contas-cadastro',
  templateUrl: './aprovacao-contas-cadastro.component.html',
  styleUrls: ['./aprovacao-contas-cadastro.component.scss']
})
export class AprovacaoContasCadastroComponent extends AbstractCadastroComponent<Viagem, AprovacaoContasService> {

  categorias: Categoria[];
  inscricoes: Subscription[] = [];
  reciboSelecionado: Recibo = {};
  lancamentoSelecionado: Lancamento = {};
  itensDestinoSaldo: DestinoSaldo[] = [];

  @ViewChild('reciboModal') modalRecibo: ModalComponent;
  @ViewChild('modalGlosa') modalGlosa: ModalComponent;
  @ViewChild('modalMotivoRejeicao') modalMotivoRejeicao: ModalComponent;
  @ViewChild('modalSelecionarDestino') modalSelecionarDestino: ModalComponent;

  constructor(
    private loginService: LoginService,
    private route: ActivatedRoute,
    private router: Router,
    private blockService: BlockUiService,
    private aprovacaoContasService: AprovacaoContasService,
    private minhasViagensService: MinhasViagensService,
    private msg: MensagemService) {
    super(route, router, blockService, msg);
  }

  ngOnInit() {
    this.bloquear();

    this.minhasViagensService.getCategorias().subscribe((retorno) => {
      this.categorias = retorno;
      super.ngOnInit();
    }, (error) => console.log(error));
  }

  exibirRecibo(lancamento: Lancamento) {
    this.reciboSelecionado = lancamento.recibo;
    this.modalRecibo.show();
  }

  glosa(lancamento: Lancamento) {
    // this.lancamentoSelecionado = lancamento;
    this.lancamentoSelecionado = Object.assign({}, lancamento);

    if (!this.lancamentoSelecionado.valorOriginal && this.lancamentoSelecionado.glosado != 'Sim') {
      this.lancamentoSelecionado.valorOriginal = this.lancamentoSelecionado.valorLancamento;
      this.lancamentoSelecionado.valorGlosa = 0;
    }

    this.lancamentoSelecionado.valorLancamentoString = FormatUtils.formatMoney(this.lancamentoSelecionado.valorLancamento);

    this.modalGlosa.show();

    setTimeout(() => {
      $("#internal_valorLancamentoStringX").focus();
      $("#internal_valorLancamentoStringX").select();
    }, 500);
  }

  calcularValorGlosa(lancamento: Lancamento) {
    if (!lancamento.valorLancamentoString || lancamento.valorLancamentoString.trim() == '') {
      lancamento.valorLancamentoString = '0.00';
    }

    lancamento.valorLancamento = FormatUtils.parseMoney(lancamento.valorLancamentoString);

    lancamento.valorGlosa = lancamento.valorLancamento - lancamento.valorOriginal;
  }

  confirmarGlosa(lancamento: Lancamento) {
    if (lancamento.valorGlosa != 0) {
      if (!lancamento.observacoesGlosa || lancamento.observacoesGlosa.trim() == '') {
        this.msg.warn('Informe o motivo da glosa!').then(() => $('#internal_observacoesGlosa').focus());
        return;
      }
    }

    let l = this.getLancamento(lancamento);

    if (l) {
      l.valorLancamento = lancamento.valorLancamento;
      l.valorGlosa = lancamento.valorGlosa;
      l.valorOriginal = lancamento.valorOriginal;
      l.observacoesGlosa = lancamento.observacoesGlosa;
    }

    if (l.valorGlosa != 0) {
      l.glosado = 'Sim';
    } else {
      l.glosado = 'Não';
    }

    this.minhasViagensService.calcularSaldos(this.vo, this.categorias);

    this.modalGlosa.close();
  }

  restaurarValorOriginal(lancamento: Lancamento) {
    console.log(lancamento);

    lancamento.valorLancamento = lancamento.valorOriginal;
    lancamento.valorLancamentoString = FormatUtils.formatMoney(lancamento.valorLancamento);

    this.calcularValorGlosa(lancamento);

    console.log(lancamento);
  }

  getLancamento(lancamento: Lancamento): Lancamento {
    let l = null;

    this.vo.lancamentos
      .filter((lancamentoFilter) => lancamentoFilter.id == lancamento.id)
      .forEach((item) => l = item);

    return l;
  }

  aprovar() {
    this.itensDestinoSaldo = [];

    if (this.vo.fechamento.saldoFinal == 0) {
      this.itensDestinoSaldo.push({ label: 'Confirmar', value: 'SEM_SALDO' });
    } else if (this.vo.fechamento.saldoFinal > 0) {
      this.itensDestinoSaldo.push({ label: 'Devolução', value: 'DEVOLUCAO', help: 'O saldo do relatório será zerado, e será gerado um documento para o colaborador devolver o saldo positivo.' });
      this.itensDestinoSaldo.push({ label: 'Saldo positivo para próxima viagem', value: 'SALDO_POSITIVO_PROXIMA_VIAGEM', help: 'O saldo positivo será utilizado na próxima viagem.' });
      this.itensDestinoSaldo.push({ label: 'Zerar saldo positivo', value: 'ZERAR_SALDO_POSITIVO', help: 'O saldo do relatório será zerado, e o valor a mais ficará para o colaborador.' });
    } else if (this.vo.fechamento.saldoFinal < 0) {
      this.itensDestinoSaldo.push({ label: 'Reembolso', value: 'REEMBOLSO' });
      this.itensDestinoSaldo.push({ label: 'Zerar saldo negativo', value: 'ZERAR_SALDO_NEGATIVO' });
      this.itensDestinoSaldo.push({ label: 'Saldo negativo para próxima viagem', value: 'SALDO_NEGATIVO_PROXIMA_VIAGEM' });
    }

    this.modalSelecionarDestino.show();

    // this.msg.confirm('Deseja APROVAR este Relatório de Viagem? Esta ação não poderá ser desfeita!')
    //   .then(() => {
    //     this.aprovacaoContasService.aprovar(this.vo).subscribe((retorno) => {
    //       if (retorno.success) {
    //         this.msg.nofiticationSucess(retorno.message);

    //         this.voltar();
    //       } else {
    //         this.msg.error(Utils.buildErrorMessage(retorno));
    //       }
    //     });
    //   }).catch(() => {
    //     // Nao faz nada....
    //   });;
  }

  confirmarAprovar(destinoSaldo: string) {
    this.modalSelecionarDestino.close();

    this.vo.fechamento.destinoSaldo = destinoSaldo;

    this.aprovacaoContasService.aprovar(this.vo).subscribe((retorno) => {
      if (retorno.success) {
        this.msg.nofiticationSucess(retorno.message);

        this.voltar();
      } else {
        this.msg.error(Utils.buildErrorMessage(retorno));
      }
    });
  }

  /**
   * Indica se um lançamento pode ser glosado.
   * @param lancamento Lançamento a ser avaliado.
   */
  isPodeFazerGlosa(lancamento: Lancamento): boolean {
    if (this.isExcluir() || this.isFinalizado()) {
      return false;
    }

    return this.getCategoria(lancamento.categoria.id).interna == 'Não';
  }

  getCategoria(id: any): Categoria {
    let c = {};

    this.categorias.forEach((item) => {
      if (item.id == id) {
        c = item;
      }
    });

    return c;
  }

  ajuda(idButton: string) {
    $('#' + idButton).popover('toggle');
  }

  rejeitar() {
    this.msg.confirm('Deseja REJEITAR este Relatório de Viagem? Esta ação não poderá ser desfeita!')
      .then(() => {
        this.modalMotivoRejeicao.show();
      }).catch(() => {
        // Nao faz nada....
      });
  }

  confirmarRejeicao() {
    this.aprovacaoContasService.rejeitar(this.vo).subscribe((retorno) => {
      if (retorno.success) {
        this.msg.nofiticationSucess(retorno.message);

        this.voltar();
      } else {
        this.msg.error(Utils.buildErrorMessage(retorno));
      }
    });
  }

  isSelecionado(lancamento: Lancamento) {
    let index = this.getLancamentoIndex(this.vo.lancamentos, lancamento);

    return index >= 0;
  }

  isFinalizado() {
    return this.vo.status == 'Relatório aprovado' || this.vo.status == 'Relatório rejeitado';
  }

  getLancamentoIndex(lancamentos: Lancamento[], lancamento: Lancamento) {
    let i = -1;
    for (let c of lancamentos) {
      i++;
      if (c.id === lancamento.id) {
        return i;
      }
    }
    return -1;
  }

  isHabilitadoAprovarRejeitar() {
    return this.vo.status == 'Finalizado' || this.vo.status == 'Relatório em análise';
  }

  isRejeitado() {
    return this.vo.status == 'Relatório rejeitado';
  }

  isAprovado() {
    return this.vo.status == 'Relatório aprovado';
  }

  isUrl(recibo: Recibo): boolean {
    if (recibo && recibo.urlNfe) {
      return recibo.urlNfe.startsWith('http');
    }
    return false;
  }

  getUrlAlternativa(recibo: Recibo): string {
    if (recibo && recibo.chaveNfe) {
      if (recibo.chaveNfe.startsWith('CFe35')) {
        // Projeto SAT - SP
        return 'https://satsp.fazenda.sp.gov.br/COMSAT/Public/ConsultaPublica/ConsultaPublicaCfe.aspx';
      }
    }
    return '';
  }

  salvar() {
    super.salvar(false, true);
  }

  getService(): AprovacaoContasService {
    return this.aprovacaoContasService;
  }

  getVoNewInstance(): Viagem {
    return {
      fechamento: {}
    };
  }
}


export class DestinoSaldo {
  label: string;
  value: string;
  help?: string;
}
