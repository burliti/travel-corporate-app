import { AprovacaoContasCadastroComponent } from './aprovacao-contas-cadastro/aprovacao-contas-cadastro.component';
import { AprovacaoContasConsultaComponent } from './aprovacao-contas-consulta/aprovacao-contas-consulta.component';
import { AutenticacaoGuard } from './../../../security/autenticacao.guard';
import { AprovacaoContasComponent } from './aprovacao-contas.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{
  path: 'financeiro/aprovacao-contas', component: AprovacaoContasComponent, canActivate: [AutenticacaoGuard], children: [
    { path: '', component: AprovacaoContasConsultaComponent },
    {
      path: ':acao', children: [
        { path: '', component: AprovacaoContasCadastroComponent },
        { path: ':id', component: AprovacaoContasCadastroComponent }
      ]
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AprovacaoContasRoutingModule { }
