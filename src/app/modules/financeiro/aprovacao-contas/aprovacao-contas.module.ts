import { AprovacaoContasService } from './aprovacao-contas.service';
import { FormsModule } from '@angular/forms';
import { PluginsModule } from './../../../plugins/plugins.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AprovacaoContasRoutingModule } from './aprovacao-contas-routing.module';
import { AprovacaoContasComponent } from './aprovacao-contas.component';
import { AprovacaoContasCadastroComponent } from './aprovacao-contas-cadastro/aprovacao-contas-cadastro.component';
import { AprovacaoContasConsultaComponent } from './aprovacao-contas-consulta/aprovacao-contas-consulta.component';

@NgModule({
  imports: [
    CommonModule,
    PluginsModule,
    FormsModule,
    AprovacaoContasRoutingModule
  ],
  providers: [AprovacaoContasService],
  declarations: [AprovacaoContasComponent, AprovacaoContasCadastroComponent, AprovacaoContasConsultaComponent]
})
export class AprovacaoContasModule { }
