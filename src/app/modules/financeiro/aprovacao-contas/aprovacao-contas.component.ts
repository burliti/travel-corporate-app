import { TituloPaginaService } from './../../../components/services/titulo-pagina.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-aprovacao-contas',
  templateUrl: './aprovacao-contas.component.html',
  styleUrls: ['./aprovacao-contas.component.css']
})
export class AprovacaoContasComponent implements OnInit {

  constructor(private ts : TituloPaginaService) { }

  ngOnInit() {
    this.ts.setTitulo("Aprovação de contas");
  }
}
