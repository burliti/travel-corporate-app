import { Fornecedor } from './../../cadastros/fornecedor/fornecedor.model';
import { Funcionario } from './../../cadastros/funcionario/funcionario.model';

export class Recibo {
  id?: string;
  dataInserido?: string;
  dataReferencia?: string;
  fotoAnexoRecibo?: string;
  numeroDocumento?: string;
  valor?: number;
  valorString?: string;
  urlNfe?: string;
  chaveNfe?: string;
  fornecedor?: Fornecedor = {};
  nomeFornecedor?: string;
  cpfCnpjFornecedor?: string;
  funcionario?: Funcionario = {};
}
