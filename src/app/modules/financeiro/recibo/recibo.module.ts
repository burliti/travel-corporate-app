import { PluginsModule } from './../../../plugins/plugins.module';
import { FormsModule } from '@angular/forms';
import { ReciboService } from './recibo.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReciboRoutingModule } from './recibo-routing.module';
import { ReciboComponent } from './recibo.component';
import { ReciboCadastroComponent } from './recibo-cadastro/recibo-cadastro.component';
import { ReciboConsultaComponent } from './recibo-consulta/recibo-consulta.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PluginsModule,
    ReciboRoutingModule
  ],
  providers: [ReciboService],
  declarations: [ReciboComponent, ReciboCadastroComponent, ReciboConsultaComponent]
})
export class ReciboModule { }
