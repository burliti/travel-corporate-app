import { LoginService } from './../../../security/login.service';
import { Http } from '@angular/http';
import { Recibo } from './recibo.model';
import { Injectable, Inject } from '@angular/core';
import { AbstractCrudService } from '../../../components/services/abstract-crud.service';

@Injectable()
export class ReciboService extends AbstractCrudService<Recibo>{

  constructor( @Inject(Http) _http: Http, @Inject(LoginService) loginService: LoginService) {
    super(_http, loginService);
  }

  getEntityName(): string {
    return "recibo";
  }
}
