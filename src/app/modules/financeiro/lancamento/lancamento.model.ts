import { Fornecedor } from './../../cadastros/fornecedor/fornecedor.model';
import { Recibo } from './../recibo/recibo.model';
import { Categoria } from './../../configuracoes/categoria/categoria.model';
import { Viagem } from './../viagem/viagem.model';
import { Funcionario } from './../../cadastros/funcionario/funcionario.model';

export class Lancamento {
  id?: string;
  sequencial?: string;
  dataHoraLancamento?: string;
  dataInicial?: string;
  dataFinal?: string;
  observacoes?: string;
  observacoesGlosa?: string;
  tipoLancamento?: string;
  valorLancamento?: number;
  valorLancamentoString?: string;
  glosado?: string;
  valorOriginal?: number;
  valorGlosa?: number;
  valorGlosaString?: string;
  categoria?: Categoria = {};
  funcionario?: Funcionario = {};
  recibo?: Recibo = {};
  viagem?: Viagem = {};
  referenciado?: Lancamento = {};
  fornecedor?: Fornecedor = {};
  tipoPeriodo?: string;
}
