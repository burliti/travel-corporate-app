import { BlockUiService } from './../../../../plugins/block-ui/block-ui.service';
import { CategoriaService } from './../../../configuracoes/categoria/categoria.service';
import { LancamentoService } from './../lancamento.service';
import { Lancamento } from './../lancamento.model';
import { MensagemService } from './../../../../plugins/forms/mensagem.service';
import { SelectGroupOption } from './../../../../plugins/forms/select-group/select-group-option';
import { Subscription } from 'rxjs/Rx';
import { Component, OnInit } from '@angular/core';
import { AbstractCadastroComponent } from '../../../../components/geral/crud/abstract-cadastro.component';
import { EnumUtils } from '../../../../plugins/shared';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-lancamento-cadastro',
  templateUrl: './lancamento-cadastro.component.html',
  styleUrls: ['./lancamento-cadastro.component.css']
})
export class LancamentoCadastroComponent extends AbstractCadastroComponent<Lancamento, LancamentoService> {

  inscricoes: Subscription[] = [];

  // Colecoes
  itensStatus: SelectGroupOption[] = EnumUtils.getOptionsAtivo();
  itensSimNao: SelectGroupOption[] = EnumUtils.getOptionsSimNao();
  itensCategoria: SelectGroupOption[] = [];
  itensFuncionario: SelectGroupOption[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private blockService: BlockUiService,
    private lancamentoService: LancamentoService,
    private msg: MensagemService) {
    super(route, router, blockService, msg);
  }

  ngOnInit() {
    const s = this.lancamentoService.getCategorias('CREDITO').subscribe((retorno) => {
      this.itensCategoria.push({ label: 'Selecione a categoria de lançamento', value: '' });

      for (const categoria of retorno.lista) {
        this.itensCategoria.push({ label: categoria.nome, value: categoria.id, group: categoria.tipo });
      }

      const s2 = this.lancamentoService.getFuncionarios().subscribe((retorno) => {
        this.itensFuncionario.push({ label : 'Selecione o funcionário', value : ''})

        for (const funcionario of retorno.lista) {
          this.itensFuncionario.push({ label: funcionario.nome, value: funcionario.id });
        }

        super.ngOnInit();
      });

      this.inscricoes.push(s2);
    });

    this.inscricoes.push(s);
  }

  getService(): LancamentoService {
    return this.lancamentoService;
  }

  getVoNewInstance(): Lancamento {
    return {
      categoria: {},
      funcionario: {}
    };
  }
}
