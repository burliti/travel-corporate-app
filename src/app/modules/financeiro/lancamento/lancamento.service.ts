import { Categoria } from './../../configuracoes/categoria/categoria.model';
import { Lancamento } from './lancamento.model';
import { Observable } from 'rxjs';
import { ConsultaResponse } from './../../../components/model/consulta-response.model';
import { Injectable, Inject } from '@angular/core';
import { AbstractCrudService } from '../../../components/services/abstract-crud.service';
import { Http } from '@angular/http';
import { LoginService } from '../../../security/login.service';
import { Funcionario } from '../../cadastros/funcionario/funcionario.model';

@Injectable()
export class LancamentoService extends AbstractCrudService<Lancamento> {

  constructor( @Inject(Http) _http: Http, @Inject(LoginService) loginService: LoginService) {
    super(_http, loginService);
  }

  getEntityName(): string {
    return "lancamento";
  }

  getCategorias(tipoLancamento: string): Observable<ConsultaResponse<Categoria>> {
    return this._http.post(this.getUrl() + '/categorias/' + tipoLancamento,
      null,
      { headers: this.getHeaders() })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  getFuncionarios(): Observable<ConsultaResponse<Funcionario>> {
    return this._http.post(this.getUrl() + '/funcionarios',
      null,
      { headers: this.getHeaders() })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  getListAtivos(): Observable<ConsultaResponse<Lancamento>> {
    return this.getList({
      filtro: {},
      order: { "nome": "asc" },
      pageSize: null,
      pageNumber: null
    });
  }
}
