import { LancamentoCadastroComponent } from './lancamento-cadastro/lancamento-cadastro.component';
import { LancamentoConsultaComponent } from './lancamento-consulta/lancamento-consulta.component';
import { AutenticacaoGuard } from './../../../security/autenticacao.guard';
import { LancamentoComponent } from './lancamento.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{
  path: 'financeiro/lancamento', component: LancamentoComponent, canActivate: [AutenticacaoGuard], children: [
    { path: '', component: LancamentoConsultaComponent },
    {
      path: ':acao', children: [
        { path: '', component: LancamentoCadastroComponent },
        { path: ':id', component: LancamentoCadastroComponent }
      ]
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LancamentoRoutingModule { }
