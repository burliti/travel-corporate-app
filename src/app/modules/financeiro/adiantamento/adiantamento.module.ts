import { AdiantamentoService } from './adiantamento.service';
import { PluginsModule } from './../../../plugins/plugins.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdiantamentoRoutingModule } from './adiantamento-routing.module';
import { AdiantamentoComponent } from './adiantamento.component';
import { AdiantamentoCadastroComponent } from './adiantamento-cadastro/adiantamento-cadastro.component';
import { AdiantamentoConsultaComponent } from './adiantamento-consulta/adiantamento-consulta.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    PluginsModule,
    FormsModule,
    AdiantamentoRoutingModule
  ],
  providers: [AdiantamentoService],
  declarations: [AdiantamentoComponent, AdiantamentoCadastroComponent, AdiantamentoConsultaComponent]
})
export class AdiantamentoModule { }
