import { AdiantamentoService } from './../adiantamento.service';
import { BlockUiService } from './../../../../plugins/block-ui/block-ui.service';
import { MensagemService } from './../../../../plugins/forms/mensagem.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { Lancamento } from './../../lancamento/lancamento.model';
import { ConsultaResponse } from './../../../../components/model/consulta-response.model';
import { DataTableComponent } from './../../../../plugins/data-table/data-table.component';
import { DataPaginationEvent } from './../../../../plugins/plugins.api';
import { EnumUtils } from './../../../../plugins/shared';
import { SelectGroupOption } from './../../../../plugins/forms/select-group/select-group-option';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-adiantamento-consulta',
  templateUrl: './adiantamento-consulta.component.html',
  styleUrls: ['./adiantamento-consulta.component.css']
})
export class AdiantamentoConsultaComponent implements OnInit {
  // Colecoes
  itensStatus: SelectGroupOption[] = EnumUtils.getOptionsAtivo("TODOS");
  itensStatusViagem: SelectGroupOption[] = EnumUtils.getOptionsStatusViagem("TODOS");
  itensCategoria: SelectGroupOption[] = [];
  itensTipo: SelectGroupOption[] = EnumUtils.getOptionsTipoLancamento("TODOS");

  dados: ConsultaResponse<Lancamento>;

  filtro: any = {
    "ativo": ""
  };
  order: any = { "dataHoraLancamento": "desc" };

  private inscricoes: Subscription[] = [];

  // Recupera o componente dataTable
  @ViewChild(DataTableComponent) dt: DataTableComponent<Lancamento>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private blockService: BlockUiService,
    private msg: MensagemService,
    private adiantamentoService: AdiantamentoService) { }

  ngAfterViewInit() { }

  ngOnDestroy() {
    for (let s of this.inscricoes) {
      s.unsubscribe();
    }
  }

  ngOnInit() {
    this.itensCategoria.push({ label: "TODOS", group: "TODOS" });

    const s3 = this.adiantamentoService.getCategorias().subscribe((retorno) => {
      for (const categoria of retorno.lista) {
        this.itensCategoria.push({ label: categoria.nome, value: categoria.id, group: categoria.tipo });
      }
    });

    this.inscricoes.push(s3);
  }

  novo() {
    this.router.navigate(['novo'], { relativeTo: this.route });
  }

  editar() {
    if (this.dt.getSelectedItem()) {
      this.router.navigate(['editar', this.dt.getSelectedItem().id], { relativeTo: this.route });
    } else {
      this.msg.nofiticationWarning('Nenhum item selecionado para alterar!');
    }
  }

  excluir() {
    if (this.dt.getSelectedItem()) {
      this.router.navigate(['excluir', this.dt.getSelectedItem().id], { relativeTo: this.route });
    } else {
      this.msg.nofiticationWarning('Nenhum item selecionado para excluir!');
    }
  }

  pesquisar() {
    this.dt.load(1);
  }

  limpar() {
    this.filtro = {};
    this.pesquisar();
  }

  paginacao(event: DataPaginationEvent) {
    this.blockService.block('.table-consulta');

    let s = this.adiantamentoService.getList({
      filtro: this.filtro,
      order: this.order,
      pageSize: event.pageSize,
      pageNumber: event.page
    }).subscribe((dados) => {
      this.dados = dados;

      this.blockService.unBlock('.table-consulta');
    }, (error) => {
      this.dados = {
        lista: [],
        total: 0
      };

      this.blockService.unBlock('.table-consulta');
    });
  }
}
