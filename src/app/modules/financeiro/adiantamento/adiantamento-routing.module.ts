import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AutenticacaoGuard } from './../../../security/autenticacao.guard';

import { AdiantamentoCadastroComponent } from './adiantamento-cadastro/adiantamento-cadastro.component';
import { AdiantamentoConsultaComponent } from './adiantamento-consulta/adiantamento-consulta.component';
import { AdiantamentoComponent } from './adiantamento.component';

const routes: Routes = [{
  path: 'financeiro/adiantamento', component: AdiantamentoComponent, canActivate: [AutenticacaoGuard], children: [
    { path: '', component: AdiantamentoConsultaComponent },
    {
      path: ':acao', children: [
        { path: '', component: AdiantamentoCadastroComponent },
        { path: ':id', component: AdiantamentoCadastroComponent }
      ]
    }
  ]
}];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdiantamentoRoutingModule { }
