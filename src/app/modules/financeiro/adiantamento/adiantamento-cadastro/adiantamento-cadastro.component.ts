import { FormatUtils } from './../../../../plugins/format-utils';
import { Lancamento } from './../../lancamento/lancamento.model';
import { BlockUiService } from './../../../../plugins/block-ui/block-ui.service';
import { CategoriaService } from './../../../configuracoes/categoria/categoria.service';
import { MensagemService } from './../../../../plugins/forms/mensagem.service';
import { SelectGroupOption } from './../../../../plugins/forms/select-group/select-group-option';
import { Subscription } from 'rxjs/Rx';
import { Component, OnInit } from '@angular/core';
import { AbstractCadastroComponent } from '../../../../components/geral/crud/abstract-cadastro.component';
import { EnumUtils } from '../../../../plugins/shared';
import { ActivatedRoute, Router } from '@angular/router';
import { AdiantamentoService } from '../adiantamento.service';
import { Funcionario } from '../../../cadastros/funcionario/funcionario.model';

@Component({
  selector: 'app-adiantamento-cadastro',
  templateUrl: './adiantamento-cadastro.component.html',
  styleUrls: ['./adiantamento-cadastro.component.css']
})
export class AdiantamentoCadastroComponent extends AbstractCadastroComponent<Lancamento, AdiantamentoService> {

  inscricoes: Subscription[] = [];

  // Colecoes
  itensStatus: SelectGroupOption[] = EnumUtils.getOptionsAtivo();
  itensSimNao: SelectGroupOption[] = EnumUtils.getOptionsSimNao();
  itensCategoria: SelectGroupOption[] = [];
  itensFuncionario: SelectGroupOption[] = [];

  funcionarios: Funcionario[] = [];
  funcionarioSelecionado: Funcionario = {};

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private blockService: BlockUiService,
    private adiantamentoService: AdiantamentoService,
    private msg: MensagemService) {
    super(route, router, blockService, msg);
  }

  ngOnInit() {
    const s = this.adiantamentoService.getCategorias('CREDITO').subscribe((retorno) => {
      this.itensCategoria.push({ label: 'Selecione a categoria de lançamento', value: '' });

      for (const categoria of retorno.lista) {
        this.itensCategoria.push({ label: categoria.nome, value: categoria.id, group: categoria.tipo });
      }

      const s2 = this.adiantamentoService.getFuncionarios().subscribe((retorno) => {
        this.funcionarios = retorno.lista;

        this.itensFuncionario.push({ label: 'Selecione o funcionário', value: '' })

        for (const funcionario of retorno.lista) {
          this.itensFuncionario.push({ label: funcionario.nome, value: funcionario.id });
        }

        super.ngOnInit();
      });

      this.inscricoes.push(s2);
    });

    this.inscricoes.push(s);
  }

  beforeVisualizar(vo: Lancamento) {
    this.changeFuncionario();

    vo.valorLancamentoString = FormatUtils.formatMoney(vo.valorLancamento);
  }

  changeFuncionario() {
    this.funcionarioSelecionado = {};

    this.funcionarios.forEach(f => {
      if (f.id == this.vo.funcionario.id) {
        this.funcionarioSelecionado = f;
      }
    });
  }

  changeValor(lancamento: Lancamento) {
    if (!lancamento.valorLancamentoString || lancamento.valorLancamentoString.trim() == '') {
      lancamento.valorLancamentoString = '0.00';
    }

    lancamento.valorLancamento = parseFloat(lancamento.valorLancamentoString.replace(',', '.').replace('R', '').replace('$', ''));
  }

  getService(): AdiantamentoService {
    return this.adiantamentoService;
  }

  getVoNewInstance(): Lancamento {
    return {
      categoria: {},
      funcionario: {}
    };
  }
}
