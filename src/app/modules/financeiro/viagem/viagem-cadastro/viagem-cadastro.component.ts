import { BlockUiService } from './../../../../plugins/block-ui/block-ui.service';
import { FuncionarioService } from './../../../cadastros/funcionario/funcionario.service';
import { Funcionario } from './../../../cadastros/funcionario/funcionario.model';
import { ProjetoService } from './../../../cadastros/projeto/projeto.service';
import { Cliente } from './../../../cadastros/cliente/cliente.model';
import { ClienteService } from './../../../cadastros/cliente/cliente.service';
import { SelectGroupComponent } from './../../../../plugins/forms/select-group/select-group.component';
import { ViagemService } from './../viagem.service';
import { Viagem } from './../viagem.model';
import { MensagemService } from './../../../../plugins/forms/mensagem.service';
import { SelectGroupOption } from './../../../../plugins/forms/select-group/select-group-option';
import { Subscription } from 'rxjs/Rx';
import { Component, OnInit } from '@angular/core';
import { AbstractCadastroComponent } from '../../../../components/geral/crud/abstract-cadastro.component';
import { EnumUtils } from '../../../../plugins/shared';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-viagem-cadastro',
  templateUrl: './viagem-cadastro.component.html',
  styleUrls: ['./viagem-cadastro.component.css']
})
export class ViagemCadastroComponent extends AbstractCadastroComponent<Viagem, ViagemService> {
  funcionarios: Funcionario[] = [];

  inscricoes: Subscription[] = [];

  // Colecoes
  itensStatus: SelectGroupOption[] = EnumUtils.getOptionsAtivo();
  itensSimNao: SelectGroupOption[] = EnumUtils.getOptionsSimNao();
  itensFuncao: SelectGroupOption[] = [];
  itensStatusViagem: SelectGroupOption[] = EnumUtils.getOptionsStatusViagem("Selecione uma opção");
  itensCliente: SelectGroupOption[] = [];
  itensProjeto: SelectGroupOption[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private blockService: BlockUiService,
    private viagemService: ViagemService,
    private funcionarioService: FuncionarioService,
    private clienteService: ClienteService,
    private projetoService: ProjetoService,
    private msg: MensagemService) {
    super(route, router, blockService, msg);
  }

  ngOnInit() {
    this.itensCliente.push({ label: "Nenhum" });
    this.itensProjeto.push({ label: "Nenhum" });

    const s = this.clienteService.getListAtivos().subscribe((retorno) => {

      for (const cliente of retorno.lista) {
        this.itensCliente.push({ label: cliente.nome, value: cliente.id });
      }

      const s2 = this.projetoService.getListAtivos().subscribe((retorno) => {
        for (const projeto of retorno.lista) {
          this.itensProjeto.push({ label: projeto.nome, value: projeto.id });
        }

        this.funcionarioService.getListAtivos().subscribe((retorno) => {
          this.funcionarios = retorno.lista;

          super.ngOnInit();
        });
      });

      this.inscricoes.push(s2);
    });

    this.inscricoes.push(s);
  }

  beforeVisualizar() {
    if (!this.vo.cliente) {
      this.vo.cliente = {};
    }
    if (!this.vo.projeto) {
      this.vo.projeto = {};
    }
  }

  adicionarFuncionario(funcionario: Funcionario) {
    // this.vo.funcionarios.push(funcionario);
  }

  removerFuncionario(funcionario: Funcionario) {
    // var index = this.getFuncionarioIndex(this.vo.funcionarios, funcionario);
    // if (index > -1) {
    //   this.vo.funcionarios.splice(index, 1);
    // }
  }

  getFuncionariosNaoSelecionados() {
    // if (this.funcionarios != null) {
    //   return this.funcionarios.filter(funcionario => {
    //     var index = this.getFuncionarioIndex(this.vo.funcionarios, funcionario); //this.vo.clientes.indexOf(cliente, 0);
    //     if (index < 0) {
    //       return funcionario;
    //     }
    //   });
    // }
    return null;
  }

  getFuncionarioIndex(funcionarios: Funcionario[], funcionario: Funcionario) {
    let i = -1;
    for (let c of funcionarios) {
      i++;
      if (c.id === funcionario.id) {
        return i;
      }
    }
    return -1;
  }

  getService(): ViagemService {
    return this.viagemService;
  }

  getVoNewInstance(): Viagem {
    return {
      status: "Planejamento",
      cliente: {},
      projeto: {}
    };
  }
}

