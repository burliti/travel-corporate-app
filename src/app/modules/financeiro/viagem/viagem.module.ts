import { ViagemService } from './viagem.service';
import { PluginsModule } from './../../../plugins/plugins.module';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViagemRoutingModule } from './viagem-routing.module';
import { ViagemComponent } from './viagem.component';
import { ViagemCadastroComponent } from './viagem-cadastro/viagem-cadastro.component';
import { ViagemConsultaComponent } from './viagem-consulta/viagem-consulta.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PluginsModule,
    ViagemRoutingModule
  ],
  providers: [ViagemService],
  declarations: [ViagemComponent, ViagemCadastroComponent, ViagemConsultaComponent]
})
export class ViagemModule { }
