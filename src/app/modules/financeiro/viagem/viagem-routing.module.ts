import { AutenticacaoGuard } from './../../../security/autenticacao.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViagemComponent } from './viagem.component';
import { ViagemConsultaComponent } from './viagem-consulta/viagem-consulta.component';
import { ViagemCadastroComponent } from './viagem-cadastro/viagem-cadastro.component';

const routes: Routes = [{
  path: 'lancamentos/viagem', component: ViagemComponent, canActivate: [AutenticacaoGuard], children: [
    { path: '', component: ViagemConsultaComponent },
    {
      path: ':acao', children: [
        { path: '', component: ViagemCadastroComponent },
        { path: ':id', component: ViagemCadastroComponent }
      ]
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViagemRoutingModule { }
