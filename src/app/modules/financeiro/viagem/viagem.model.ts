import { Fechamento } from './../../colaborador/fechamento/fechamento.model';
import { Lancamento } from './../lancamento/lancamento.model';
import { Funcionario } from './../../cadastros/funcionario/funcionario.model';
import { Projeto } from './../../cadastros/projeto/projeto.model';
import { Cliente } from './../../cadastros/cliente/cliente.model';

export class Viagem {
  id?: string;
  sequencial?: string;
  dataIda?: string;
  dataVolta?: string;
  descricao?: string;
  objetivo?: string;
  observacao?: string;
  status?: string;
  cliente?: Cliente = {};
  projeto?: Projeto = {};
  funcionario?: Funcionario = {};
  lancamentos?: Lancamento[] = [];
  fechamento?: Fechamento = {};
}
