import { ConsultaRequest } from './../../../components/model/consulta-request.model';
import { Observable } from 'rxjs';
import { ConsultaResponse } from './../../../components/model/consulta-response.model';
import { Injectable, Inject } from '@angular/core';
import { AbstractCrudService } from '../../../components/services/abstract-crud.service';
import { Http } from '@angular/http';
import { LoginService } from '../../../security/login.service';
import { Viagem } from './viagem.model';

@Injectable()
export class ViagemService extends AbstractCrudService<Viagem> {

  constructor( @Inject(Http) _http: Http, @Inject(LoginService) loginService: LoginService) {
    super(_http, loginService);
  }

  getEntityName(): string {
    return "viagem";
  }

  getListAtivos(): Observable<ConsultaResponse<Viagem>> {
    return this.getList({
      filtro: {},
      order: { "nome": "asc" },
      pageSize: null,
      pageNumber: null
    });
  }

  /**
    * Retorna uma lista para o DataGrid
    */
  getListMinhasViagens(consultaRequest: ConsultaRequest): Observable<ConsultaResponse<Viagem>> {
    return this._http.post(this.getUrl() + '/minhas-viagens',
      JSON.stringify(consultaRequest),
      { headers: this.getHeaders() })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }
}
