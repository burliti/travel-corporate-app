import { TituloPaginaService } from './../../../components/services/titulo-pagina.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-viagem',
  templateUrl: './viagem.component.html',
  styleUrls: ['./viagem.component.css']
})
export class ViagemComponent implements OnInit {

  constructor(private tituloPaginaService: TituloPaginaService) { }

  ngOnInit() {
    this.tituloPaginaService.setTitulo("Viagens");
  }

}
