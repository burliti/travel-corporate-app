import { Estado } from './../../tabelas/estado/estado.model';
import { Cidade } from './../../tabelas/cidade/cidade.model';
import { Funcionario } from './../../cadastros/funcionario/funcionario.model';

export class Multa {
  id?: number;
  numeroMulta?: string;
  cidade?: Cidade = {};
  nomeCidade?: string;
  uf?: string;
  cidadeCompleta?: string;
  tipoVeiculo?: string;
  dataHora?: string;
  dataMultaAux?: string;
  horaMultaAux?: string;
  codigoMulta?: string;
  textoMulta?: string;
  valorMulta?: number;
  valorMultaString?: string;
  funcionario?: Funcionario = {};
  funcionarioCadastro?: Funcionario = {};
}

export class MultaDados {
  funcionarios?: Funcionario[];
  cidades?: Cidade[];
  estados?: Estado[];
}
