import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MultaRoutingModule } from './multa-routing.module';
import { MultaComponent } from './multa.component';
import { MultaCadastroComponent } from './multa-cadastro/multa-cadastro.component';
import { MultaConsultaComponent } from './multa-consulta/multa-consulta.component';
import { FormsModule } from '@angular/forms';
import { PluginsModule } from '../../../plugins/plugins.module';
import { MultaService } from './multa.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PluginsModule,
    MultaRoutingModule
  ],
  providers : [MultaService],
  declarations: [MultaComponent, MultaCadastroComponent, MultaConsultaComponent]
})
export class MultaModule { }
