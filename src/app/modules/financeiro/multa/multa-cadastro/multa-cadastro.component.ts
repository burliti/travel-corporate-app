import { FormatUtils } from './../../../../plugins/format-utils';
import { Cidade } from './../../../tabelas/cidade/cidade.model';
import { Estado } from './../../../tabelas/estado/estado.model';
import { Multa } from './../multa.model';
import { BlockUiService } from './../../../../plugins/block-ui/block-ui.service';
import { SelectGroupComponent } from './../../../../plugins/forms/select-group/select-group.component';
import { MensagemService } from './../../../../plugins/forms/mensagem.service';
import { SelectGroupOption } from './../../../../plugins/forms/select-group/select-group-option';
import { Subscription } from 'rxjs/Rx';
import { Component, OnInit } from '@angular/core';
import { AbstractCadastroComponent } from '../../../../components/geral/crud/abstract-cadastro.component';
import { EnumUtils } from '../../../../plugins/shared';
import { ActivatedRoute, Router } from '@angular/router';
import { MultaService } from '../multa.service';
import { LoginService } from '../../../../security/login.service';

declare var $: any;

@Component({
  selector: 'app-multa-cadastro',
  templateUrl: './multa-cadastro.component.html',
  styleUrls: ['./multa-cadastro.component.scss']
})
export class MultaCadastroComponent extends AbstractCadastroComponent<Multa, MultaService> {

  inscricoes: Subscription[] = [];

  // Colecoes
  itensStatus: SelectGroupOption[] = EnumUtils.getOptionsAtivo();
  itensSimNao: SelectGroupOption[] = EnumUtils.getOptionsSimNao();
  itensFuncionario: SelectGroupOption[] = [];
  itensTipoVeiculo: SelectGroupOption[] = EnumUtils.getOptionsTipoVeiculo('[Selecione o tipo]');
  itensCidade: SelectGroupOption[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private loginService: LoginService,
    private blockService: BlockUiService,
    private multaService: MultaService,
    private msg: MensagemService) {
    super(route, router, blockService, msg);
  }

  ngOnInit() {
    this.itensFuncionario.push({ label: '[Selecione o funcionário]' });

    this.bloquear();

    let s = this.multaService.getDados().subscribe((dados) => {

      dados.funcionarios.forEach((f) => this.itensFuncionario.push({ value: f.id, label: f.nome }));

      super.ngOnInit();
    }, (error) => {
      console.error(error);
      this.desbloquear();
    });

    this.inscricoes.push(s);
  }

  ngAfterViewInit() {
    let chave = this.loginService.getUsuarioAutenticado().chaveSessao;

    $('#internal_cidade').select2({
      ajax: {
        url: '/api/resources/multa/v1/cidades',
        data: function (params) {
          var query = {
            buscarPor: params.term,
            pagina: params.page || 1,
            tamanhoPagina: 20,
            AUTHENTICATION_TOKEN: chave
          };
          return query;
        },
        processResults: function (data, params) {
          params.page = params.page || 1;
          let results = [];

          data.lista.forEach((i) => results.push({ "id": i.id, "text": i.nome + ' - ' + i.estado.sigla, "estado": i.estado.sigla, "cidade": i.nome }));

          return {
            results: results,
            more: (params.page * 20) < data.total
          };
        }
      }
    });

    $('#internal_cidade').on('select2:select', (e) => {
      var data = e.params.data;

      if (data.cidade) {
        this.vo.nomeCidade = data.cidade;
      }
      if (data.estado) {
        this.vo.uf = data.estado;
      }
    });

    super.ngAfterViewInit();
  }

  beforeVisualizar(vo: Multa) {
    if (!vo.funcionario) {
      vo.funcionario = {};
    }

    if (!vo.funcionarioCadastro) {
      vo.funcionarioCadastro = {};
    }

    if (vo.dataHora) {
      vo.dataMultaAux = vo.dataHora.substring(0, 10);
      vo.horaMultaAux = vo.dataHora.substring(11, 16);
    }

    if (!vo.cidade) {
      vo.cidade = {};
    } else if (vo.cidade.id) {
      this.itensCidade.push({ label: vo.cidade.nome + ' - ' + vo.cidade.estado.sigla, value: vo.cidade.id.toString() });
      $('#internal_cidade').val(vo.cidade.id).trigger('change');
    }

    vo.valorMultaString = FormatUtils.formatMoney(vo.valorMulta);
  }

  changeValorMulta() {
    if (!this.vo.valorMultaString || this.vo.valorMultaString.trim() == '') {
      this.vo.valorMultaString = '0.00';
    }

    this.vo.valorMulta = FormatUtils.parseMoney(this.vo.valorMultaString);
  }

  beforeSalvar(vo: Multa) {
    if (vo.dataMultaAux && vo.horaMultaAux) {
      vo.dataHora = vo.dataMultaAux + ' ' + vo.horaMultaAux;
    }
    return true;
  }

  getService(): MultaService {
    return this.multaService;
  }

  getVoNewInstance(): Multa {
    return {
      cidade: {},
      funcionario: {},
      funcionarioCadastro: {}
    };
  }
}
