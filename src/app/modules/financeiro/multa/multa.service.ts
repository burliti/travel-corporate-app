import { ConsultaRequest } from './../../../components/model/consulta-request.model';
import { MensagemDados } from './../../geral/mensagem/mensagem.model';
import { Observable } from 'rxjs/Observable';
import { LoginService } from './../../../security/login.service';
import { AbstractCrudService } from '../../../components/services/abstract-crud.service';
import { Multa, MultaDados } from './multa.model';
import { Injectable, Inject } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class MultaService extends AbstractCrudService<Multa> {

  constructor( @Inject(Http) _http: Http, @Inject(LoginService) loginService: LoginService) {
    super(_http, loginService);
  }

  getEntityName(): string {
    return "multa";
  }

  getDados(): Observable<MultaDados> {
    return this._http.post(this.getUrl() + '/dados', null, { headers: this.getHeaders() })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  getCidades(consultaRequest: ConsultaRequest) : Observable<MultaDados> {
    return this._http.post(this.getUrl() + '/cidades', JSON.stringify(consultaRequest), { headers: this.getHeaders() })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }
}
