import { MultaService } from './../multa.service';
import { Multa } from './../multa.model';
import { BlockUiService } from './../../../../plugins/block-ui/block-ui.service';
import { Lancamento } from './../../../financeiro/lancamento/lancamento.model';
import { TituloPaginaService } from './../../../../components/services/titulo-pagina.service';
import { CategoriaService } from './../../../configuracoes/categoria/categoria.service';
import { ClienteService } from './../../../cadastros/cliente/cliente.service';
import { DataPaginationEvent } from './../../../../plugins/plugins.api';
import { MensagemService } from './../../../../plugins/forms/mensagem.service';
import { ActivatedRoute, Router } from '@angular/router';
import { EnumUtils, Utils } from './../../../../plugins/shared';
import { Subscription } from 'rxjs/Rx';
import { DataTableComponent } from './../../../../plugins/data-table/data-table.component';
import { ConsultaResponse } from './../../../../components/model/consulta-response.model';
import { SelectGroupOption } from './../../../../plugins/forms/select-group/select-group-option';
import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-multa-consulta',
  templateUrl: './multa-consulta.component.html',
  styleUrls: ['./multa-consulta.component.scss']
})
export class MultaConsultaComponent implements OnDestroy, AfterViewInit {

  // Colecoes
  itensStatus: SelectGroupOption[] = EnumUtils.getOptionsAtivo("TODOS");
  itensStatusViagem: SelectGroupOption[] = EnumUtils.getOptionsStatusViagemAprovacao("TODOS");
  itensCategoria: SelectGroupOption[] = [];
  itensTipo: SelectGroupOption[] = EnumUtils.getOptionsTipoLancamento("TODOS");

  dados: ConsultaResponse<Multa>;

  public id: any;

  filtro: any = {
  };

  order: any = { "dataHora": "desc" };

  private inscricoes: Subscription[] = [];

  // Recupera o componente dataTable
  @ViewChild(DataTableComponent) dt: DataTableComponent<Multa>;

  constructor(
    private tituloPaginaService: TituloPaginaService,
    private route: ActivatedRoute,
    private router: Router,
    private blockService: BlockUiService,
    private msg: MensagemService,
    private multaService: MultaService) { }

  ngOnInit() {
    //
  }

  ngAfterViewInit() { }

  ngOnDestroy() {
    for (let s of this.inscricoes) {
      s.unsubscribe();
    }
  }

  pesquisar() {
    if (this.filtro.categoria$id) {
      this.filtro.tipoLancamento = null;
    }

    this.dt.load(1);
  }

  limpar() {
    this.filtro = {};
    this.pesquisar();
  }


  editar() {
    if (this.dt.getSelectedItem()) {
      this.router.navigate(['editar', this.dt.getSelectedItem().id], { relativeTo: this.route });
    } else {
      this.msg.nofiticationWarning('Nenhum item selecionado para alterar!');
    }
  }

  excluir() {
    if (this.dt.getSelectedItem()) {
      this.router.navigate(['excluir', this.dt.getSelectedItem().id], { relativeTo: this.route });
    } else {
      this.msg.nofiticationWarning('Nenhum item selecionado para excluir!');
    }
  }

  novo() {
    this.router.navigate(['novo'], { relativeTo: this.route });
  }

  paginacao(event: DataPaginationEvent) {
    this.blockService.block('.table-consulta');

    let s = this.multaService.getList({
      filtro: this.filtro,
      order: this.order,
      pageSize: event.pageSize,
      pageNumber: event.page
    }).subscribe((dados) => {
      this.dados = dados;

      this.blockService.unBlock('.table-consulta');
    }, (error) => {
      this.dados = {
        lista: [],
        total: 0
      };

      this.blockService.unBlock('.table-consulta');
    });
  }
}
