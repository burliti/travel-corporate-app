import { AutenticacaoGuard } from './../../../security/autenticacao.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MultaComponent } from './multa.component';
import { MultaConsultaComponent } from './multa-consulta/multa-consulta.component';
import { MultaCadastroComponent } from './multa-cadastro/multa-cadastro.component';

const routes: Routes = [{
  path: 'financeiro/multa', component: MultaComponent, canActivate: [AutenticacaoGuard], children: [
    { path: '', component: MultaConsultaComponent },
    {
      path: ':acao', children: [
        { path: '', component: MultaCadastroComponent },
        { path: ':id', component: MultaCadastroComponent }
      ]
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MultaRoutingModule { }
