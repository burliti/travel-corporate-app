import { TituloPaginaService } from './../../../components/services/titulo-pagina.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-multa',
  templateUrl: './multa.component.html',
  styleUrls: ['./multa.component.scss']
})
export class MultaComponent implements OnInit {

  constructor(private ts: TituloPaginaService) { }

  ngOnInit() {
    this.ts.setTitulo("Multas");
  }
}
