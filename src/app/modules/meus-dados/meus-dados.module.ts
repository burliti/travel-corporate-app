import { NgModule } from '@angular/core';
import { MeusDadosService } from './meus-dados.service';

@NgModule({
  imports: [],
  exports: [],
  declarations: [],
  providers: [MeusDadosService],
})
export class MeusDadosModule { }
