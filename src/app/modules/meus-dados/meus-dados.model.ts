import { Viagem } from "../financeiro/viagem/viagem.model";

export class MeusDados {
  ultimaViagem?: Viagem;
  ultimaViagemAberta?: Viagem;
  saldo?: number;
  saldoUltimaViagem?: number;
  viagensAberto?: number;
}
