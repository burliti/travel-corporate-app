import { Observable } from 'rxjs/Rx';
import { LoginService } from './../../security/login.service';
import { Http, Headers, Response } from '@angular/http';
import { Injectable, Inject } from '@angular/core';
import { MeusDados } from './meus-dados.model';

@Injectable()
export class MeusDadosService {

  constructor(private _http: Http, private loginService: LoginService) {
  }

  getHeaders(): Headers {
    // cria uma instância de Headers
    const headers = new Headers();
    // Adiciona o tipo de conteúdo application/json
    headers.append('Content-Type', 'application/json');
    // Adiciona o token de autenticacao
    if (this.loginService.getUsuarioAutenticado() != null) {
      headers.append('AUTHENTICATION_TOKEN', this.loginService.getUsuarioAutenticado().chaveSessao);
    }

    return headers;
  }

  // /**
  //  * Retorna os meus dados
  //  */
  // getMeusDados(): Observable<MeusDados> {
  //   return this._http.post('/api/resources/meus-dados/v1',
  //     null,
  //     { headers: this.getHeaders() })
  //     .map(res => res.json())
  //     .catch((error: any) => Observable.throw(error || 'Server error'));
  // }

  /**
   * Retorna os meus dados
   */
  async getMeusDados(): Promise<MeusDados> {
    const response = await this._http.post('/api/resources/meus-dados/v1',
      null,
      { headers: this.getHeaders() }).toPromise();
    return response.json();
  }
}
