import { ConfiguracaoAprovacaoViagemCadastroComponent } from './configuracao-aprovacao-viagem-cadastro/configuracao-aprovacao-viagem-cadastro.component';
import { ConfiguracaoAprovacaoViagemConsultaComponent } from './configuracao-aprovacao-viagem-consulta/configuracao-aprovacao-viagem-consulta.component';
import { AutenticacaoGuard } from './../../../security/autenticacao.guard';
import { ConfiguracaoAprovacaoViagemComponent } from './configuracao-aprovacao-viagem.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{
  path: 'configuracoes/configuracao-aprovacao-viagem', component: ConfiguracaoAprovacaoViagemComponent, canActivate: [AutenticacaoGuard], children: [
    { path: '', component: ConfiguracaoAprovacaoViagemConsultaComponent },
    {
      path: ':acao', children: [
        { path: '', component: ConfiguracaoAprovacaoViagemCadastroComponent },
        { path: ':id', component: ConfiguracaoAprovacaoViagemCadastroComponent }
      ]
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfiguracaoAprovacaoViagemRoutingModule { }
