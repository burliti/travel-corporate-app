import { ConfiguracaoAprovacaoViagemService } from './configuracao-aprovacao-viagem.service';
import { PluginsModule } from './../../../plugins/plugins.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfiguracaoAprovacaoViagemRoutingModule } from './configuracao-aprovacao-viagem-routing.module';
import { ConfiguracaoAprovacaoViagemComponent } from './configuracao-aprovacao-viagem.component';
import { ConfiguracaoAprovacaoViagemCadastroComponent } from './configuracao-aprovacao-viagem-cadastro/configuracao-aprovacao-viagem-cadastro.component';
import { ConfiguracaoAprovacaoViagemConsultaComponent } from './configuracao-aprovacao-viagem-consulta/configuracao-aprovacao-viagem-consulta.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    PluginsModule,
    FormsModule,
    ConfiguracaoAprovacaoViagemRoutingModule
  ],
  providers: [ConfiguracaoAprovacaoViagemService],
  declarations: [ConfiguracaoAprovacaoViagemComponent, ConfiguracaoAprovacaoViagemCadastroComponent, ConfiguracaoAprovacaoViagemConsultaComponent]
})
export class ConfiguracaoAprovacaoViagemModule { }
