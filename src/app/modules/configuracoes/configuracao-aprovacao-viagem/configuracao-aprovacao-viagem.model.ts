import { Funcionario } from './../../cadastros/funcionario/funcionario.model';
import { Empresa } from './../empresa/empresa.model';

export class ConfiguracaoAprovacaoViagem {
  id?: number;
  filial?: Empresa = {};
  possuiAprovacaoNivel?: string;
  funcionarios?: FuncionariosAprovacao[] = [];
}

export class FuncionariosAprovacao {
  id?: number;
  funcionario?: Funcionario = {};
  prazoAprovacaoHoras?: number;
  ordem?: number;
}

export class ConfigDados {
  funcionarios?: Funcionario[];
  empresas?: Empresa[];
}
