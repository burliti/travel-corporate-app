import { ConfiguracaoAprovacaoViagemService } from './../configuracao-aprovacao-viagem.service';
import { ConfiguracaoAprovacaoViagem, FuncionariosAprovacao } from './../configuracao-aprovacao-viagem.model';
import { BlockUiService } from './../../../../plugins/block-ui/block-ui.service';
import { MensagemService } from './../../../../plugins/forms/mensagem.service';
import { SelectGroupOption } from './../../../../plugins/forms/select-group/select-group-option';
import { Subscription } from 'rxjs/Rx';
import { Component, OnInit } from '@angular/core';
import { AbstractCadastroComponent } from '../../../../components/geral/crud/abstract-cadastro.component';
import { EnumUtils } from '../../../../plugins/shared';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-configuracao-aprovacao-viagem-cadastro',
  templateUrl: './configuracao-aprovacao-viagem-cadastro.component.html',
  styleUrls: ['./configuracao-aprovacao-viagem-cadastro.component.scss']
})
export class ConfiguracaoAprovacaoViagemCadastroComponent extends AbstractCadastroComponent<ConfiguracaoAprovacaoViagem, ConfiguracaoAprovacaoViagemService> {

  inscricoes: Subscription[] = [];

  // Colecoes
  itensStatus: SelectGroupOption[] = EnumUtils.getOptionsAtivo();
  itensSimNao: SelectGroupOption[] = EnumUtils.getOptionsSimNao();
  itensEmpresa: SelectGroupOption[] = [];
  itensFuncionario: SelectGroupOption[] = [];
  itensTipoLancamento: SelectGroupOption[] = EnumUtils.getOptionsTipoLancamento("Selecione uma opção");

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private blockService: BlockUiService,
    private configuracaoService: ConfiguracaoAprovacaoViagemService,
    private msg: MensagemService) {
    super(route, router, blockService, msg);
  }

  ngOnInit() {
    this.itensEmpresa.push({ label: '[Selecione a filial]' });
    this.itensFuncionario.push({ label: '[Selecione o funcionário]' });

    const s1 = this.configuracaoService.getDados().subscribe((dados) => {
      dados.empresas.forEach((empresa) => this.itensEmpresa.push({ label: empresa.nome, value: empresa.id }));
      dados.funcionarios.forEach((funcionario) => this.itensFuncionario.push({ label: funcionario.nome, value: funcionario.id }));
      super.ngOnInit();
    });

    this.inscricoes.push(s1);
  }

  beforeVisualizar(vo: ConfiguracaoAprovacaoViagem) {
    if (!vo.filial) {
      vo.filial = {};
    }
  }

  getService(): ConfiguracaoAprovacaoViagemService {
    return this.configuracaoService;
  }

  getVoNewInstance(): ConfiguracaoAprovacaoViagem {
    return {
      possuiAprovacaoNivel: 'Não',
      filial: {},
      funcionarios: []
    };
  }

  adicionarNivel() {
    this.vo.funcionarios.push({
      funcionario: {},
      prazoAprovacaoHoras: 0,
      ordem: this.vo.funcionarios.length + 1
    });
  }

  removerNivel(item: FuncionariosAprovacao) {
    let index = this.vo.funcionarios.indexOf(item);
    this.vo.funcionarios.splice(index, 1);
  }
}
