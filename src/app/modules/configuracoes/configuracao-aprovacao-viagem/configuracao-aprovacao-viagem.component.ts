import { TituloPaginaService } from './../../../components/services/titulo-pagina.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-configuracao-aprovacao-viagem',
  templateUrl: './configuracao-aprovacao-viagem.component.html',
  styleUrls: ['./configuracao-aprovacao-viagem.component.scss']
})
export class ConfiguracaoAprovacaoViagemComponent implements OnInit {

  constructor(private ts: TituloPaginaService) { }

  ngOnInit() {
    this.ts.setTitulo('Configurações - Aprovação de Viagem');
  }

}
