import { Observable } from 'rxjs/Observable';
import { LoginService } from './../../../security/login.service';
import { Http } from '@angular/http';
import { Injectable, Inject } from '@angular/core';
import { AbstractCrudService } from '../../../components/services/abstract-crud.service';
import { ConfiguracaoAprovacaoViagem, ConfigDados } from './configuracao-aprovacao-viagem.model';

@Injectable()
export class ConfiguracaoAprovacaoViagemService extends AbstractCrudService<ConfiguracaoAprovacaoViagem> {

  constructor(@Inject(Http) _http: Http, @Inject(LoginService) loginService: LoginService) {
    super(_http, loginService);
  }

  getEntityName(): string {
    return "configuracaoAprovacaoViagem";
  }

  getDados(): Observable<ConfigDados> {
    return this._http.post(this.getUrl() + '/dados', null, { headers: this.getHeaders() })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }
}
