import { SetorService } from './setor.service';
import { PluginsModule } from './../../../plugins/plugins.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SetorRoutingModule } from './setor-routing.module';
import { SetorComponent } from './setor.component';
import { SetorCadastroComponent } from './setor-cadastro/setor-cadastro.component';
import { SetorConsultaComponent } from './setor-consulta/setor-consulta.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PluginsModule,
    SetorRoutingModule
  ],
  providers: [SetorService],
  declarations: [SetorComponent, SetorCadastroComponent, SetorConsultaComponent]
})
export class SetorModule { }
