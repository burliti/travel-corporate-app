import { Observable } from 'rxjs/Rx';
import { Empresa } from './../empresa/empresa.model';
import { LoginService } from './../../../security/login.service';
import { Http } from '@angular/http';
import { Injectable, Inject } from '@angular/core';
import { Setor } from './setor.model';
import { AbstractCrudService } from '../../../components/services/abstract-crud.service';
import { CentroCusto } from '../../cadastros/centro-custo/centro-custo.model';

@Injectable()
export class SetorService extends AbstractCrudService<Setor> {

  constructor( @Inject(Http) _http: Http, @Inject(LoginService) loginService: LoginService) {
    super(_http, loginService);
  }

  getEntityName(): string {
    return "setor";
  }

  getFiliais(): Observable<Empresa[]> {
    return this._http.post(this.getUrl() + '/filiais',
      null,
      { headers: this.getHeaders() })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  getCentrosCusto(): Observable<CentroCusto[]> {
    return this._http.post(this.getUrl() + '/centroCusto',
      null,
      { headers: this.getHeaders() })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

}
