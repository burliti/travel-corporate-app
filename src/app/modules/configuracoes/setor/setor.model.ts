import { Empresa } from './../empresa/empresa.model';
import { CentroCusto } from '../../cadastros/centro-custo/centro-custo.model';

export class Setor {
  id?: string;
  nome?: string;
  status?: string;
  filial?: Empresa;
  centroCusto?: CentroCusto;
}
