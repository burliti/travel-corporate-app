import { SetorService } from './../setor.service';
import { Setor } from './../setor.model';
import { BlockUiService } from './../../../../plugins/block-ui/block-ui.service';
import { SelectGroupComponent } from './../../../../plugins/forms/select-group/select-group.component';
import { MensagemService } from './../../../../plugins/forms/mensagem.service';
import { SelectGroupOption } from './../../../../plugins/forms/select-group/select-group-option';
import { Subscription } from 'rxjs/Rx';
import { Component, OnInit } from '@angular/core';
import { AbstractCadastroComponent } from '../../../../components/geral/crud/abstract-cadastro.component';
import { EnumUtils } from '../../../../plugins/shared';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-setor-cadastro',
  templateUrl: './setor-cadastro.component.html',
  styleUrls: ['./setor-cadastro.component.css']
})
export class SetorCadastroComponent extends AbstractCadastroComponent<Setor, SetorService> {

  inscricoes: Subscription[] = [];

  // Colecoes
  itensStatus: SelectGroupOption[] = EnumUtils.getOptionsAtivo();
  itensFilial: SelectGroupOption[] = [];
  itensCentroCusto: SelectGroupOption[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private blockService: BlockUiService,
    private setorService: SetorService,
    private msg: MensagemService) {
    super(route, router, blockService, msg);
  }

  ngOnInit() {
    this.itensFilial.push({ label: '[NENHUM]', value: '' });
    this.itensCentroCusto.push({ label: '[NENHUM]', value: '' });

    let s1 = this.getService()
      .getFiliais()
      .subscribe((response) => {
        response.forEach((item) => this.itensFilial.push({ label: item.nome, value: item.id }));

        let s2 = this.getService()
          .getCentrosCusto()
          .subscribe((response) => {
            response.forEach((item) => this.itensCentroCusto.push({ label: item.codigo + ' - ' + item.nome, value: item.id.toString() }))
          });

        this.inscricoes.push(s2);

        super.ngOnInit();
      });

    this.inscricoes.push(s1);
  }

  beforeVisualizar(vo: Setor) {
    if (!vo.filial) {
      vo.filial = {};
    }

    if (!vo.centroCusto) {
      vo.centroCusto = {};
    }
  }

  getService(): SetorService {
    return this.setorService;
  }

  getVoNewInstance(): Setor {
    return {
      filial: {},
      centroCusto: {},
      status: 'Ativo'
    };
  }
}
