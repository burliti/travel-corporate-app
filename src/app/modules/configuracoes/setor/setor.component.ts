import { TituloPaginaService } from './../../../components/services/titulo-pagina.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-setor',
  templateUrl: './setor.component.html',
  styleUrls: ['./setor.component.css']
})
export class SetorComponent implements OnInit {

  constructor(private tituloPaginaService: TituloPaginaService) { }

  ngOnInit() {
    this.tituloPaginaService.setTitulo("Setores");
  }

}
