import { AutenticacaoGuard } from './../../../security/autenticacao.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SetorComponent } from './setor.component';
import { SetorConsultaComponent } from './setor-consulta/setor-consulta.component';
import { SetorCadastroComponent } from './setor-cadastro/setor-cadastro.component';

const routes: Routes = [{
  path: 'configuracoes/setor', component: SetorComponent, canActivate: [AutenticacaoGuard], children: [
    { path: '', component: SetorConsultaComponent },
    {
      path: ':acao', children: [
        { path: '', component: SetorCadastroComponent },
        { path: ':id', component: SetorCadastroComponent }
      ]
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SetorRoutingModule { }
