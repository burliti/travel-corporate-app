import { Categoria } from './categoria.model';
import { Observable } from 'rxjs';
import { ConsultaResponse } from './../../../components/model/consulta-response.model';
import { Injectable, Inject } from '@angular/core';
import { AbstractCrudService } from '../../../components/services/abstract-crud.service';
import { Http } from '@angular/http';
import { LoginService } from '../../../security/login.service';

@Injectable()
export class CategoriaService extends AbstractCrudService<Categoria> {

  constructor( @Inject(Http) _http: Http, @Inject(LoginService) loginService: LoginService) {
    super(_http, loginService);
  }

  getEntityName(): string {
    return "categoria";
  }

  getListAtivos(): Observable<ConsultaResponse<Categoria>> {
    return this.getList({
      filtro: { "status": "Ativo" },
      order: { "nome": "asc" },
      pageSize: null,
      pageNumber: null
    });
  }

  getListDespesasAtivos(): Observable<ConsultaResponse<Categoria>> {
    return this.getList({
      filtro: { "status": "Ativo", "tipo": "Débito" },
      order: { "nome": "asc" },
      pageSize: null,
      pageNumber: null
    });
  }

}
