import { BlockUiService } from './../../../../plugins/block-ui/block-ui.service';
import { CategoriaService } from './../categoria.service';
import { Categoria } from './../categoria.model';
import { MensagemService } from './../../../../plugins/forms/mensagem.service';
import { SelectGroupOption } from './../../../../plugins/forms/select-group/select-group-option';
import { Subscription } from 'rxjs/Rx';
import { Component, OnInit } from '@angular/core';
import { AbstractCadastroComponent } from '../../../../components/geral/crud/abstract-cadastro.component';
import { EnumUtils } from '../../../../plugins/shared';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-categoria-cadastro',
  templateUrl: './categoria-cadastro.component.html',
  styleUrls: ['./categoria-cadastro.component.css']
})
export class CategoriaCadastroComponent extends AbstractCadastroComponent<Categoria, CategoriaService> {

  inscricoes: Subscription[] = [];

  // Colecoes
  itensStatus: SelectGroupOption[] = EnumUtils.getOptionsAtivo();
  itensSimNao: SelectGroupOption[] = EnumUtils.getOptionsSimNao();
  itensTipoLancamento: SelectGroupOption[] = EnumUtils.getOptionsTipoLancamento("Selecione uma opção");

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private blockService: BlockUiService,
    private categoriaService: CategoriaService,
    private msg: MensagemService) {
    super(route, router, blockService, msg);
  }

  getService(): CategoriaService {
    return this.categoriaService;
  }

  getVoNewInstance(): Categoria {
    return {
      status: 'Ativo',
    };
  }
}
