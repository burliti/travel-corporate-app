import { TituloPaginaService } from './../../../components/services/titulo-pagina.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-categoria',
  templateUrl: './categoria.component.html',
  styleUrls: ['./categoria.component.css']
})
export class CategoriaComponent implements OnInit {

  constructor(private tituloPaginaService : TituloPaginaService) { }

  ngOnInit() {
    this.tituloPaginaService.setTitulo("Categorias");
  }

}
