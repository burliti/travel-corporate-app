import { CategoriaCadastroComponent } from './categoria-cadastro/categoria-cadastro.component';
import { CategoriaConsultaComponent } from './categoria-consulta/categoria-consulta.component';
import { AutenticacaoGuard } from './../../../security/autenticacao.guard';
import { CategoriaComponent } from './categoria.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{
  path: 'configuracoes/categoria', component: CategoriaComponent, canActivate: [AutenticacaoGuard], children: [
    { path: '', component: CategoriaConsultaComponent },
    {
      path: ':acao', children: [
        { path: '', component: CategoriaCadastroComponent },
        { path: ':id', component: CategoriaCadastroComponent }
      ]
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoriaRoutingModule { }
