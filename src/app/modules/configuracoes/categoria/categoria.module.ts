import { CategoriaService } from './categoria.service';
import { FormsModule } from '@angular/forms';
import { PluginsModule } from './../../../plugins/plugins.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoriaRoutingModule } from './categoria-routing.module';
import { CategoriaComponent } from './categoria.component';
import { CategoriaCadastroComponent } from './categoria-cadastro/categoria-cadastro.component';
import { CategoriaConsultaComponent } from './categoria-consulta/categoria-consulta.component';

@NgModule({
  imports: [
    CommonModule,
    PluginsModule,
    FormsModule,
    CategoriaRoutingModule
  ],
  providers: [CategoriaService],
  declarations: [CategoriaComponent, CategoriaCadastroComponent, CategoriaConsultaComponent]
})
export class CategoriaModule { }
