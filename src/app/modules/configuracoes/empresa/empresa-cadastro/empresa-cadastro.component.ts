import { BlockUiService } from './../../../../plugins/block-ui/block-ui.service';
import { EmpresaService } from './../empresa.service';
import { Empresa } from './../empresa.model';
import { MensagemService } from './../../../../plugins/forms/mensagem.service';
import { SelectGroupOption } from './../../../../plugins/forms/select-group/select-group-option';
import { Subscription } from 'rxjs/Rx';
import { Component, OnInit } from '@angular/core';
import { AbstractCadastroComponent } from '../../../../components/geral/crud/abstract-cadastro.component';
import { EnumUtils } from '../../../../plugins/shared';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-empresa-cadastro',
  templateUrl: './empresa-cadastro.component.html',
  styleUrls: ['./empresa-cadastro.component.css']
})
export class EmpresaCadastroComponent extends AbstractCadastroComponent<Empresa, EmpresaService> {

  inscricoes: Subscription[] = [];
  itensCentroCusto: SelectGroupOption[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private blockService: BlockUiService,
    private empresaService: EmpresaService,
    private msg: MensagemService) {
    super(route, router, blockService, msg);
  }

  getService(): EmpresaService {
    return this.empresaService;
  }

  getVoNewInstance(): Empresa {
    return {
      centroCusto: {}
    };
  }

  beforeVisualizar(vo: Empresa) {
    if (!vo.centroCusto) {
      vo.centroCusto = {};
    }
  }

  ngOnInit() {
    this.itensCentroCusto.push({ label: '[NENHUM]', value: '' });

    let s2 = this.getService()
      .getCentrosCusto()
      .subscribe((response) => {
        response.forEach((item) => this.itensCentroCusto.push({ label: item.codigo + ' - ' + item.nome, value: item.id.toString() }))
      });

    this.inscricoes.push(s2);

    super.ngOnInit();
  }

  isPodeEditar() {
    return this.isExcluir() ? false : true;
  }

  getFoto() {
    if (!this.vo || !this.vo.logo || this.vo.logo === '') {
      return 'assets/img/sem_imagem.png';
    } else {
      return this.vo.logo;
    }
  }
}

