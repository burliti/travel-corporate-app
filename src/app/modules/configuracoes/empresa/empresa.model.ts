import { CentroCusto } from "../../cadastros/centro-custo/centro-custo.model";

export class Empresa {
  id?: string;
  nome?: string;
  fantasia?: string;
  chaveEmpresa?: string;
  logo?: string;
  centroCusto?: CentroCusto;
}
