import { CentroCusto } from './../../cadastros/centro-custo/centro-custo.model';
import { Empresa } from './empresa.model';
import { Observable } from 'rxjs';
import { ConsultaResponse } from './../../../components/model/consulta-response.model';
import { Injectable, Inject } from '@angular/core';
import { AbstractCrudService } from '../../../components/services/abstract-crud.service';
import { Http } from '@angular/http';
import { LoginService } from '../../../security/login.service';

@Injectable()
export class EmpresaService extends AbstractCrudService<Empresa> {

  constructor( @Inject(Http) _http: Http, @Inject(LoginService) loginService: LoginService) {
    super(_http, loginService);
  }

  getEntityName(): string {
    return "empresa";
  }

  getCentrosCusto(): Observable<CentroCusto[]> {
    return this._http.post(this.getUrl() + '/centroCusto',
      null,
      { headers: this.getHeaders() })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }
}
