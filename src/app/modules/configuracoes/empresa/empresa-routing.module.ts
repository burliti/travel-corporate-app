import { EmpresaCadastroComponent } from './empresa-cadastro/empresa-cadastro.component';
import { EmpresaConsultaComponent } from './empresa-consulta/empresa-consulta.component';
import { EmpresaComponent } from './empresa.component';
import { AutenticacaoGuard } from './../../../security/autenticacao.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{
  path: 'configuracoes/empresa', component: EmpresaComponent, canActivate: [AutenticacaoGuard], children: [
    { path: '', component: EmpresaConsultaComponent },
    {
      path: ':acao', children: [
        { path: '', component: EmpresaCadastroComponent },
        { path: ':id', component: EmpresaCadastroComponent }
      ]
    }
  ]
}];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmpresaRoutingModule { }
