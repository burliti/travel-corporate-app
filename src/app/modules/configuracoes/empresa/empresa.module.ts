import { EmpresaService } from './empresa.service';
import { FormsModule } from '@angular/forms';
import { PluginsModule } from './../../../plugins/plugins.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmpresaRoutingModule } from './empresa-routing.module';
import { EmpresaComponent } from './empresa.component';
import { EmpresaCadastroComponent } from './empresa-cadastro/empresa-cadastro.component';
import { EmpresaConsultaComponent } from './empresa-consulta/empresa-consulta.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PluginsModule,
    EmpresaRoutingModule
  ],
  providers: [EmpresaService],
  declarations: [EmpresaComponent, EmpresaCadastroComponent, EmpresaConsultaComponent]
})
export class EmpresaModule { }
