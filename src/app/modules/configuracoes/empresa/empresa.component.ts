import { TituloPaginaService } from './../../../components/services/titulo-pagina.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-empresa',
  templateUrl: './empresa.component.html',
  styleUrls: ['./empresa.component.css']
})
export class EmpresaComponent implements OnInit {

  constructor(private tituloPaginaService : TituloPaginaService) { }

  ngOnInit() {
    this.tituloPaginaService.setTitulo("Empresas");
  }

}
