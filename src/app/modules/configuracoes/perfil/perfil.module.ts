import { PerfilService } from './perfil.service';
import { PluginsModule } from './../../../plugins/plugins.module';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PerfilRoutingModule } from './perfil-routing.module';
import { PerfilComponent } from './perfil.component';
import { PerfilCadastroComponent } from './perfil-cadastro/perfil-cadastro.component';
import { PerfilConsultaComponent } from './perfil-consulta/perfil-consulta.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PluginsModule,
    PerfilRoutingModule
  ],
  providers: [PerfilService],
  declarations: [PerfilComponent, PerfilCadastroComponent, PerfilConsultaComponent]
})
export class PerfilModule { }
