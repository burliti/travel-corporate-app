import { BlockUiService } from './../../../../plugins/block-ui/block-ui.service';
import { element } from 'protractor';
import { Recurso, AcaoRecurso } from './../../recurso/recurso.model';
import { PerfilService } from './../perfil.service';
import { Perfil } from './../perfil.model';
import { MensagemService } from './../../../../plugins/forms/mensagem.service';
import { SelectGroupOption } from './../../../../plugins/forms/select-group/select-group-option';
import { Subscription } from 'rxjs/Rx';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AbstractCadastroComponent } from '../../../../components/geral/crud/abstract-cadastro.component';
import { EnumUtils } from '../../../../plugins/shared';
import { ActivatedRoute, Router } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-perfil-cadastro',
  templateUrl: './perfil-cadastro.component.html',
  styleUrls: ['./perfil-cadastro.component.css']
})
export class PerfilCadastroComponent extends AbstractCadastroComponent<Perfil, PerfilService>  {

  inscricoes: Subscription[] = [];

  // Colecoes
  itensStatus: SelectGroupOption[] = EnumUtils.getOptionsAtivo();
  itensSimNao: SelectGroupOption[] = EnumUtils.getOptionsSimNao();
  itensTipoLancamento: SelectGroupOption[] = EnumUtils.getOptionsTipoLancamento("Selecione uma opção");

  tree: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private blockService: BlockUiService,
    private perfilService: PerfilService,
    private msg: MensagemService) {
    super(route, router, blockService, msg);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  getService(): PerfilService {
    return this.perfilService;
  }

  beforeVisualizar(vo: Perfil) {

    super.beforeVisualizar(vo);

    if (this.isNovo()) {
      // Deve ser novo...
      vo.nodes = [];

      const s = this.perfilService.getRecursos().subscribe((retorno) => {
        vo.nodes = retorno;

        this.bindTreeView(vo);
      });

      this.inscricoes.push(s);
    } else {
      this.bindTreeView(vo);
    }
  }

  private bindTreeView(vo: Perfil) {
    console.log(vo);

    if (!this.isExcluir()) {
      this.tree = $('#treePermissoes').tree({
        textField: 'nome',
        uiLibrary: 'bootstrap4',
        iconsLibrary: 'glyphicons',
        primaryKey: 'id',
        childrenField: 'filhos',
        dataSource: vo.nodes,
        checkboxes: !this.isExcluir(),
        cascadeCheck: false,
        cascadeSelection: false
      });

      if (!this.tree || !this.tree.check) {
        return;
      }

      let nodes = vo.nodes ? vo.nodes : [];
      let oTree = this.tree;

      setTimeout(function () {
        // Recurso pai
        nodes.forEach(recurso => {
          if (recurso.selecionado) {
            oTree.check(oTree.getNodeById(recurso.id));
          }
          // Recurso filho
          recurso.filhos.forEach(recursoFilho => {
            if (recursoFilho.selecionado) {
              oTree.check(oTree.getNodeById(recursoFilho.id));
            }
            // Ações
            recursoFilho.filhos.forEach(acao => {
              if (acao.selecionado) {
                oTree.check(oTree.getNodeById(acao.id));
              }
            });
          });
        });
      }, 500);
    }
  }

  salvar() {
    this.saveRecursosAcoes();
    super.salvar();
  }

  /**
   * Le os check's id's e salva no VO de perfil.
   */
  saveRecursosAcoes() {
    if (this.tree) {
      let checkedIds: string[] = this.tree.getCheckedNodes();

      let recursos: Recurso[] = [];
      let acoes: AcaoRecurso[] = [];

      checkedIds.forEach(element => {
        if (element.startsWith('acaorecurso-')) {
          let id = element.split('-')[1];
          let acaoRecurso: AcaoRecurso = { id: id };

          acoes.push(acaoRecurso);
        } else if (element.startsWith('recurso-')) {
          let id = element.split('-')[1];
          let recurso: Recurso = { id: id };

          recursos.push(recurso);
        }
      });

      this.vo.acoes = acoes;
      this.vo.recursos = recursos;
    }
  }

  marcarTodos() {
    this.tree.checkAll();
  }

  desmarcarTodos() {
    this.tree.uncheckAll();
  }

  getVoNewInstance(): Perfil {
    return {
      status: 'Ativo',
      nodes: []
    };
  }
}

