import { Node } from './../recurso/node.model';
import { Recurso } from './../recurso/recurso.model';
import { Perfil } from './perfil.model';
import { Observable } from 'rxjs';
import { ConsultaResponse } from './../../../components/model/consulta-response.model';
import { Injectable, Inject } from '@angular/core';
import { AbstractCrudService } from '../../../components/services/abstract-crud.service';
import { Http } from '@angular/http';
import { LoginService } from '../../../security/login.service';

@Injectable()
export class PerfilService extends AbstractCrudService<Perfil> {

  constructor( @Inject(Http) _http: Http, @Inject(LoginService) loginService: LoginService) {
    super(_http, loginService);
  }

  getEntityName(): string {
    return "perfil";
  }

  getRecursos(): Observable<Node[]> {
    return this._http.post(this.getUrl() + '/recursos',
      null,
      { headers: this.getHeaders() })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  getListAtivos(): Observable<ConsultaResponse<Perfil>> {
    return this.getList({
      filtro: { "status": "Ativo" },
      order: { "nome": "asc" },
      pageSize: null,
      pageNumber: null
    });
  }
}
