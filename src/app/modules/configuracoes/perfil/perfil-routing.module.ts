import { PerfilCadastroComponent } from './perfil-cadastro/perfil-cadastro.component';
import { PerfilConsultaComponent } from './perfil-consulta/perfil-consulta.component';
import { AutenticacaoGuard } from './../../../security/autenticacao.guard';
import { PerfilComponent } from './perfil.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [{
  path: 'configuracoes/perfil', component: PerfilComponent, canActivate: [AutenticacaoGuard], children: [
    { path: '', component: PerfilConsultaComponent },
    {
      path: ':acao', children: [
        { path: '', component: PerfilCadastroComponent },
        { path: ':id', component: PerfilCadastroComponent }
      ]
    }
  ]
}];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PerfilRoutingModule { }
