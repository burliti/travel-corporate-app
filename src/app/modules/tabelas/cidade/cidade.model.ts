import { Estado } from './../estado/estado.model';

export class Cidade {
  id?: number;
  nome?: string;
  estado?: Estado;
}
