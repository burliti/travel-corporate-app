import { ReciboModule } from './../../financeiro/recibo/recibo.module';
import { MinhasViagensService } from './minhas-viagens.service';
import { FormsModule } from '@angular/forms';
import { PluginsModule } from './../../../plugins/plugins.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MinhasViagensRoutingModule } from './minhas-viagens-routing.module';
import { MinhasViagensComponent } from './minhas-viagens.component';
import { MinhasViagensCadastroComponent } from './minhas-viagens-cadastro/minhas-viagens-cadastro.component';
import { MinhasViagensConsultaComponent } from './minhas-viagens-consulta/minhas-viagens-consulta.component';

@NgModule({
  imports: [
    CommonModule,
    PluginsModule,
    FormsModule,
    ReciboModule,
    MinhasViagensRoutingModule
  ],
  providers :[MinhasViagensService],
  declarations: [MinhasViagensComponent, MinhasViagensCadastroComponent, MinhasViagensConsultaComponent]
})
export class MinhasViagensModule { }
