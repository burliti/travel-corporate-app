import { Utils } from './../../../plugins/shared';
import { Projeto } from './../../cadastros/projeto/projeto.model';
import { Cliente } from './../../cadastros/cliente/cliente.model';
import { Viagem } from './../../financeiro/viagem/viagem.model';
import { CadastroResponse } from './../../../components/model/cadastro-response.model';
import { Lancamento } from './../../financeiro/lancamento/lancamento.model';
import { Observable } from 'rxjs';
import { ConsultaResponse } from './../../../components/model/consulta-response.model';
import { Injectable, Inject } from '@angular/core';
import { AbstractCrudService } from '../../../components/services/abstract-crud.service';
import { Http } from '@angular/http';
import { LoginService } from '../../../security/login.service';
import { Categoria } from '../../configuracoes/categoria/categoria.model';

@Injectable()
export class MinhasViagensService extends AbstractCrudService<Viagem> {

  constructor( @Inject(Http) _http: Http, @Inject(LoginService) loginService: LoginService) {
    super(_http, loginService);
  }

  getEntityName(): string {
    return "minhas-viagens";
  }

  /**
   * Retorna a lista de lancamentos
   */
  getLancamentos(idFechamento: string): Observable<Lancamento[]> {
    return this._http.post(this.getUrl() + '/lancamentos/' + idFechamento,
      null,
      { headers: this.getHeaders() })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  /**
   * Finaliza o Relatório de Viagem
   * @param vo Entidade de fechamento
   */
  finalizar(vo: Viagem): Observable<CadastroResponse<Viagem>> {
    return this._http.post(this.getUrl() + '/finalizar', JSON.stringify(vo), { headers: this.getHeaders() })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  /**
    * Retorna a lista de clientes
    */
  getClientes(): Observable<Cliente[]> {
    return this._http.get(this.getUrl() + '/clientes',
      { headers: this.getHeaders() })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  /**
   * Retorna a lista de projetos
   */
  getProjetos(): Observable<Projeto[]> {
    return this._http.get(this.getUrl() + '/projetos',
      { headers: this.getHeaders() })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

   /**
   * Retorna a lista de categorias
   */
  getCategorias(): Observable<Categoria[]> {
    return this._http.get(this.getUrl() + '/categorias',
      { headers: this.getHeaders() })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  calcularSaldos(vo: Viagem, categorias: Categoria[]) {
    // Parse dos valores
    vo.fechamento.totalCreditos = 0;
    vo.fechamento.totalDebitos = 0;

    vo.lancamentos.forEach((item) => {
      // Busca categoria pelo id
      let c = this.getCategoria(item.categoria.id, categorias);

      if (c) {
        if (c.tipo == 'Débito') {
          vo.fechamento.totalDebitos += Utils.parseNumber(item.valorLancamento);
        } else if (c.tipo == 'Crédito') {
          vo.fechamento.totalCreditos += Utils.parseNumber(item.valorLancamento);
        }
      }
    });

    // Saldo final
    vo.fechamento.saldoFinal =
      Utils.parseNumber(vo.fechamento.totalCreditos) -
      Utils.parseNumber(vo.fechamento.totalDebitos);
  }

  getCategoria(id: any, categorias): Categoria {
    let c = null;

    categorias.forEach((item) => {
      if (item.id == id) {
        c = item;
      }
    });

    return c;
  }

}
