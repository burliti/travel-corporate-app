import { MinhasViagensCadastroComponent } from './minhas-viagens-cadastro/minhas-viagens-cadastro.component';
import { MinhasViagensConsultaComponent } from './minhas-viagens-consulta/minhas-viagens-consulta.component';
import { AutenticacaoGuard } from './../../../security/autenticacao.guard';
import { MinhasViagensComponent } from './minhas-viagens.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{
  path: 'colaborador/minhas-viagens', component: MinhasViagensComponent, canActivate: [AutenticacaoGuard], children: [
    { path: '', component: MinhasViagensConsultaComponent },
    {
      path: ':acao', children: [
        { path: '', component: MinhasViagensCadastroComponent },
        { path: ':id', component: MinhasViagensCadastroComponent }
      ]
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MinhasViagensRoutingModule { }
