import { FormatUtils } from './../../../../plugins/format-utils';
import { SessaoUsuario } from './../../../../security/sessao.model';
import { ReciboService } from './../../../financeiro/recibo/recibo.service';
import { ModalComponent } from './../../../../plugins/modal/modal.component';
import { Recibo } from './../../../financeiro/recibo/recibo.model';
import { Lancamento } from './../../../financeiro/lancamento/lancamento.model';
import { Categoria } from './../../../configuracoes/categoria/categoria.model';
import { Utils } from './../../../../plugins/shared';
import { CategoriaService } from './../../../configuracoes/categoria/categoria.service';
import { Fechamento } from './../../fechamento/fechamento.model';
import { MinhasViagensService } from './../minhas-viagens.service';
import { Viagem } from './../../../financeiro/viagem/viagem.model';
import { BlockUiService } from './../../../../plugins/block-ui/block-ui.service';
import { MensagemService } from './../../../../plugins/forms/mensagem.service';
import { SelectGroupOption } from './../../../../plugins/forms/select-group/select-group-option';
import { Subscription } from 'rxjs/Rx';
import { Component, OnInit, AfterViewChecked, ViewChild } from '@angular/core';
import { AbstractCadastroComponent } from '../../../../components/geral/crud/abstract-cadastro.component';
import { EnumUtils } from '../../../../plugins/shared';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginService } from '../../../../security/login.service';

declare var $: any;

@Component({
  selector: 'app-minhas-viagens-cadastro',
  templateUrl: './minhas-viagens-cadastro.component.html',
  styleUrls: ['./minhas-viagens-cadastro.component.css']
})
export class MinhasViagensCadastroComponent extends AbstractCadastroComponent<Viagem, MinhasViagensService> implements AfterViewChecked {

  sessao: SessaoUsuario;

  inscricoes: Subscription[] = [];

  // Colecoes
  itensStatus: SelectGroupOption[] = EnumUtils.getOptionsAtivo();
  itensStatusViagem: SelectGroupOption[] = EnumUtils.getOptionsStatusViagem();
  itensSimNao: SelectGroupOption[] = EnumUtils.getOptionsSimNao();
  itensTipoFornecedor: SelectGroupOption[] = EnumUtils.getOptionsTipoPessoa();
  itensCategoria: SelectGroupOption[] = [];
  itensCliente: SelectGroupOption[] = [];
  itensProjeto: SelectGroupOption[] = [];
  categorias: Categoria[];

  // Para popup's de observacao e recibo
  lancamentoSelecionado: Lancamento = {};
  reciboSelecionado: Recibo = {};
  showObservacoes: boolean = false;
  showRecibo: boolean = false;

  // Controle de ordenação
  sortByField: string = 'sequencial';
  sortByDesc: boolean = true;



  @ViewChild('reciboModal') reciboModal: ModalComponent;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private blockService: BlockUiService,
    private categoriaService: CategoriaService,
    private minhasViagensService: MinhasViagensService,
    private reciboService: ReciboService,
    private msg: MensagemService,
    private loginService: LoginService) {
    super(route, router, blockService, msg);
  }

  ngAfterViewChecked() {
    $('[data-toggle="tooltip"]').tooltip();
  }

  ngOnInit() {
    this.sessao = this.loginService.getUsuarioAutenticado();

    var f = this.carregarRelatorio;

    $(document).ready(() => {
      $('#a-relatorio-viagem').on("click", function () {
        console.log('click');
        f();
      });
    });

    const s = this.minhasViagensService.getCategorias().subscribe((retorno) => {
      this.categorias = retorno;

      this.itensCategoria.push({ label: 'Selecione a categoria de lançamento', value: '' });

      for (const categoria of retorno) {
        if (categoria.interna == 'Não') {
          this.itensCategoria.push({ label: categoria.nome, value: categoria.id, group: categoria.tipo });
        }
      }

      const s2 = this.minhasViagensService.getClientes().subscribe((retorno) => {
        this.itensCliente.push({ label: 'Nenhum', value: '' });

        for (const cliente of retorno) {
          this.itensCliente.push({ label: cliente.nome, value: cliente.id });
        }

        this.minhasViagensService.getProjetos().subscribe((retorno) => {

          this.itensProjeto.push({ label: 'Nenhum', value: '' });

          for (const projeto of retorno) {
            this.itensProjeto.push({ label: projeto.nome, value: projeto.id });
          }

          super.ngOnInit();
        });
      });
    });

    this.inscricoes.push(s);
  }

  avancarParaLancamentos() {
    if (this.isPodeEditar()) {
      this.salvar(false, false, () => $('#a-lancamentos').click());
    } else {
      $('#a-lancamentos').click()
    }
  }

  voltarParaDadosGerais() {
    if (this.isPodeEditar()) {
      this.salvar(false, false, () => $('#a-dados-viagem').click());
    } else {
      $('#a-dados-viagem').click()
    }
  }

  avancarParaFechamento() {
    if (this.isPodeEditar()) {
      this.salvar(false, false, () => $('#a-relatorio-viagem').click());
    } else {
      $('#a-relatorio-viagem').click();
    }
  }

  voltarParaLancamentos() {
    this.avancarParaLancamentos();
  }

  carregarRelatorio() {
    console.log("/api/resources/minhas-viagens/v1/relatorio/" + $("#frameRelatorio").attr("data-relatorio-id"));

    $("#frameRelatorio").attr("src", "/api/resources/minhas-viagens/v1/relatorio/" + $("#frameRelatorio").attr("data-relatorio-id"));
  }

  finalizar() {
    this.blockService.block();

    this.msg.confirm('Deseja FINALIZAR o Relatório de Viagens? Esta ação não poderá ser desfeita!')
      .then(() => {
        this.minhasViagensService.finalizar(this.vo).subscribe((retorno) => {
          this.blockService.unBlock();

          if (retorno.success) {
            this.msg.nofiticationSucess(retorno.message);
            this.voltar();
          } else {
            this.msg.warn(Utils.buildErrorMessage(retorno));
          }
        }, (error) => {
          this.blockService.unBlock();
          console.log(error);
          this.msg.error(Utils.buildErrorMessage(error._body));
        });
      }).catch(() => {
        // Faz nada...
        this.blockService.unBlock();
      });
  }

  isPodeEditar() {
    return this.isExcluir() ? false : this.vo.status == 'Em Andamento' || this.vo.status == 'Relatório rejeitado';
  }

  salvarEContinuar() {
    this.salvar(false);
  }

  salvar(voltar: boolean = true, showSuccessMessage: boolean = true, callbackSuccess: () => any = undefined) {
    if (this.vo.lancamentos) {
      this.vo.lancamentos.forEach((lancamento) => {
        if (!lancamento.recibo || !lancamento.recibo.id) {
          lancamento.recibo = null;
        }
      });
    }

    super.salvar(voltar, showSuccessMessage, callbackSuccess);
  }

  toogleSort(field: string) {
    if (field == this.sortByField) {
      this.sortByDesc = !this.sortByDesc;
    } else {
      this.sortByField = field;
      this.sortByDesc = false;
    }
  }

  adicionar() {
    if (!this.vo.lancamentos) {
      this.vo.lancamentos = [];
    }

    let oSequencial = this.vo.lancamentos.length + 1;

    let today = new Date().toLocaleDateString(); //Utils.formatDate(new Date());

    this.vo.lancamentos.splice(0, 0, {
      sequencial: oSequencial.toString(),
      dataInicial: today,
      tipoPeriodo: 'Data de Referência',
      categoria: {},
      recibo: {}
    });

    this.calcularSaldos();
  }

  remover(lancamento: Lancamento) {
    this.msg.confirm('Confirma a EXCLUSÃO deste lançamento?')
      .then(() => {
        let index = this.vo.lancamentos.indexOf(lancamento);
        this.vo.lancamentos.splice(index, 1);

        this.calcularSaldos();
      });
  }

  mudarTipoData(lancamento: Lancamento) {
    if (lancamento.tipoPeriodo == 'Data de Referência') {
      lancamento.tipoPeriodo = 'Período';
    } else {
      lancamento.tipoPeriodo = 'Data de Referência';
    }
  }

  observacoes(lancamento: Lancamento) {
    // Esconde o recibo
    this.showRecibo = false;

    if (this.lancamentoSelecionado.sequencial == lancamento.sequencial) {
      this.fecharObservacoes(lancamento);
    } else {
      this.lancamentoSelecionado = lancamento;
      this.showObservacoes = true;

      setTimeout(() => $('#internal_observacoes_' + lancamento.sequencial).focus(), 200);
    }
  }

  isRejeitado() {
    return this.vo.status == 'Relatório rejeitado';
  }

  isAprovado() {
    return this.vo.status == 'Relatório aprovado';
  }

  recibo(lancamento: Lancamento) {
    this.showObservacoes = false;

    this.lancamentoSelecionado = lancamento;

    if (!lancamento.recibo || !lancamento.recibo.id) {
      if (this.isPodeEditar()) {
        this.reciboSelecionado = {
          dataReferencia: lancamento.dataInicial,
          valor: lancamento.valorLancamento
        };
        this.reciboModal.show();
      } else {
        this.msg.nofiticationWarning('Não há recibo para este lançamento!');
        return;
      }
    } else {
      this.reciboSelecionado = lancamento.recibo;
      this.reciboModal.show();
    }

    this.reciboSelecionado.valorString = FormatUtils.formatMoney(this.reciboSelecionado.valor);
  }

  salvarRecibo() {
    if (!this.reciboSelecionado.id) {
      // Insert
      let s = this.reciboService.inserir(this.reciboSelecionado).subscribe((retorno) => {
        if (retorno.success) {
          this.lancamentoSelecionado.recibo = retorno.vo;
          this.reciboModal.close();
        } else {
          this.msg.warn(Utils.buildErrorMessage(retorno)).then(() => {
            if (retorno.errors && retorno.errors.length > 0) {
              let field = '#internal_recibo_' + retorno.errors[0].fieldName;
              $(field).focus();
            }
          }).catch(() => null);
        }
      });
    } else {
      // Update
      let s = this.reciboService.atualizar(this.reciboSelecionado, this.reciboSelecionado.id).subscribe((retorno) => {
        if (retorno.success) {
          this.lancamentoSelecionado.recibo = retorno.vo;
          this.reciboModal.close();
        } else {
          this.msg.warn(Utils.buildErrorMessage(retorno)).then(() => {
            if (retorno.errors && retorno.errors.length > 0) {
              let field = '#internal_recibo_' + retorno.errors[0].fieldName;
              $(field).focus();
            }
          }).catch(() => null);
        }
      });
    }
  }

  abrirLinkNfe(recibo: Recibo) {
    if (recibo.urlNfe && (recibo.urlNfe.startsWith('http://') || recibo.urlNfe.startsWith('https://'))) {
      window.open(recibo.urlNfe, '_blank');
    } else {
      this.msg.nofiticationWarning('Chave da NF-e inválida');
    }
  }

  visualizarPdf() {
    window.open($("#frameRelatorio").attr("src"), '_blank');
  }

  isShowObservacoes(lancamento: Lancamento) {
    return this.showObservacoes && this.lancamentoSelecionado.sequencial == lancamento.sequencial;
  }

  fecharObservacoes(lancamento: Lancamento) {
    this.showObservacoes = false;
    this.showRecibo = false;
    this.lancamentoSelecionado = {};
  }

  fecharRecibo(lancamento: Lancamento) {
    this.showObservacoes = false;
    this.showRecibo = false;
    this.lancamentoSelecionado = {};
  }

  isShowRecibo(lancamento: Lancamento) {
    return this.showRecibo && this.lancamentoSelecionado.sequencial == lancamento.sequencial;
  }

  getCaracteresNfe(recibo: Recibo): string {
    if (recibo && recibo.chaveNfe) {
      return '(' + recibo.chaveNfe.length + ' caracteres)';
    }
    return '(0 caracteres)';
  }

  getClasseCaracteresNfe(recibo: Recibo): string {
    if (recibo && recibo.chaveNfe && recibo.chaveNfe.length > 0) {
      if (recibo.chaveNfe.length == 44) {
        return 'caracteres-validos';
      } else {
        return 'caracteres-invalidos';
      }
    }
    return '';
  }

  getFoto(recibo: Recibo) {
    if (!recibo || !recibo.fotoAnexoRecibo || recibo.fotoAnexoRecibo === '') {
      return 'assets/img/sem_imagem.png';
    } else {
      return recibo.fotoAnexoRecibo;
    }
  }

  getService(): MinhasViagensService {
    return this.minhasViagensService;
  }

  changeValorRecibo(recibo: Recibo) {
    if (!recibo.valorString || recibo.valorString.trim() == '') {
      recibo.valorString = '0.00';
    }

    recibo.valor = FormatUtils.parseMoney(recibo.valorString); //parseFloat(recibo.valorString.replace('.', '').replace(',', '.').replace('R', '').replace('$', ''));
  }

  changeValorLancamento(lancamento: Lancamento) {
    if (!lancamento.valorLancamentoString || lancamento.valorLancamentoString.trim() == '') {
      lancamento.valorLancamentoString = '0.00';
    }

    lancamento.valorLancamento = FormatUtils.parseMoney(lancamento.valorLancamentoString);
    // parseFloat(lancamento.valorLancamentoString.replace('.', '').replace(',', '.').replace('R', '').replace('$', ''));

    this.calcularSaldos();
  }

  calcularSaldos() {
    this.getService().calcularSaldos(this.vo, this.categorias);
  }

  beforeVisualizar(vo: Viagem) {
    if (!vo.cliente) {
      vo.cliente = {};
    }
    if (!vo.projeto) {
      vo.projeto = {};
    }

    if (!vo.lancamentos) {
      vo.lancamentos = [];
    }

    vo.lancamentos.forEach((lancamento) => {
      lancamento.valorLancamentoString = FormatUtils.formatMoney(lancamento.valorLancamento);

      if (lancamento.recibo) {
        lancamento.recibo.valorString = FormatUtils.formatMoney(lancamento.recibo.valor);
      }
    });
  }

  getCategoria(id: any): Categoria {
    let c = {};

    this.categorias.forEach((item) => {
      if (item.id == id) {
        c = item;
      }
    });

    return c;
  }

  getVoNewInstance(): Viagem {
    return {
      fechamento: {
        saldoAnterior: 0,
        saldoFinal: 0,
        totalCreditos: 0,
        totalDebitos: 0
      },
      status: 'Em Andamento',
      cliente: {},
      projeto: {},
      lancamentos: []
    };
  }
}
