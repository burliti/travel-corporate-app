import { TituloPaginaService } from './../../../components/services/titulo-pagina.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-minhas-viagens',
  templateUrl: './minhas-viagens.component.html',
  styleUrls: ['./minhas-viagens.component.css']
})
export class MinhasViagensComponent implements OnInit {

  constructor(private tituloPagianService : TituloPaginaService) { }

  ngOnInit() {
    this.tituloPagianService.setTitulo("Minhas viagens");
  }

}
