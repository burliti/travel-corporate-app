import { TituloPaginaService } from './../../../components/services/titulo-pagina.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-prestacao-contas',
  templateUrl: './prestacao-contas.component.html',
  styleUrls: ['./prestacao-contas.component.css']
})
export class PrestacaoContasComponent implements OnInit {

  constructor(private t : TituloPaginaService) { }

  ngOnInit() {
    this.t.setTitulo("Prestação de contas");
  }
}
