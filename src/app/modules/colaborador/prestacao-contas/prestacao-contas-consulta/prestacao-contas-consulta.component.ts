import { BlockUiService } from './../../../../plugins/block-ui/block-ui.service';
import { PrestacaoContasService } from './../prestacao-contas.service';
import { Lancamento } from './../../../financeiro/lancamento/lancamento.model';
import { TituloPaginaService } from './../../../../components/services/titulo-pagina.service';
import { CategoriaService } from './../../../configuracoes/categoria/categoria.service';
import { ClienteService } from './../../../cadastros/cliente/cliente.service';
import { DataPaginationEvent } from './../../../../plugins/plugins.api';
import { MensagemService } from './../../../../plugins/forms/mensagem.service';
import { ActivatedRoute, Router } from '@angular/router';
import { EnumUtils } from './../../../../plugins/shared';
import { Subscription } from 'rxjs/Rx';
import { DataTableComponent } from './../../../../plugins/data-table/data-table.component';
import { ConsultaResponse } from './../../../../components/model/consulta-response.model';
import { SelectGroupOption } from './../../../../plugins/forms/select-group/select-group-option';
import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild } from '@angular/core';
import { Viagem } from '../../../financeiro/viagem/viagem.model';

@Component({
  selector: 'app-prestacao-contas-consulta',
  templateUrl: './prestacao-contas-consulta.component.html',
  styleUrls: ['./prestacao-contas-consulta.component.css']
})
export class PrestacaoContasConsultaComponent implements OnDestroy, AfterViewInit {
  // Colecoes
  itensStatus: SelectGroupOption[] = EnumUtils.getOptionsAtivo("TODOS");
  itensStatusViagem: SelectGroupOption[] = EnumUtils.getOptionsStatusViagem("TODOS");
  itensCategoria: SelectGroupOption[] = [];
  itensTipo: SelectGroupOption[] = EnumUtils.getOptionsTipoLancamento("TODOS");

  dados: ConsultaResponse<Lancamento>;

  public id: any;

  filtro: any = {
  };

  order: any = { "sequencial": "desc" };

  private inscricoes: Subscription[] = [];

  // Recupera o componente dataTable
  @ViewChild(DataTableComponent) dt: DataTableComponent<Lancamento>;

  constructor(
    private tituloPaginaService : TituloPaginaService,
    private route: ActivatedRoute,
    private router: Router,
    private msg: MensagemService,
    private blockService : BlockUiService,
    private prestacaoContasService: PrestacaoContasService,
    private categoriaService: CategoriaService,
    private clienteService: ClienteService) { }

  ngOnInit() {
    this.itensCategoria.push({ label: "TODOS", group: "TODOS" });

    // const s = this.route.params.subscribe((params: any[]) => {
    //   this.id = params['idViagem'];

    //   if (this.id) {
    //     const s2 = this.viagemService.buscar(this.id).subscribe((retorno) => {
    //       this.viagem = retorno;

    //       this.pesquisar();
    //     });
    //     this.inscricoes.push(s2);
    //   }
    // });

    const s3 = this.categoriaService.getListAtivos().subscribe((retorno) => {
      for (const categoria of retorno.lista) {
        this.itensCategoria.push({ label: categoria.nome, value: categoria.id, group: categoria.tipo });
      }
    });

    this.inscricoes.push(s3);
  }

  ngAfterViewInit() { }

  ngOnDestroy() {
    for (let s of this.inscricoes) {
      s.unsubscribe();
    }
  }

  lancamentos() {
    if (this.dt.getSelectedItem()) {
      //
    } else {
      this.msg.warn("Nenhuma viagem selecionada");
    }
  }

  pesquisar() {
    if (this.filtro.categoria$id) {
      this.filtro.tipoLancamento = null;
    }

    this.dt.load(1);
  }

  limpar() {
    this.filtro = {};
    this.pesquisar();
  }

  novo() {
    this.router.navigate(['novo'], { relativeTo: this.route });
  }

  editar() {
    if (this.dt.getSelectedItem()) {
      this.router.navigate(['editar', this.dt.getSelectedItem().id], { relativeTo: this.route });
    } else {
      this.msg.nofiticationWarning('Nenhum item selecionado para alterar!');
    }
  }

  excluir() {
    if (this.dt.getSelectedItem()) {
      this.router.navigate(['excluir', this.dt.getSelectedItem().id], { relativeTo: this.route });
    } else {
      this.msg.nofiticationWarning('Nenhum item selecionado para excluir!');
    }
  }

  voltar() {
    this.router.navigate(['/lancamentos/minhas-viagens']);
  }

  paginacao(event: DataPaginationEvent) {
    this.blockService.block('.table-consulta');

    let s = this.prestacaoContasService.getList({
      filtro: this.filtro,
      order: this.order,
      pageSize: event.pageSize,
      pageNumber: event.page
    }).subscribe((dados) => {
      this.dados = dados;

      this.blockService.unBlock('.table-consulta');
    }, (error) => {
      this.dados = {
        lista: [],
        total: 0
      };

      this.blockService.unBlock('.table-consulta');
    });
  }
}
