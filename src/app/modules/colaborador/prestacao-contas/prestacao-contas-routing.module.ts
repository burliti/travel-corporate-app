import { PrestacaoContasCadastroComponent } from './prestacao-contas-cadastro/prestacao-contas-cadastro.component';
import { PrestacaoContasConsultaComponent } from './prestacao-contas-consulta/prestacao-contas-consulta.component';
import { AutenticacaoGuard } from './../../../security/autenticacao.guard';
import { PrestacaoContasComponent } from './prestacao-contas.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{
  path: 'colaborador/prestacao-contas', component: PrestacaoContasComponent, canActivate: [AutenticacaoGuard], children: [
    { path: '', component: PrestacaoContasConsultaComponent },
    {
      path: ':acao', children: [
        { path: '', component: PrestacaoContasCadastroComponent },
        { path: ':id', component: PrestacaoContasCadastroComponent }
      ]
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrestacaoContasRoutingModule { }
