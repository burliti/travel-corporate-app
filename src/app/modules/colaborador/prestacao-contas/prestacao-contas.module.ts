import { PrestacaoContasService } from './prestacao-contas.service';
import { FormsModule } from '@angular/forms';
import { PluginsModule } from './../../../plugins/plugins.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrestacaoContasRoutingModule } from './prestacao-contas-routing.module';
import { PrestacaoContasComponent } from './prestacao-contas.component';
import { PrestacaoContasCadastroComponent } from './prestacao-contas-cadastro/prestacao-contas-cadastro.component';
import { PrestacaoContasConsultaComponent } from './prestacao-contas-consulta/prestacao-contas-consulta.component';

@NgModule({
  imports: [
    CommonModule,
    PluginsModule,
    FormsModule,
    PrestacaoContasRoutingModule
  ],
  providers : [PrestacaoContasService],
  declarations: [PrestacaoContasComponent, PrestacaoContasCadastroComponent, PrestacaoContasConsultaComponent]
})
export class PrestacaoContasModule { }
