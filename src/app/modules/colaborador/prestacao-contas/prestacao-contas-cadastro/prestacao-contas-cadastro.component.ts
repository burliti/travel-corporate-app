import { BlockUiService } from './../../../../plugins/block-ui/block-ui.service';
import { PrestacaoContasService } from './../prestacao-contas.service';
import { Lancamento } from './../../../financeiro/lancamento/lancamento.model';
import { TituloPaginaService } from './../../../../components/services/titulo-pagina.service';
import { TirarFotoService } from './../../../../plugins/camera/tirar-foto.service';
import { LoginService } from './../../../../security/login.service';
import { CategoriaService } from './../../../configuracoes/categoria/categoria.service';
import { MensagemService } from './../../../../plugins/forms/mensagem.service';
import { SelectGroupOption } from './../../../../plugins/forms/select-group/select-group-option';
import { Subscription } from 'rxjs/Rx';
import { Component, OnInit } from '@angular/core';
import { AbstractCadastroComponent } from '../../../../components/geral/crud/abstract-cadastro.component';
import { EnumUtils } from '../../../../plugins/shared';
import { ActivatedRoute, Router } from '@angular/router';
import { Viagem } from '../../../financeiro/viagem/viagem.model';

declare var $: any;

@Component({
  selector: 'app-prestacao-contas-cadastro',
  templateUrl: './prestacao-contas-cadastro.component.html',
  styleUrls: ['./prestacao-contas-cadastro.component.css']
})
export class PrestacaoContasCadastroComponent extends AbstractCadastroComponent<Lancamento, PrestacaoContasService> {

  inscricoes: Subscription[] = [];

  // Colecoes
  itensStatus: SelectGroupOption[] = EnumUtils.getOptionsAtivo();
  itensSimNao: SelectGroupOption[] = EnumUtils.getOptionsSimNao();
  itensCategoria: SelectGroupOption[] = [];

  viagem: Viagem = {};

  constructor(
    private tituloPaginaService: TituloPaginaService,
    private tirarFotoService: TirarFotoService,
    private loginService: LoginService,
    private route: ActivatedRoute,
    private router: Router,
    private blockService: BlockUiService,
    private categoriaService: CategoriaService,
    private prestacaoContasService: PrestacaoContasService,
    private msg: MensagemService) {
    super(route, router, blockService, msg);
  }

  ngOnInit() {
    const s = this.categoriaService.getListDespesasAtivos().subscribe((retorno) => {
      this.itensCategoria.push({ label: 'Selecione a categoria de lançamento', value: '' });

      for (const categoria of retorno.lista) {
        this.itensCategoria.push({ label: categoria.nome, value: categoria.id, group: categoria.tipo });
      }

      const s2 = this.route.params.subscribe((params: any[]) => {
        let idViagem = params['idViagem'];

        // if (idViagem) {
        //   const s3 = this.viagemService.buscar(idViagem).subscribe((retorno) => {
        //     this.viagem = retorno;
        //   });
        //   this.inscricoes.push(s3);

        super.ngOnInit();
        // }
      });
      this.inscricoes.push(s2);
    });
    this.inscricoes.push(s);
  }

  getService(): PrestacaoContasService {
    return this.prestacaoContasService;
  }

  salvar() {
    super.salvar();
  }

  selecionarRecibo() {
    this.msg.warn("Seleção de recibo ainda nao implementada!");
  }

  novoRecibo() {
    this.vo.recibo = {
      id: "",
      funcionario: {}
    };

    setTimeout(() => $("#internal_recibo_dataReferencia").focus(), 300);
  }

  limparRecibo() {
    this.vo.recibo.id = "-1";
  }

  isPodeEditarRecibo(): boolean {
    if (this.isExcluir()) {
      // Se estiver em modo de exclusão, não pode editar também.
      return false;
    } else if (!this.vo.recibo) {
      // Se nao tiver recibo, nao pode editar
      return false;
    } else if (!this.vo.recibo.id) {
      // Se for um recibo novo, pode editar
      return true;
    } else if (this.vo.recibo.funcionario && this.vo.recibo.funcionario.id == this.loginService.getUsuarioAutenticado().funcionario.id) {
      // Se for um recibo cadastrado por mim, pode editar
      return true;
    }
    return false;
  }

  /**
   * Vai indicar se a tela de cadastro do recibo será exibida
   */
  isExibeRecibo(): boolean {
    return this.vo && this.vo.recibo && this.vo.recibo.id != "-1";
  }

  getVoNewInstance(): Lancamento {
    return {
      viagem: this.viagem,
      categoria: {},
      funcionario: {},
      recibo: {
        id: "-1",
        funcionario: {}
      }
    };
  }

  getFoto() {
    if (!this.vo.recibo.fotoAnexoRecibo || this.vo.recibo.fotoAnexoRecibo === '') {
      return 'assets/img/sem_imagem.png';
    } else {
      return this.vo.recibo.fotoAnexoRecibo;
    }
  }

  tirarFoto() {
    const idVideoSource = this.tirarFotoService.retornarDeviceFoto('cadastro_morador');

    this.tirarFotoService.tirarFoto(idVideoSource)
      .then((fotoBase64) => {
        this.vo.recibo.fotoAnexoRecibo = fotoBase64;
      }).catch((mensagem) => {
        if (mensagem) {
          this.msg.nofiticationError(mensagem);
        }
      });
  }

  limparFoto() {
    this.vo.recibo.fotoAnexoRecibo = '';
  }
}

