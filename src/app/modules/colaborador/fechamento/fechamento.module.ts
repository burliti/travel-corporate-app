import { FechamentoService } from './fechamento.service';
import { PluginsModule } from './../../../plugins/plugins.module';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FechamentoRoutingModule } from './fechamento-routing.module';
import { FechamentoComponent } from './fechamento.component';
import { FechamentoCadastroComponent } from './fechamento-cadastro/fechamento-cadastro.component';
import { FechamentoConsultaComponent } from './fechamento-consulta/fechamento-consulta.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PluginsModule,
    FechamentoRoutingModule
  ],
  providers : [FechamentoService],
  declarations: [FechamentoComponent, FechamentoCadastroComponent, FechamentoConsultaComponent]
})
export class FechamentoModule { }
