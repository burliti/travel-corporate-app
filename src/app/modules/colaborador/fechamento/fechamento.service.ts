import { CadastroResponse } from './../../../components/model/cadastro-response.model';
import { Lancamento } from './../../financeiro/lancamento/lancamento.model';
import { Fechamento } from './fechamento.model';
import { Observable } from 'rxjs';
import { ConsultaResponse } from './../../../components/model/consulta-response.model';
import { Injectable, Inject } from '@angular/core';
import { AbstractCrudService } from '../../../components/services/abstract-crud.service';
import { Http } from '@angular/http';
import { LoginService } from '../../../security/login.service';

@Injectable()
export class FechamentoService extends AbstractCrudService<Fechamento> {

  constructor( @Inject(Http) _http: Http, @Inject(LoginService) loginService: LoginService) {
    super(_http, loginService);
  }

  getEntityName(): string {
    return "fechamento";
  }

  /**
   * Retorna a lista de lancamentos
   */
  getLancamentos(idFechamento: string): Observable<Lancamento[]> {
    return this._http.post(this.getUrl() + '/lancamentos/' + idFechamento,
      null,
      { headers: this.getHeaders() })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  /**
   * Finaliza o Relatório de Viagem
   * @param vo Entidade de fechamento
   */
  finalizar(vo: Fechamento): Observable<CadastroResponse<Fechamento>> {
    return this._http.post(this.getUrl() + '/finalizar', JSON.stringify(vo), { headers: this.getHeaders() })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

}
