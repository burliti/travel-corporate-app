import { TituloPaginaService } from './../../../components/services/titulo-pagina.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fechamento',
  templateUrl: './fechamento.component.html',
  styleUrls: ['./fechamento.component.css']
})
export class FechamentoComponent implements OnInit {

  constructor(private tituloPaginaService : TituloPaginaService) { }

  ngOnInit() {
    this.tituloPaginaService.setTitulo("Relatório de Viagem");
  }

}
