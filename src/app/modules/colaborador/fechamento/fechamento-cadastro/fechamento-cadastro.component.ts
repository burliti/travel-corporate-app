import { BlockUiService } from './../../../../plugins/block-ui/block-ui.service';
import { ModalComponent } from './../../../../plugins/modal/modal.component';
import { Recibo } from './../../../financeiro/recibo/recibo.model';
import { Utils } from './../../../../plugins/shared';
import { Lancamento } from './../../../financeiro/lancamento/lancamento.model';
import { FechamentoService } from './../fechamento.service';
import { Fechamento } from './../fechamento.model';
import { LoginService } from './../../../../security/login.service';
import { CategoriaService } from './../../../configuracoes/categoria/categoria.service';
import { MensagemService } from './../../../../plugins/forms/mensagem.service';
import { SelectGroupOption } from './../../../../plugins/forms/select-group/select-group-option';
import { Subscription } from 'rxjs/Rx';
import { Component, OnInit, ViewChild } from '@angular/core';
import { AbstractCadastroComponent } from '../../../../components/geral/crud/abstract-cadastro.component';
import { EnumUtils } from '../../../../plugins/shared';
import { ActivatedRoute, Router } from '@angular/router';
import { Viagem } from '../../../financeiro/viagem/viagem.model';

declare var $: any;

@Component({
  selector: 'app-fechamento-cadastro',
  templateUrl: './fechamento-cadastro.component.html',
  styleUrls: ['./fechamento-cadastro.component.css']
})
export class FechamentoCadastroComponent extends AbstractCadastroComponent<Fechamento, FechamentoService> {

  lancamentos: Lancamento[] = [];
  inscricoes: Subscription[] = [];
  reciboSelecionado: Recibo = {};

  @ViewChild('reciboModal') modal: ModalComponent;

  constructor(
    private loginService: LoginService,
    private route: ActivatedRoute,
    private router: Router,
    private blockService: BlockUiService,
    private fechamentoService: FechamentoService,
    private msg: MensagemService) {
    super(route, router, blockService, msg);
  }

  beforeVisualizar(vo: Fechamento) {
    // Busca lista

    if (!this.isFinalizado()) {
      const s = this.fechamentoService.getLancamentos(vo.id).subscribe((retorno) => {
        this.lancamentos = retorno;
      });

      this.inscricoes.push(s);
    }
  }

  exibirRecibo(lancamento: Lancamento) {
    this.alternarItem(lancamento);

    this.reciboSelecionado = lancamento.recibo;
    this.modal.show();
  }

  finalizar() {
    this.msg.confirm('Deseja FINALIZAR este relatório de viagens? Esta ação não poderá ser desfeita!')
      .then(() => {
        const s = this.getService().finalizar(this.vo).subscribe((retorno) => {
          if (retorno.success) {
            this.msg.nofiticationSucess(retorno.message);
            this.voltar();
          } else {
            this.msg.error(Utils.buildErrorMessage(retorno));
          }
        });
        this.inscricoes.push(s);
      });
  }

  getLancamentos(): Lancamento[] {
    if (this.isFinalizado()) {
      return this.vo.lancamentos;
    } else {
      return this.lancamentos;
    }
  }

  isSelecionado(lancamento: Lancamento) {
    let index = this.getLancamentoIndex(this.vo.lancamentos, lancamento);

    return index >= 0;
  }

  isFinalizado() {
    return false;
    // return this.vo.status == 'Finalizado' || this.vo.status == 'Aprovado';
  }

  novoLancamento() {
    this.router.navigate(['/colaborador/prestacao-contas/novo']);
  }

  marcarTodos() {
    this.lancamentos.forEach(element => {
      this.marcarItem(element);
    });
  }

  desmarcarTodos() {
    this.lancamentos.forEach(element => {
      this.desmarcarItem(element);
    });
  }

  alternarItem(lancamento: Lancamento) {
    if (this.isFinalizado()) {
      return;
    }

    let index = this.getLancamentoIndex(this.vo.lancamentos, lancamento);

    if (index >= 0) {
      this.desmarcarItem(lancamento);
    } else {
      this.marcarItem(lancamento);
    }
  }

  marcarItem(lancamento: Lancamento) {
    let index = this.getLancamentoIndex(this.vo.lancamentos, lancamento);

    if (index < 0) {
      if (lancamento.tipoLancamento == 'Crédito') {
        this.vo.totalCreditos += Utils.parseNumber(lancamento.valorLancamento);
      } else {
        this.vo.totalDebitos += Utils.parseNumber(lancamento.valorLancamento);
      }

      this.vo.lancamentos.push(lancamento);

      this.calculaSaldoFinal();
    }
  }

  desmarcarItem(lancamento: Lancamento) {
    let index = this.getLancamentoIndex(this.vo.lancamentos, lancamento);

    if (index >= 0) {
      if (lancamento.tipoLancamento == 'Crédito') {
        this.vo.totalCreditos -= Utils.parseNumber(lancamento.valorLancamento);
      } else {
        this.vo.totalDebitos -= Utils.parseNumber(lancamento.valorLancamento);
      }

      this.vo.lancamentos.splice(index, 1);

      this.calculaSaldoFinal();
    }
  }

  getLancamentoIndex(lancamentos: Lancamento[], lancamento: Lancamento) {
    let i = -1;
    for (let c of lancamentos) {
      i++;
      if (c.id === lancamento.id) {
        return i;
      }
    }
    return -1;
  }

  calculaSaldoFinal() {
    this.vo.saldoFinal =
      Utils.parseNumber(this.vo.saldoAnterior) +
      Utils.parseNumber(this.vo.totalCreditos) -
      Utils.parseNumber(this.vo.totalDebitos);
  }



  getService(): FechamentoService {
    return this.fechamentoService;
  }

  salvar() {
    super.salvar();
  }

  getVoNewInstance(): Fechamento {
    return {};
  }
}
