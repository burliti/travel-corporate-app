import { FechamentoCadastroComponent } from './fechamento-cadastro/fechamento-cadastro.component';
import { FechamentoConsultaComponent } from './fechamento-consulta/fechamento-consulta.component';
import { AutenticacaoGuard } from './../../../security/autenticacao.guard';
import { FechamentoComponent } from './fechamento.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{
  path: 'colaborador/fechamento', component: FechamentoComponent, canActivate: [AutenticacaoGuard], children: [
    { path: '', component: FechamentoConsultaComponent },
    {
      path: ':acao', children: [
        { path: '', component: FechamentoCadastroComponent },
        { path: ':id', component: FechamentoCadastroComponent }
      ]
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FechamentoRoutingModule { }
