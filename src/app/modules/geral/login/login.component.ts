import { BlockUiService } from './../../../plugins/block-ui/block-ui.service';
import { SessaoUsuario } from './../../../security/sessao.model';
import { Utils } from './../../../plugins/shared';
import { Subscription } from 'rxjs/Rx';
import { MensagemService } from './../../../plugins/forms/mensagem.service';
import { LoginService } from './../../../security/login.service';
import { Router } from '@angular/router';
import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';


declare var $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, AfterViewInit, OnDestroy {

  inscricoes: Subscription[] = [];
  chaveEmpresa: string;
  email: string;
  senha: string;
  lembrar: boolean;
  novaSenha: string;
  confirmacaoNovaSenha: string;
  isAlterarSenha: boolean = false;
  sessaoUsuario: SessaoUsuario;

  constructor(
    private router: Router,
    private loginService: LoginService,
    private blockService: BlockUiService,
    private msg: MensagemService) {
  }

  ngOnInit() {
    this.email = this.loginService.getEmailLembrar();

    $('#email').on('keydown', function (e) {
      if (e.keyCode === 13) {
        e.preventDefault();
        $('#senha').focus();
      }
    });

    $('#senha').on('keydown', function (e) {
      if (e.keyCode === 13) {
        e.preventDefault();
        $('#botaoEntrar').click();
      }
    });
  }

  ngOnDestroy() {
    // Remove as inscricoes
    for (const s of this.inscricoes) {
      s.unsubscribe();
    }
  }

  ngAfterViewInit() {
    if (this.email) {
      $('#senha').focus();
    } else {
      $('#email').focus();
    }
  }

  entrar() {
    if (!this.email || !this.email.trim()) {
      this.msg.warn('Informe o seu usuário')
        .then(() => $('#email').focus());
    } else if (!this.senha || !this.senha.trim()) {
      this.msg.warn('Informe a sua senha')
        .then(() => $('#senha').focus());
    } else {
      this.blockService.block();

      const s = this.loginService
        .entrar(this.email, this.senha)
        .subscribe((retorno) => {
          this.blockService.unBlock();
          if (retorno.success) {
            if (this.lembrar) {
              this.loginService.setEmailLembrar(retorno.vo.funcionario.email);
            } else {
              if (this.loginService.getEmailLembrar() != retorno.vo.funcionario.email) {
                this.loginService.clearEmailLembrar();
              }
            }

            console.log(retorno);

            // Verifica se ele precisa alterar senha no primeiro login
            if (retorno.vo.funcionario.resetarSenha == 'Sim') {
              this.isAlterarSenha = true;
              this.sessaoUsuario = retorno.vo;

              setTimeout(function () {
                $("#novaSenha").focus();

                // Bind dos eventos ENTER
                $('#novaSenha').on('keydown', function (e) {
                  if (e.keyCode === 13) {
                    e.preventDefault();
                    $('#confirmacaoNovaSenha').focus();
                  }
                });

                $('#confirmacaoNovaSenha').on('keydown', function (e) {
                  if (e.keyCode === 13) {
                    e.preventDefault();
                    $('#botaoAlterarSenha').click();
                  }
                });
              }, 500);
            } else {
              this.loginService.setUsuarioAutenticado(retorno.vo);
              this.acessarIndex();
            }
          } else {
            this.msg.error(retorno.message)
              .then(() => $('#email').focus());
          }
        }, (error) => {
          console.log(error);
          this.blockService.unBlock();
        });

      this.inscricoes.push(s);
    }
  }

  private acessarIndex() {
    this.loginService.startTimer();
    this.router.navigate(['/index']);
  }

  alterarSenha() {
    const s = this.loginService.alterarSenha('', this.novaSenha, this.confirmacaoNovaSenha, this.sessaoUsuario.chaveSessao).subscribe((retorno) => {
      if (retorno.success) {
        this.msg.nofiticationInfo(retorno.message);

        this.loginService.setUsuarioAutenticado(this.sessaoUsuario);
        this.acessarIndex();
      } else {
        this.msg.error(Utils.buildErrorMessage(retorno)).then(() => {
          $("#internal_novaSenha").focus();
        });
      }
    });

    this.inscricoes.push(s);
  }
}
