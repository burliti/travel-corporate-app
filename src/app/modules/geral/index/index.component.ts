import { Subscription } from 'rxjs/Rx';
import { MeusDadosService } from './../../meus-dados/meus-dados.service';
import { TituloPaginaService } from './../../../components/services/titulo-pagina.service';
import { Component, OnInit } from '@angular/core';
import { Viagem } from '../../financeiro/viagem/viagem.model';
import { MeusDados } from '../../meus-dados/meus-dados.model';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  constructor(private tituloPaginaService: TituloPaginaService, private meusDadosService: MeusDadosService) { }

  meusDados: MeusDados = {};
  public inscricoes: Subscription[] = [];

  async ngOnInit() {
    this.tituloPaginaService.setTitulo("Início");

    // let s = this.meusDadosService.getMeusDados().subscribe((retorno) => {
    //   this.meusDados = retorno;
    // });

    // this.inscricoes.push(s);

    this.meusDados = await this.meusDadosService.getMeusDados();
  }

  ngOnDestroy() {
    // Remove as inscricoes
    for (const s of this.inscricoes) {
      s.unsubscribe();
    }
  }
}
