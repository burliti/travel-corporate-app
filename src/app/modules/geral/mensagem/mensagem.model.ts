import { Funcionario } from './../../cadastros/funcionario/funcionario.model';

export class Mensagem {
  id?: number;
  assunto?: any;
  assuntoDescricao?: string;
  autor?: Funcionario;
  mensagemSistema?: string;
  permiteResponder?: string;
  permiteEncaminhar?: string;
  exigeConfirmacao?: string;
  bloqueiaSemResponder?: string;
  textoBotaoConfirmacao?: string;
  mensagemOrigemResposta?: Mensagem;
  mensagemOrigemEncaminhada?: Mensagem;
  status?: string;
  textoMensagem?: string;
  destinatarios?: MensagemDestinatario[] = [];
  destinatariosAux?: any[] = [];
  formasNotificacao?: MensagemFormaNotificacao[] = [];
}

export class MensagemDestinatario {
  id?: number;
  destinatario?: Funcionario;
  status?: string;
}

export class MensagemFormaNotificacao {
  id?: number;
  formaNotificacao?: FormaNotificacao;
}

export class MensagemDados {
  funcionarios?: Funcionario[] = [];
  formasNotificacao?: FormaNotificacaoEmpresa[] = [];

  totalRecebidas?: number;
  totalEnviadas?: number;
  totalRascunhos?: number;
}

export class FormaNotificacaoEmpresa {
  id?: number;
  formaNotificacao?: FormaNotificacao;
}

export class FormaNotificacao {
  id?: number;
  nome?: string;
}
