import { MensagemDados } from './../mensagem.model';
import { TituloPaginaService } from './../../../../components/services/titulo-pagina.service';
import { MinhasMensagensService } from './../minhas-mensagens.service';
import { BlockUiService } from './../../../../plugins/block-ui/block-ui.service';
import { DataPaginationEvent } from './../../../../plugins/plugins.api';
import { MensagemService } from './../../../../plugins/forms/mensagem.service';
import { ActivatedRoute, Router } from '@angular/router';
import { EnumUtils, Utils } from './../../../../plugins/shared';
import { Subscription } from 'rxjs/Rx';
import { DataTableComponent } from './../../../../plugins/data-table/data-table.component';
import { ConsultaResponse } from './../../../../components/model/consulta-response.model';
import { SelectGroupOption } from './../../../../plugins/forms/select-group/select-group-option';
import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild } from '@angular/core';
import { Mensagem } from '../mensagem.model';

declare var $: any;

@Component({
  selector: 'app-mensagem-consulta',
  templateUrl: './mensagem-consulta.component.html',
  styleUrls: ['./mensagem-consulta.component.scss']
})
export class MensagemConsultaComponent implements OnDestroy, AfterViewInit {

  // Colecoes
  itensStatus: SelectGroupOption[] = EnumUtils.getOptionsStatusMensagem("TODOS");
  itensTipoCaixa: SelectGroupOption[] = [{ label: 'Caixa de entrada', value: 'recebidas' }, { label: 'Mensagens enviadas', value: 'enviadas' }, { label: 'Rascunhos', value: 'rascunhos' }];
  tipoCaixa: string = this.itensTipoCaixa[0].value;

  totais: MensagemDados = {};

  dados: ConsultaResponse<Mensagem>;

  filtro: any = {
    "status": ""
  };
  order: any = { "id": "desc" };

  private inscricoes: Subscription[] = [];

  // Recupera o componente dataTable
  @ViewChild(DataTableComponent) dt: DataTableComponent<Mensagem>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private msg: MensagemService,
    private blockService: BlockUiService,
    private ts: TituloPaginaService,
    private minhasMensagensService: MinhasMensagensService) { }

  ngAfterViewInit() {
    this.changeTitulo();

    $('#internal_assuntoDescricao, #internal_textoMensagem').on('keydown', Utils.debounce(() => this.pesquisar(), 500));
  }

  ngOnDestroy() {
    for (let s of this.inscricoes) {
      s.unsubscribe();
    }
  }

  toogleTipoCaixa(tipoCaixa: string) {
    this.tipoCaixa = tipoCaixa;
    this.pesquisar();
  }

  novo() {
    this.router.navigate(['novo'], { relativeTo: this.route });
  }

  editar() {
    if (this.dt.getSelectedItem()) {
      this.router.navigate(['editar', this.dt.getSelectedItem().id], { relativeTo: this.route });
    } else {
      this.msg.nofiticationWarning('Nenhum item selecionado para alterar!');
    }
  }

  excluir() {
    if (this.dt.getSelectedItem()) {
      if (this.tipoCaixa == 'enviadas') {
        this.router.navigate(['excluir-autor', this.dt.getSelectedItem().id], { relativeTo: this.route });
      } else {
        this.router.navigate(['excluir', this.dt.getSelectedItem().id], { relativeTo: this.route });
      }

    } else {
      this.msg.nofiticationWarning('Nenhum item selecionado para excluir!');
    }
  }

  pesquisar() {
    this.changeTitulo();
    this.dt.load(1);
  }

  changeTitulo() {
    let tipoCaixaDesc: string = '';
    this.itensTipoCaixa.filter((t) => t.value == this.tipoCaixa).forEach((t) => tipoCaixaDesc = t.label);
    this.ts.setTitulo('Minhas mensagens - ' + tipoCaixaDesc);
  }

  limpar() {
    this.filtro = {
      "status": ""
    };
    this.pesquisar();
  }

  getMensagemLida(m: Mensagem) {
    return this.isMensagemNaoLida(m) ? '*' : undefined;
  }

  isMensagemNaoLida(m: Mensagem): boolean {
    return (m.destinatarios && m.destinatarios.length > 0 && m.destinatarios[0].status == 'Não lida');
  }

  paginacao(event: DataPaginationEvent) {
    // buscar totais
    let s1 = this.minhasMensagensService.getTotais().subscribe((retorno) => this.totais = retorno);

    this.inscricoes.push(s1);

    this.blockService.block('.table-consulta');

    let s2 = this.minhasMensagensService.getMensagens({
      filtro: this.filtro,
      order: this.order,
      pageSize: event.pageSize,
      pageNumber: event.page
    }, this.tipoCaixa).subscribe((dados) => {
      this.dados = dados;
      this.blockService.unBlock('.table-consulta');
    }, (error) => {
      this.dados = {
        lista: [],
        total: 0
      };

      this.blockService.unBlock('.table-consulta');
    });

    this.inscricoes.push(s2);
  }
}
