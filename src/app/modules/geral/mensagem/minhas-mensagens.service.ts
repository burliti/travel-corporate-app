import { TimerObservable } from 'rxjs/observable/TimerObservable';
import { Subscription } from 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { LoginService } from './../../../security/login.service';
import { Http, Headers } from '@angular/http';
import { ConsultaRequest } from './../../../components/model/consulta-request.model';
import { Injectable, Inject, EventEmitter } from '@angular/core';
import { ConsultaResponse } from '../../../components/model/consulta-response.model';
import { Mensagem, MensagemDados } from './mensagem.model';
import { AbstractCrudService } from '../../../components/services/abstract-crud.service';
import { CadastroResponse } from '../../../components/model/cadastro-response.model';

@Injectable()
export class MinhasMensagensService extends AbstractCrudService<Mensagem> {

  public onMessageReceive: EventEmitter<Mensagem[]> = new EventEmitter();

  private timerStarted = false;

  private inscricaoTimer: Subscription;

  constructor(@Inject(Http) _http: Http, @Inject(LoginService) loginService: LoginService) {
    super(_http, loginService);
  }

  getEntityName(): string {
    return "minhas-mensagens";
  }

  getUrl() {
    return '/api/messages/' + this.getEntityName() + '/' + this.getVersion();
  }

  /**
  * Excluir mensagens como autor
  */
  excluirAutor(id: any): Observable<CadastroResponse<Mensagem>> {
    return this._http.delete(this.getUrl() + '/' + id + '/Autor', { headers: this.getHeaders() })
      // .map(res => res.json())
      // .catch((error: any) => Observable.throw(error || 'Server error'));
      .map(this.extractData)
      .catch(this.handleError);
  }

  /**
  * Excluir mensagens como autor
  */
 excluirDestinatario(id: any): Observable<CadastroResponse<Mensagem>> {
  return this._http.delete(this.getUrl() + '/' + id + '/Destinatario', { headers: this.getHeaders() })
    // .map(res => res.json())
    // .catch((error: any) => Observable.throw(error || 'Server error'));
    .map(this.extractData)
    .catch(this.handleError);
}

  salvarRascunho(vo: Mensagem): Observable<CadastroResponse<Mensagem>> {
    return this._http.post(this.getUrl() + '/salvar', JSON.stringify(vo), { headers: this.getHeaders() })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  enviar(vo: Mensagem): Observable<CadastroResponse<Mensagem>> {
    return this._http.post(this.getUrl() + '/enviar', JSON.stringify(vo), { headers: this.getHeaders() })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  getDados(): Observable<MensagemDados> {
    return this._http.post(this.getUrl() + '/dados', null, { headers: this.getHeaders() })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  getTotais(): Observable<MensagemDados> {
    return this._http.post(this.getUrl() + '/totais', null, { headers: this.getHeaders() })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  /**
    * Retorna uma lista das mensagens recebidas
    */
  getMensagens(consultaRequest: ConsultaRequest, tipoCaixa: string): Observable<ConsultaResponse<Mensagem>> {
    return this._http.post(this.getUrl() + '/' + tipoCaixa,
      JSON.stringify(consultaRequest),
      { headers: this.getHeaders() })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  /**
   * Retorna as 3 ultimas mensagens para notificação no topo
   */
  buscarMensagensTop() {
    this.getMensagens({ filtro: {}, pageNumber: 1, pageSize: 3 }, 'recebidas').subscribe((retorno) => this.onMessageReceive.emit(retorno.lista));
  }

  /**
   * Marca uma mensagem como lida.
   * @param mensagem Mensagem a ser marcada como lida.
   */
  marcarLida(mensagem: Mensagem) {
    return this._http.post(this.getUrl() + '/marcarLida/' + mensagem.id,
      JSON.stringify({}),
      { headers: this.getHeaders() })
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  /**
     * Para o timer
     */
  stopTimer() {
    this.timerStarted = false;

    this.inscricaoTimer.unsubscribe();
  }

  /**
   * Inicia o timer para o keep alive
   */
  startTimer() {
    const timer = TimerObservable.create(1000, 10000);

    this.inscricaoTimer = timer.subscribe((t) => this.buscarMensagensTop());

    this.timerStarted = true;
  }
}
