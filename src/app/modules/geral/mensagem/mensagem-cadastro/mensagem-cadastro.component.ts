import { TituloPaginaService } from './../../../../components/services/titulo-pagina.service';
import { FormaNotificacaoEmpresa, FormaNotificacao, MensagemFormaNotificacao } from './../mensagem.model';
import { FormatUtils } from './../../../../plugins/format-utils';
import { MinhasMensagensService } from './../minhas-mensagens.service';
import { BlockUiService } from './../../../../plugins/block-ui/block-ui.service';
import { IndexComponent } from './../../../geral/index/index.component';
import { MensagemService } from './../../../../plugins/forms/mensagem.service';
import { SelectGroupOption } from './../../../../plugins/forms/select-group/select-group-option';
import { Subscription } from 'rxjs/Rx';
import { Component, OnInit } from '@angular/core';
import { AbstractCadastroComponent } from '../../../../components/geral/crud/abstract-cadastro.component';
import { EnumUtils, Utils } from '../../../../plugins/shared';
import { ActivatedRoute, Router } from '@angular/router';
import { Mensagem } from '../mensagem.model';

import { AfterViewInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { Funcionario } from '../../../cadastros/funcionario/funcionario.model';

declare var $: any;

@Component({
  selector: 'app-mensagem-cadastro',
  templateUrl: './mensagem-cadastro.component.html',
  styleUrls: ['./mensagem-cadastro.component.scss']
})
export class MensagemCadastroComponent extends AbstractCadastroComponent<Mensagem, MinhasMensagensService> implements AfterViewInit {

  inscricoes: Subscription[] = [];

  // Colecoes
  itensStatus: SelectGroupOption[] = EnumUtils.getOptionsAtivo();
  itensSimNao: SelectGroupOption[] = EnumUtils.getOptionsSimNao();
  itensFuncionarios: SelectGroupOption[] = [];
  itensFormaNotificacao: SelectGroupOption[] = [];
  funcionarios: Funcionario[] = [];
  formasNotificacaoEmpresa: FormaNotificacaoEmpresa[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private blockService: BlockUiService,
    private ts: TituloPaginaService,
    private minhasMensagemService: MinhasMensagensService,
    private msg: MensagemService) {
    super(route, router, blockService, msg);
  }

  salvar() {
    this.bloquear();

    let s = this.minhasMensagemService.salvarRascunho(this.vo).subscribe((retorno) => {
      this.desbloquear();

      if (retorno.success) {

        this.beforeVisualizar(retorno.vo);

        this.vo = retorno.vo;

        this.afterVisualizar(this.vo);

        this.msg.nofiticationInfo('Registro alterado com sucesso!');

        this.voltar();
      } else {
        this.msg.warn(Utils.buildErrorMessage(retorno)).then(() => {
          if (retorno.errors && retorno.errors.length > 0) {
            $(this.getFieldName(retorno.errors[0].fieldName)).focus();
          }
        });
      }
    }, (error) => {
      this.desbloquear();

      this.msg.warn(Utils.buildErrorMessage(error)).then(() => {
        if (error.errors && error.errors.length > 0) {
          $(this.getFieldName(error.errors[0].fieldName)).focus();
        }
      });
    });

    this.inscricoes.push(s);
  }

  enviar() {
    this.bloquear();

    let s = this.minhasMensagemService.enviar(this.vo).subscribe((retorno) => {
      this.desbloquear();

      if (retorno.success) {

        this.beforeVisualizar(retorno.vo);

        this.vo = retorno.vo;

        this.afterVisualizar(this.vo);

        this.msg.nofiticationInfo('Registro alterado com sucesso!');

        this.voltar();
      } else {
        this.msg.warn(Utils.buildErrorMessage(retorno)).then(() => {
          if (retorno.errors && retorno.errors.length > 0) {
            $(this.getFieldName(retorno.errors[0].fieldName)).focus();
          }
        });
      }
    }, (error) => {
      this.desbloquear();

      this.msg.warn(Utils.buildErrorMessage(error)).then(() => {
        if (error.errors && error.errors.length > 0) {
          $(this.getFieldName(error.errors[0].fieldName)).focus();
        }
      });
    });

    this.inscricoes.push(s);
  }

  ngOnInit() {
    let s1 = this.minhasMensagemService.getDados().subscribe((retorno) => {

      if (!retorno.formasNotificacao) {
        retorno.formasNotificacao = [];
      }
      if (!retorno.funcionarios) {
        retorno.funcionarios = [];
      }

      retorno.funcionarios.forEach((f) => this.itensFuncionarios.push({ label: f.nome, value: f.id }));
      retorno.formasNotificacao.forEach((f) => this.itensFormaNotificacao.push({ label: f.formaNotificacao.nome, value: f.id.toString() }));

      this.funcionarios = retorno.funcionarios;
      this.formasNotificacaoEmpresa = retorno.formasNotificacao;

      super.ngOnInit();
    });

    this.inscricoes.push(s1);
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();

    this.changeTitulo();

    $('#internal_destinatariosAux').select2();
  }

  changeTitulo() {
    let titulo = 'Nova mensagem - ';
    if (this.vo.id && this.vo.id > 0) {
      if (this.isDesabilitado()) {
        titulo = 'Mensagem - ';
      } else {
        titulo = 'Editar Mensagem - ';
      }
    }
    this.ts.setTitulo(titulo + (this.vo.assuntoDescricao ? this.vo.assuntoDescricao : '(Sem assunto)'));
  }

  changeFuncionarios() {
    this.vo.destinatarios = [];

    this.vo.destinatariosAux.forEach((idAux) => {
      // Para cada um, busca na lista de itens o funcionario selecionado
      this.funcionarios.filter(f => { return f.id == idAux }).forEach((funcionario) => this.vo.destinatarios.push({ destinatario: funcionario }));
    });
  }

  beforeVisualizar(vo: Mensagem) {
    vo.destinatariosAux = [];
    if (vo.destinatarios) {
      vo.destinatarios.forEach((d) => vo.destinatariosAux.push(d.destinatario.id));
    } else {
      vo.destinatarios = [];
    }

    if (!vo.formasNotificacao) {
      vo.formasNotificacao = [];
    }
  }

  afterVisualizar(vo: Mensagem) {
    this.changeTitulo();

    // Marcar como lida
    if (this.isDesabilitado()) {
      let s = this.minhasMensagemService.marcarLida(vo).subscribe();
      this.inscricoes.push(s);
    }
  }

  toogleFormaNotificacao(fm: FormaNotificacaoEmpresa) {
    let r: MensagemFormaNotificacao;

    this.vo.formasNotificacao.filter(f => f.formaNotificacao.id == fm.formaNotificacao.id).forEach(f => r = f);

    if (r) {
      // Se encontrou, remove
      let index = this.vo.formasNotificacao.indexOf(r);
      if (index >= 0) {
        this.vo.formasNotificacao.splice(index, 1);
      }
    } else {
      // Se nao encontrou, adiciona
      this.vo.formasNotificacao.push(fm);
    }
  }

  isFormaNotificacaoSelecionada(fm: FormaNotificacaoEmpresa): boolean {
    let r: MensagemFormaNotificacao;

    this.vo.formasNotificacao.filter(f => f.formaNotificacao.id == fm.formaNotificacao.id).forEach(f => r = f);

    if (r) {
      return true;
    } else {
      return false;
    }
  }

  isDesabilitado() {
    return !(this.vo.status == 'Rascunho');
  }

  excluir() {
    this.msg.confirm('Confirma a EXCLUSÃO deste registro?').then(() => {
      if (this.acao == 'excluir-autor') {
        const s: Subscription = this.getService().excluirAutor(this.id).subscribe((retorno) => {
          if (retorno.success) {
            this.msg.nofiticationInfo('Registro excluído com sucesso!');
            this.voltar();
          } else {
            this.msg.warn(Utils.buildErrorMessage(retorno)).then(() => {
              if (retorno.errors && retorno.errors.length > 0) {
                $(this.getFieldName(retorno.errors[0].fieldName)).focus();
              }
            });
          }
        }, (retorno) => {
          this.msg.warn(Utils.buildErrorMessage(retorno)).then(() => {
            if (retorno.errors && retorno.errors.length > 0) {
              $(this.getFieldName(retorno.errors[0].fieldName)).focus();
            }
          });
        });
        this.inscricoes.push(s);
      } else {
        const s: Subscription = this.getService().excluirDestinatario(this.id).subscribe((retorno) => {
          if (retorno.success) {
            this.msg.nofiticationInfo('Registro excluído com sucesso!');
            this.voltar();
          } else {
            this.msg.warn(Utils.buildErrorMessage(retorno)).then(() => {
              if (retorno.errors && retorno.errors.length > 0) {
                $(this.getFieldName(retorno.errors[0].fieldName)).focus();
              }
            });
          }
        }, (retorno) => {
          this.msg.warn(Utils.buildErrorMessage(retorno)).then(() => {
            if (retorno.errors && retorno.errors.length > 0) {
              $(this.getFieldName(retorno.errors[0].fieldName)).focus();
            }
          });
        });
        this.inscricoes.push(s);
      }
    }).catch(() => {
      // Do nothing.
    });


  }

  getService(): MinhasMensagensService {
    return this.minhasMensagemService;
  }

  getVoNewInstance(): Mensagem {
    return {
      status: 'Rascunho',
      formasNotificacao: [],
      destinatarios: []
    };
  }
}
