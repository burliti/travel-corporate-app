import { MensagemCadastroComponent } from './mensagem-cadastro/mensagem-cadastro.component';
import { AutenticacaoGuard } from './../../../security/autenticacao.guard';
import { MensagemComponent } from './mensagem.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MensagemConsultaComponent } from './mensagem-consulta/mensagem-consulta.component';

const routes: Routes = [{
  path: 'minhas-mensagens', component: MensagemComponent, canActivate: [AutenticacaoGuard], children: [
    { path: '', component: MensagemConsultaComponent },
    {
      path: ':acao', children: [
        { path: '', component: MensagemCadastroComponent },
        { path: ':id', component: MensagemCadastroComponent }
      ]
    }
  ]
}];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MensagemRoutingModule { }
