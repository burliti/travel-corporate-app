import { MinhasMensagensService } from './minhas-mensagens.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MensagemRoutingModule } from './mensagem-routing.module';
import { MensagemComponent } from './mensagem.component';
import { MensagemCadastroComponent } from './mensagem-cadastro/mensagem-cadastro.component';
import { MensagemConsultaComponent } from './mensagem-consulta/mensagem-consulta.component';
import { FormsModule } from '@angular/forms';
import { PluginsModule } from '../../../plugins/plugins.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PluginsModule,
    MensagemRoutingModule
  ],
  providers: [MinhasMensagensService],
  declarations: [MensagemComponent, MensagemCadastroComponent, MensagemConsultaComponent]
})
export class MensagemModule { }
