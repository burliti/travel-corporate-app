import { LogoffComponent } from './components/geral/logoff/logoff.component';
import { AutenticacaoGuard } from './security/autenticacao.guard';
import { LoginComponent } from './modules/geral/login/login.component';
import { IndexComponent } from './modules/geral/index/index.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'index', component: IndexComponent, canActivate: [AutenticacaoGuard] },
  { path: '', redirectTo: 'index', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'logoff', component: LogoffComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingRoutingModule { }
