import { TituloPaginaService } from './components/services/titulo-pagina.service';
import { LoginService } from './security/login.service';
import { Component, OnInit } from '@angular/core';

declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';

  constructor(private loginService: LoginService, private tituloPaginaService: TituloPaginaService) {

  }

  ngOnInit() {
    $(window).trigger('load');
  }

  isAutenticado() {
    return this.loginService.isAutenticado();
  }

  getTitulo() {
    return this.tituloPaginaService.getTitulo();
  }
}
