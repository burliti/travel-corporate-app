import { Observable } from 'rxjs/Rx';
import { Http } from '@angular/http';
import { Component, OnInit, Input, Output, Host, EventEmitter, forwardRef, ElementRef, AfterViewInit } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

import { Utils } from './../../shared';
import { SelectGroupOption } from '../select-group/select-group-option';
import { SimpleChanges, OnDestroy, OnChanges } from '@angular/core/src/metadata/lifecycle_hooks';

// Função sem conteudo, apenas para nao dar null pointer no call das referencias.
const noop = () => {
};

declare var $: any;

export const CUSTOM_SELECT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => SelectGroup2Component),
  multi: true
};

@Component({
  selector: 'select-group-2',
  templateUrl: './select-group-2.component.html',
  styleUrls: ['./select-group-2.component.scss'],
  providers: [CUSTOM_SELECT_CONTROL_VALUE_ACCESSOR]
})
export class SelectGroup2Component implements OnChanges, OnDestroy, AfterViewInit {

  // Texto de ajuda, exibido abaixo do input
  @Input() help: string;

  // Texto do label exibido acima do input
  @Input() label: string;

  // Largura , de 1 a 12, para o grid system.
  @Input() width: number = 3;

  // Itens da combo
  @Input() options: SelectGroupOption[] = [];

  // Indica se o input estará desabilitado
  @Input() disabled: boolean = false;

  // Indica se o select será multi-seleção
  @Input() multiple: boolean = false;

  // URL para o select2 fazer a pesquisa (padrao REST)
  @Input() urlSearch: string = '';

  // Quando usado o padrao REST com urlSearch, este campo diz o tamanho máximo de páginas a serem trazidas.
  @Input() maxResults: number = 10;

  private model: any; // inner value

  select2: any;

  private onChange: (_: any) => void;
  private onTouched: () => void;
  @Output() onSelect = new EventEmitter<any>();

  // Id do componente select
  @Input('id') componentId: string;

  // Evento disparado no "change" do input
  @Output() change: EventEmitter<any> = new EventEmitter();

  constructor(private el: ElementRef, private _http: Http) { }

  ngAfterViewInit() {
    this.select2.select2('val', [this.value]);
  }

  getData() {
    const opt = [];

    this.options.forEach(element => {
      opt.push({ id: element.value, text: element.label });
    });

    return opt;
  }

  ngOnChanges(changes: SimpleChanges) {
    // Checking if the plugin is initialized
    if (this.select2 && this.select2.hasClass("select2-hidden-accessible")) {
      // Select2 has been initialized
      if (changes.options) {
        // options change
        this.select2.empty();
        this.select2.select2({ data: this.options });
      }
      this.select2.trigger('change');
    } else {
      this.select2 = $(this.el.nativeElement).find('select').select2({
        data: this.options,
      }).on('select2:select', (ev: any) => {
        const { id, text } = ev['params']['data'];
        this.value = id;
        this.onSelect.emit({ id, text });
      });
    }
    this.select2.select2('val', [this.value]);
  }

  // get accessor
  get value(): any {
    return this.model;
  }

  // set accessor including call the onchange callback
  set value(v: any) {
    if (v !== this.model) {
      this.model = v;
      this.onChange(v);
    }
  }

  // Set touched on blur
  onBlur() {
    this.onTouched();
  }

  // Write a new value to the element.
  writeValue(value: string): void {
    if (value !== this.model) {
      this.model = value;
    }
  }

  // Set the function to be called when the control receives a change event.
  registerOnChange(fn: (_: any) => {}): void {
    this.onChange = fn;
  }

  // registerOnTouched(fn: any) : void
  registerOnTouched(fn: () => {}): void {
    this.onTouched = fn;
  }

  ngOnDestroy() {
    this.select2.select2('destroy');
  }

  getOptionGroups(): string[] {
    let retorno: string[] = [];

    if (this.options) {
      for (let o of this.options) {
        if (o.group) {
          if (retorno.indexOf(o.group) < 0) {
            retorno.push(o.group);
          }
        }
      }
    }

    if (retorno.length > 0) {
      return retorno
    } else {
      return null;
    }
  }
}
