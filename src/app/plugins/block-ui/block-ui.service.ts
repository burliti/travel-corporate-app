import { element } from 'protractor';
import { Injectable } from '@angular/core';

declare var $: any;

@Injectable()
export class BlockUiService {

  constructor() { }

  private loader = '<div class="cssload-loader"><div class="cssload-inner cssload-one"></div><div class="cssload-inner cssload-two"></div><div class="cssload-inner cssload-three"></div></div>';

  /**
   * Bloqueia a tela ou um unico elemento seletor
   * @param element Elemento a ser bloqueado
   */
  block(element: string = undefined): void {
    if (element) {
      // Bloqueia elemento
      $(element).block({ message: this.loader });
    } else {
      // Bloqueia pagina
      $.blockUI({ message: this.loader });
    }
  }

  /**
   * Desbloqueia a tela ou um único elemento seletor
   * @param element Elemento a ser desbloqueado
   */
  unBlock(element: string = undefined): void {
    if (element) {
      // Debloqueia elemento
      $(element).unblock();
    } else {
      // Desbloqueia pagina
      $.unblockUI();
    }
  }
}
