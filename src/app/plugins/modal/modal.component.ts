import { Component, OnInit, Input } from '@angular/core';

declare var $: any;

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  @Input() titulo: string;

  @Input() title: string;

  @Input('sub-titulo') subTitulo: string;

  @Input('subTitle') subTitle: string;

  @Input('icon') classIcon: string;

  @Input('modal-id') componentId: string = 'myModal';

  @Input() large: boolean = false;

  @Input() modal : boolean = false;

  constructor() { }

  ngOnInit() {
  }

  show() {
    $('#' + this.componentId).modal({
      backdrop : 'static',
      keyboard : !this.modal
    });
  }

  close() {
    $('#' + this.componentId).modal('hide');
  }

  getLargeClass() {
    return this.large ? 'modal-lg' : '';
  }

  getTitulo() {
    return this.title || this.titulo;
  }

  getSubTitulo() {
    return this.subTitle || this.subTitulo;
  }
}
