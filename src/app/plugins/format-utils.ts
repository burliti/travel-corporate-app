import { SelectGroupOption } from "./forms/select-group/select-group-option";

export class FormatUtils {
  static dataToDatePicker(data: string): string {
    if (!data) {
      return null;
    } else {
      return data.substr(6, 4) + '-' + data.substr(3, 2) + '-' + data.substr(0, 2);
    }
  }

  static datePickerToData(datePicker: string): string {
    if (!datePicker) {
      return null;
    } else {
      return datePicker.substr(8, 2) + '/' + datePicker.substr(5, 2) + '/' + datePicker.substr(0, 4);
    }
  }

  static formatMoney(valor: number): string {
    if (!valor) {
      valor = 0;
    }

    var formatter = new Intl.NumberFormat('pt-BR', {
      style: 'decimal',
      currency: 'BRL',
      minimumFractionDigits: 2,
    });

    const v: string = formatter.format(valor);

    return v;
  }

  static parseMoney(valor: string): number {
    if (valor) {
      try {
        let v: number = parseFloat(valor.replace('.', '').replace(',', '.').replace('R', '').replace('$', ''));

        if (isNaN(v)) {
          v = 0.0;
        }

        return v;
      } catch (err) {
        console.log(err);
      }
    }
    return 0.0;
  }

  /**
   * Preenche uma string com um caractere à esquerda
   * @param source String original
   * @param padWith String a preencher
   * @param length Tamanho total
   */
  static fill(source, padWith, length) {
    if (!source) {
      source = '';
    }
    while (source.toString().length < length) {
      source = padWith + source;
    }
    return source;
  }




  static toOption(itens: SelectGroupOption[]): any[] {
    let result: any[] = [];

    itens.forEach((i) => result.push({ id: i.value, text: i.label }));

    return result;
  }

}
