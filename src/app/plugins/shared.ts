import { SelectGroupOption } from './forms/select-group/select-group-option';
import { CadastroResponse } from './../components/model/cadastro-response.model';

export class Utils {
  static getRandomNumber() {
    return Math.floor(Math.random() * (1000 - 1 + 1)) + 1;
  }

  static buildErrorMessage<T>(response: CadastroResponse<T>): string {
    //
    let msg = response.message;

    if (response.errors && response.errors.length > 0) {
      msg += '<div class="text-left">';
      msg += '<ul>';

      for (const error of response.errors) {
        msg += '<li>';
        msg += error.message;
        msg += '</li>';
      }
      msg += '</ul>';
      msg += '</div>';
    }

    return msg;
  }

  static parseNumber(value: any) {
    if (!value) {
      return 0.0;
    }

    let v: string = value.toString();

    if (v.indexOf('R$') >= 0) {
      v = v.replace('R$', '');
      v = v.replace('.', '');
      v = v.replace(',', '.');
    }

    let f = parseFloat(v);

    return f;
  }

  static formatDate(data: Date): string {
    let dd = data.getDate().toString();
    let mm = new String(data.getMonth() + 1); //January is 0!
    var yyyy = data.getFullYear();

    if (data.getDate() < 10) {
      dd = '0' + dd
    }

    if (data.getMonth() < 10) {
      mm = '0' + mm
    }

    let retorno = dd + '/' + mm + '/' + yyyy;

    return retorno;

  }

  static debounce(func: any, wait: number, immediate: boolean = false) {
    var timeout;

    return function () {
      var context = this, args = arguments;

      clearTimeout(timeout);

      timeout = setTimeout(function () {
        timeout = null;
        console.log('Call');
        if (!immediate) func.apply(context, args);
      }, wait);
      if (immediate && !timeout) func.apply(context, args);
    };
  }


}

export class EnumUtils {

  /**
   * Retorna lista de options para combo Ativo / Inativo
   */
  static getOptionsAtivo(opcaoVazia: string = null): SelectGroupOption[] {
    const lista: SelectGroupOption[] = [];

    if (opcaoVazia) {
      lista.push({ label: opcaoVazia, value: '' });
    }

    lista.push({ label: 'Ativo', value: 'Ativo' });
    lista.push({ label: 'Inativo', value: 'Inativo' });

    return lista;
  }

  /**
  * Retorna lista de options para combo Ativo / Inativo
  */
  static getOptionsStatusMensagem(opcaoVazia: string = null): SelectGroupOption[] {
    const lista: SelectGroupOption[] = [];

    if (opcaoVazia) {
      lista.push({ label: opcaoVazia, value: '' });
    }

    lista.push({ label: 'Rascunho', value: 'Rascunho' });
    lista.push({ label: 'Enviada', value: 'Enviada' });
    lista.push({ label: 'Excluída', value: 'Excluída' });

    return lista;
  }

  /**
   * Retorna lista de options para combo de status da viagem
   */
  static getOptionsStatusViagem(opcaoVazia: string = null): SelectGroupOption[] {
    const lista: SelectGroupOption[] = [];

    if (opcaoVazia) {
      lista.push({ label: opcaoVazia, value: '' });
    }

    lista.push({ label: 'Solicitada', value: 'Solicitada' });
    lista.push({ label: 'Planejamento', value: 'Planejamento' });
    lista.push({ label: 'Viagem aprovada', value: 'Viagem aprovada' });
    lista.push({ label: 'Em Andamento', value: 'Em Andamento' });
    lista.push({ label: 'Finalizado', value: 'Finalizado' });
    lista.push({ label: 'Relatório em análise', value: 'Relatório em análise' });
    lista.push({ label: 'Relatório aprovado', value: 'Relatório aprovado' });
    lista.push({ label: 'Relatório rejeitado', value: 'Relatório rejeitado' });

    return lista;
  }

  /**
  * Retorna lista de options para combo de status da viagem
  */
  static getOptionsStatusViagemAprovacao(opcaoVazia: string = null): SelectGroupOption[] {
    const lista: SelectGroupOption[] = [];

    if (opcaoVazia) {
      lista.push({ label: opcaoVazia, value: '' });
    }

    lista.push({ label: 'Finalizado', value: 'Finalizado' });
    lista.push({ label: 'Relatório em análise', value: 'Relatório em análise' });
    lista.push({ label: 'Relatório aprovado', value: 'Relatório aprovado' });
    lista.push({ label: 'Relatório rejeitado', value: 'Relatório rejeitado' });

    return lista;
  }

  /**
   * Retorna lista de options para combo de tipo de lancamento
   */
  static getOptionsTipoLancamento(opcaoVazia: string = null): SelectGroupOption[] {
    const lista: SelectGroupOption[] = [];

    if (opcaoVazia) {
      lista.push({ label: opcaoVazia, value: '' });
    }

    lista.push({ label: 'Crédito', value: 'Crédito' });
    lista.push({ label: 'Débito', value: 'Débito' });

    return lista;
  }

  /**
   * Retorna lista de options para Sim / Não
   */
  static getOptionsSimNao(opcaoVazia: string = null): SelectGroupOption[] {

    const lista: SelectGroupOption[] = [];

    if (opcaoVazia) {
      lista.push({ label: opcaoVazia, value: '' });
    }

    lista.push({ label: 'Sim', value: 'Sim' });
    lista.push({ label: 'Não', value: 'Não' });

    return lista;
  }

  /**
  * Retorna lista de options para Tipo de Pessoa
  */
  static getOptionsTipoPessoa(opcaoVazia: string = null): SelectGroupOption[] {

    const lista: SelectGroupOption[] = [];

    if (opcaoVazia) {
      lista.push({ label: opcaoVazia, value: '' });
    }

    lista.push({ label: 'Física', value: 'Física' });
    lista.push({ label: 'Jurídica', value: 'Jurídica' });

    return lista;
  }


  /**
   * Retorna lista de options para Entrada / Saída
   */
  static getOptionsTipoPorta(opcaoVazia: string = null): SelectGroupOption[] {
    const lista: SelectGroupOption[] = [];

    if (opcaoVazia) {
      lista.push({ label: opcaoVazia, value: '' });
    }

    lista.push({ label: 'Entrada', value: 'Entrada' });
    lista.push({ label: 'Saída', value: 'Saída' });

    return lista;
  }

   /**
   * Retorna lista de options para Sim / Não
   */
  static getOptionsTipoVeiculo(opcaoVazia: string = null): SelectGroupOption[] {

    const lista: SelectGroupOption[] = [];

    if (opcaoVazia) {
      lista.push({ label: opcaoVazia, value: '' });
    }

    lista.push({ label: 'Empresa', value: 'Empresa' });
    lista.push({ label: 'Alugado', value: 'Alugado' });
    lista.push({ label: 'Outros', value: 'Outros' });

    return lista;
  }
}
