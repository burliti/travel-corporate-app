import { SelectGroup2Component } from './forms/select-group-2/select-group-2.component';
import { TruncatePipe } from './truncate.pipe';
import { VersionApiService } from './../components/geral/version/version.api.service';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { OrderPipe } from './orderBy/order-by.pipe';
import { BlockUiService } from './block-ui/block-ui.service';
import { InputDisabledComponent } from './forms/input-disabled/input-disabled.component';
import { BotaoComponent } from './forms/buttons/botao.component';
import { TextAreaGroupComponent } from './forms/textarea-group/textarea-group.component';
import { RouterModule } from '@angular/router';
import { SideBarComponent } from './../components/geral/side-bar/side-bar.component';
import { FooterComponent } from './../components/geral/footer/footer.component';
import { HeaderComponent } from './../components/geral/header/header.component';
import { BaseComponent } from './../components/geral/base/base.component';
import { BotaoAlterarComponent } from './forms/buttons/botao-alterar.component';
import { BotaoGridExcluirComponent } from './forms/buttons/botao-grid-excluir.component';
import { BotaoGridAlterarComponent } from './forms/buttons/botao-grid-alterar.component';
import { BotaoExcluirComponent } from './forms/buttons/botao-excluir.component';
import { BotaoVoltarComponent } from './forms/buttons/botao-voltar.component';
import { BotaoSalvarComponent } from './forms/buttons/botao-salvar.component';
import { BotaoLimparPesquisaComponent } from './forms/buttons/botao-limpar-pesquisa.component';
import { BotaoPesquisarComponent } from './forms/buttons/botao-pesquisar.component';
import { MensagemService } from './forms/mensagem.service';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataTableComponent, ButtonsRightComponent } from './data-table/data-table.component';
import { ColumnComponent } from './data-table/column/column.component';
import { PaginatorComponent } from './data-table/paginator/paginator.component';
import { PanelComponent } from './forms/panel/panel.component';
import { ColumnTemplateComponent } from './data-table/column-template/column-template.component';
import { InputGroupComponent } from './forms/input-group/input-group.component';
import { SelectGroupComponent } from './forms/select-group/select-group.component';
import { RowComponent, ColSm1Component, ColSm2Component, ColSm3Component, ColSm4Component, ColSm5Component, ColSm6Component, ColSm7Component, ColSm8Component, ColSm9Component, ColSm10Component, ColSm11Component, ColSm12Component } from './forms/grid/grid-system.component';
import { BotaoNovoComponent } from './forms/buttons/botao-novo.component';
import { TextMaskModule } from 'angular2-text-mask';
import { SelectGroupFilterPipe } from './forms/select-group-filter.pipe';
import { PaddingPipe } from './padding.pipe';
import { ModalComponent } from './modal/modal.component';
import { VersionComponent } from '../components/geral/version/version.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TextMaskModule,
    RouterModule
  ],
  providers: [MensagemService, BlockUiService, VersionApiService],
  declarations: [
    HeaderComponent,
    FooterComponent,
    SideBarComponent,
    BaseComponent,
    VersionComponent,

    DataTableComponent,
    ButtonsRightComponent,
    ColumnComponent,
    PaginatorComponent,
    PanelComponent,
    ColumnTemplateComponent,
    InputGroupComponent,
    InputDisabledComponent,
    TextAreaGroupComponent,
    SelectGroupComponent,
    SelectGroup2Component,
    RowComponent,

    ColSm1Component,
    ColSm2Component,
    ColSm3Component,
    ColSm4Component,
    ColSm5Component,
    ColSm6Component,
    ColSm7Component,
    ColSm8Component,
    ColSm9Component,
    ColSm9Component,
    ColSm10Component,
    ColSm11Component,
    ColSm12Component,

    BotaoNovoComponent,
    BotaoPesquisarComponent,
    BotaoLimparPesquisaComponent,
    BotaoSalvarComponent,
    BotaoVoltarComponent,
    BotaoExcluirComponent,
    BotaoAlterarComponent,
    BotaoGridAlterarComponent,
    BotaoGridExcluirComponent,
    BotaoComponent,
    SelectGroupFilterPipe,
    PaddingPipe,
    ModalComponent,
    OrderPipe,
    TruncatePipe,
    FileUploadComponent],
  exports: [
    HeaderComponent,
    FooterComponent,
    SideBarComponent,
    BaseComponent,
    VersionComponent,

    DataTableComponent,
    ButtonsRightComponent,
    ColumnComponent,
    PanelComponent,
    InputGroupComponent,
    InputDisabledComponent,
    TextAreaGroupComponent,
    SelectGroupComponent,
    SelectGroup2Component,
    RowComponent,

    ColSm1Component,
    ColSm2Component,
    ColSm3Component,
    ColSm4Component,
    ColSm5Component,
    ColSm6Component,
    ColSm7Component,
    ColSm8Component,
    ColSm9Component,
    ColSm9Component,
    ColSm10Component,
    ColSm11Component,
    ColSm12Component,

    BotaoNovoComponent,
    BotaoPesquisarComponent,
    BotaoLimparPesquisaComponent,
    BotaoSalvarComponent,
    BotaoVoltarComponent,
    BotaoExcluirComponent,
    BotaoAlterarComponent,
    BotaoComponent,
    BotaoGridAlterarComponent,
    BotaoGridExcluirComponent,
    SelectGroupFilterPipe,
    PaddingPipe,
    ModalComponent,
    OrderPipe,
    TruncatePipe,
    FileUploadComponent]
})
export class PluginsModule { }
