import { Empresa } from './../modules/configuracoes/empresa/empresa.model';
import { Funcionario } from './../modules/cadastros/funcionario/funcionario.model';

/**
 * Classe que representa a sessao do usuario no servidor
 */
export class SessaoUsuario {
  chaveSessao?:string;
  empresa?: Empresa;
  dataCriacao?: string;
  ultimaInteracao?: string;
  enderecoIp?: string;
  hostName?: string;
  funcionario?: Funcionario;
}
