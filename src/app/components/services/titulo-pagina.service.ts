import { Injectable, EventEmitter } from '@angular/core';
import { Title } from '@angular/platform-browser';

declare var $ : any;

@Injectable()
export class TituloPaginaService {

  private titulo: string = "";
  private tituloApp : string = "Travel Corporate";

  // Emissor de evento indicando aos listener's que o texto mudou
  public emitter: EventEmitter<string> = new EventEmitter();

  constructor(private title : Title) { }

  /**
   * Serviço para setar o título da página
   */
  setTitulo(titulo: string) {
    this.titulo = titulo;

    // Seta o título da página
    this.title.setTitle(this.titulo + " - " + this.tituloApp);

    // Disparando este evento, irá alterar o título da página.
    this.emitter.emit(this.titulo);

    // Força o evento de reload da pagina
    $(window).trigger('load');
  }

  getTitulo() {
    return this.titulo;
  }
}
