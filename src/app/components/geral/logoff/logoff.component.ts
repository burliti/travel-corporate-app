import { LoginService } from './../../../security/login.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-logoff',
  templateUrl: './logoff.component.html',
  styleUrls: ['./logoff.component.css']
})
export class LogoffComponent implements OnInit {

  constructor(private router : Router, private loginService : LoginService) { }

  ngOnInit() {
    this.loginService.logoff();
  }

}
