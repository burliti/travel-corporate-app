import { MensagemService } from './../../../plugins/forms/mensagem.service';
import { Router } from '@angular/router';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { MinhasMensagensService } from './../../../modules/geral/mensagem/minhas-mensagens.service';
import { Mensagem } from './../../../modules/geral/mensagem/mensagem.model';
import { SessaoUsuario } from './../../../security/sessao.model';
import { LoginService } from './../../../security/login.service';
import { Component, OnInit } from '@angular/core';

const { version: appVersion } = require('../../../../../package.json')

declare var $: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {

  sessaoUsuario: SessaoUsuario;
  version: string = '0.0.0';
  mensagens: Mensagem[] = [];

  constructor(private loginService: LoginService, private mms: MinhasMensagensService, private router: Router, private ms: MensagemService) { }

  ngOnInit() {
    this.version = appVersion;

    this.sessaoUsuario = this.loginService.getUsuarioAutenticado();

    $(function () {
      $('#side-menu').metisMenu();
    });

    // Recebe as novas mensagens
    this.mms.onMessageReceive.subscribe((retorno) => {
      this.mensagens = retorno;

      // TODO Checa por novas mensagens para fazer a notificação
      this.checkNewMessages();
    });

    // Inicia o timer de mensagens
    this.mms.startTimer();
  }

  checkNewMessages() {
    let list: Mensagem[] = JSON.parse(sessionStorage.getItem('mensagens'));

    if (list && list.length > 0) {
      this.mensagens.filter((m) => {
        return list.filter((m2) => m2.id == m.id).length <= 0;
      })
        .forEach((m) => this.notificar(m));
    } else {
      this.mensagens.forEach((m) => this.notificar(m));
    }

    sessionStorage.setItem('mensagens', JSON.stringify(this.mensagens));
  }

  /**
   *
   * @param m
   */
  notificar(m: Mensagem) {
    this.getPermission()
      .then(() => {

        setTimeout(() => {
          var n = new Notification(m.assuntoDescricao, {
            // use \n para quebrar linhas
            body: 'Enviada por ' + m.autor.nome,
            icon: 'assets/img/icon_512.png'
          });

          n.onclick = () => this.router.navigate(['/minhas-mensagens/editar', m.id]);
        }, 1000);
      })
      .catch((status) => console.error(status));
    // Notificação interna
    this.ms.nofiticationInfo(m.textoMensagem, 5000, m.assuntoDescricao);
  }

  getPermission() {
    return new Promise((resolve, reject) => {
      Notification.requestPermission().then(status => {
        if (status == 'granted') {
          resolve();
        } else {
          reject(status);
        }
      });
    });
  }


  getFotoFuncionario() {
    return this.loginService.getFotoFuncionario(this.sessaoUsuario);
  }

  getTotalNaoLidas() {
    return this.mensagens.filter((m) => m.destinatarios.length > 0 && m.destinatarios[0].status == 'Não lida').length;
  }

  ngOnDestroy() {
    this.mms.stopTimer();
  }
}
