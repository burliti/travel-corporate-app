import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class VersionApiService {

  constructor(private http: Http) { }

  getApiResource(): Observable<string> {
    return this.http.get('/api/resources/versao/v1').map(res => res.text());
  }

  getApiReport(): Observable<string> {
    return this.http.get('/api/reports/versao/v1').map(res => res.text());
  }

  getApiMessages(): Observable<string> {
    return this.http.get('/api/messages/versao/v1').map(res => res.text());
  }
}
