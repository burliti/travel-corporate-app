import { VersionApiService } from './version.api.service';
import { ModalComponent } from './../../../plugins/modal/modal.component';
import { Component, OnInit, Input, ViewChild, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

const { version: appVersion } = require('../../../../../package.json')

@Component({
  selector: 'app-version',
  templateUrl: './version.component.html',
  styleUrls: ['./version.component.scss']
})
export class VersionComponent implements OnInit, OnDestroy {

  @Input() negative: string = "false";

  version: string = '0.0.0';

  versaoApiResource: string = '0.0.0';
  versaoApiReport: string = '0.0.0';
  versaoMessagesReport: string = '0.0.0';

  inscricoes: Subscription[] = [];

  @ViewChild('modalVersao') modal: ModalComponent

  constructor(private versionApiService: VersionApiService) { }

  ngOnInit() {
    this.version = appVersion;

    this.updateVersions();
  }

  private updateVersions() {
    let s1 = this.versionApiService.getApiResource().subscribe((v) => this.versaoApiResource = v);
    let s2 = this.versionApiService.getApiReport().subscribe((v) => this.versaoApiReport = v);
    let s3 = this.versionApiService.getApiMessages().subscribe((v) => this.versaoMessagesReport = v);
    this.inscricoes.push(s1);
    this.inscricoes.push(s2);
    this.inscricoes.push(s3);
  }

  showVersao() {
    this.updateVersions();
    this.modal.show();
  }

  isNonNegative(): boolean {
    return this.negative !== "true";
  }

  isNegative(): boolean {
    return this.negative === "true";
  }

  ngOnDestroy() {
    this.inscricoes.forEach((s) => s.unsubscribe());
  }
}
