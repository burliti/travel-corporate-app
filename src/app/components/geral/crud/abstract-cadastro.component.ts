import { BlockUiService } from './../../../plugins/block-ui/block-ui.service';
import { Utils } from './../../../plugins/shared';
import { MensagemService } from './../../../plugins/forms/mensagem.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { Component, OnInit, Input, AfterViewInit, OnDestroy } from '@angular/core';
import { AbstractCrudService } from '../../services/abstract-crud.service';

declare var $: any;

export abstract class AbstractCadastroComponent<T, S extends AbstractCrudService<T>> implements OnInit, AfterViewInit, OnDestroy {
  @Input() public vo: T;

  public inscricoes: Subscription[] = [];

  @Input() public acao: string;
  public id: any;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _blockService: BlockUiService,
    private _msg: MensagemService) {
    this.vo = this.getVoNewInstance();
  }

  ngOnInit() {
    this.bloquear();

    const s = this._route.params.subscribe((params: any[]) => {
      this.id = params['id'];
      this.acao = params['acao'];

      if (this.id) {
        const s2 = this.getService().buscar(this.id).subscribe((retorno) => {
          this.beforeVisualizar(retorno);

          this.vo = retorno;

          this.afterVisualizar(this.vo);

          this.desbloquear();
        }, (error) => {
          console.log(error);
          this.desbloquear();
        });
        this.inscricoes.push(s2);
      } else {
        this.vo = this.getVoNewInstance();
        this.beforeVisualizar(this.vo);
        this.afterVisualizar(this.vo);

        this.desbloquear();
      }
    });
    this.inscricoes.push(s);
  }

  public bloquear() {
    this._blockService.block('panel > #panel-cadastro');
  }

  public desbloquear() {
    this._blockService.unBlock('panel > #panel-cadastro');
  }

  ngAfterViewInit() {
    if (!this.isExcluir()) {
      // TODO Pegar o primeiro input
      $('#internal_nome').focus();
    } else {
      $('botao-excluir').focus();
    }
  }

  beforeVisualizar(vo: T) {
    //
  }

  afterVisualizar(vo: T) {
    //
  }

  beforeSalvar(vo: T) {
    return true;
  }

  // Depreciado.
  isExcluir() {
    return this.isDesabilitar();
  }

  isNovo() {
    return this.acao && this.acao === 'novo';
  }

  isDesabilitar() {
    return this.acao && (this.acao === 'excluir' || this.acao === 'visualizar');
  }

  /**
   * Retorna o objeto service
   */
  abstract getService(): S;

  /**
   * Retorna uma instancia vazia do VO
   */
  abstract getVoNewInstance(): T;

  salvar(voltar: boolean = true, showSuccessMessage: boolean = true, callbackSuccess: () => any = undefined) {

    if (!this.beforeSalvar(this.vo)) {
      return;
    }

    this.bloquear();

    if (this.id && this.acao === 'editar') {
      const s: Subscription = this.getService().atualizar(this.vo, this.id).subscribe((retorno) => {

        this.desbloquear();

        if (retorno.success) {

          this.beforeVisualizar(retorno.vo);

          this.vo = retorno.vo;

          this.afterVisualizar(this.vo);

          if (showSuccessMessage) {
            this._msg.nofiticationInfo('Registro alterado com sucesso!');
          }

          if (voltar) {
            this.voltar();
          }
          if (callbackSuccess) {
            callbackSuccess();
          }
        } else {
          this._msg.warn(Utils.buildErrorMessage(retorno)).then(() => {
            if (retorno.errors && retorno.errors.length > 0) {
              $(this.getFieldName(retorno.errors[0].fieldName)).focus();
            }
          });
        }
      }, (error) => {
        this.desbloquear();

        this._msg.warn(Utils.buildErrorMessage(error)).then(() => {
          if (error.errors && error.errors.length > 0) {
            $(this.getFieldName(error.errors[0].fieldName)).focus();
          }
        });
      });

      this.inscricoes.push(s);

    } else if (this.acao === 'novo') {
      const s: Subscription = this.getService().inserir(this.vo)
        .subscribe((retorno) => {
          this.desbloquear();

          if (retorno.success) {
            this.beforeVisualizar(retorno.vo);

            this.vo = retorno.vo;

            this.afterVisualizar(this.vo);

            if (showSuccessMessage) {
              this._msg.nofiticationInfo('Registro incluído com sucesso!');
            }
            if (voltar) {
              this.voltar();
            } else {
              // Se nao vai voltar, tem que redirecionar para edicao
              let vo: any = retorno.vo;
              this._router.navigate([this._route.parent.parent.routeConfig.path, 'editar', vo.id]);
            }

            if (callbackSuccess) {
              callbackSuccess();
            }
          } else {
            this._msg.warn(Utils.buildErrorMessage(retorno)).then(() => {
              if (retorno.errors && retorno.errors.length > 0) {
                $(this.getFieldName(retorno.errors[0].fieldName)).focus();
              }
            });
          }
        }, (error) => {
          this.desbloquear();

          this._msg.warn(Utils.buildErrorMessage(error)).then(() => {
            if (error.errors && error.errors.length > 0) {
              $(this.getFieldName(error.errors[0].fieldName)).focus();
            }
          });
        });
      this.inscricoes.push(s);
    }
  }

  excluir() {
    this._msg.confirm('Confirma a EXCLUSÃO deste registro?').then(() => {
      const s: Subscription = this.getService().excluir(this.id).subscribe((retorno) => {
        if (retorno.success) {
          this._msg.nofiticationInfo('Registro excluído com sucesso!');
          this.voltar();
        } else {
          this._msg.warn(Utils.buildErrorMessage(retorno)).then(() => {
            if (retorno.errors && retorno.errors.length > 0) {
              $(this.getFieldName(retorno.errors[0].fieldName)).focus();
            }
          });
        }
      }, (retorno) => {
        this._msg.warn(Utils.buildErrorMessage(retorno)).then(() => {
          if (retorno.errors && retorno.errors.length > 0) {
            $(this.getFieldName(retorno.errors[0].fieldName)).focus();
          }
        });
      });
      this.inscricoes.push(s);
    }).catch(() => {
      // Do nothing.
    });
  }

  voltar() {
    // console.log(this._route.parent.parent.snapshot.toString());
    this._router.navigate([this._route.parent.parent.routeConfig.path]);
  }

  ngOnDestroy() {
    // Remove as inscricoes
    for (const s of this.inscricoes) {
      s.unsubscribe();
    }
  }

  getFieldName(fieldName: string): any {
    if (fieldName) {
      return '#internal_' + fieldName.replace(".", "_");
    } else {
      return '#';
    }
  }
}
