import { ConfiguracaoAprovacaoViagemModule } from './modules/configuracoes/configuracao-aprovacao-viagem/configuracao-aprovacao-viagem.module';
import { MensagemModule } from './modules/geral/mensagem/mensagem.module';
import { CentroCustoModule } from './modules/cadastros/centro-custo/centro-custo.module';
import { RelatoriosModule } from './modules/relatorios/relatorios.module';
import { SetorModule } from './modules/configuracoes/setor/setor.module';
import { AdiantamentoModule } from './modules/financeiro/adiantamento/adiantamento.module';
import { AprovacaoContasModule } from './modules/financeiro/aprovacao-contas/aprovacao-contas.module';
import { LancamentoModule } from './modules/financeiro/lancamento/lancamento.module';
import { LOCALE_ID } from '@angular/core';
import { FechamentoModule } from './modules/colaborador/fechamento/fechamento.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/geral/header/header.component';
import { FooterComponent } from './components/geral/footer/footer.component';
import { SideBarComponent } from './components/geral/side-bar/side-bar.component';
import { HttpModule } from '@angular/http';
import { ViagemModule } from './modules/financeiro/viagem/viagem.module';
import { AcessoModule } from './modules/acesso/acesso/acesso.module';
import { PerfilModule } from './modules/configuracoes/perfil/perfil.module';
import { PrestacaoContasModule } from './modules/colaborador/prestacao-contas/prestacao-contas.module';
import { MinhasViagensModule } from './modules/colaborador/minhas-viagens/minhas-viagens.module';
import { EmpresaModule } from './modules/configuracoes/empresa/empresa.module';
import { PoliticaModule } from './modules/cadastros/politica/politica.module';
import { TituloPaginaService } from './components/services/titulo-pagina.service';
import { DeviceListService } from './plugins/camera/device-list.service';
import { TirarFotoService } from './plugins/camera/tirar-foto.service';
import { CategoriaModule } from './modules/configuracoes/categoria/categoria.module';
import { ReciboModule } from './modules/financeiro/recibo/recibo.module';
import { ProjetoModule } from './modules/cadastros/projeto/projeto.module';
import { ClienteModule } from './modules/cadastros/cliente/cliente.module';
import { LogoffComponent } from './components/geral/logoff/logoff.component';
import { FormsModule } from '@angular/forms';
import { PluginsModule } from './plugins/plugins.module';
import { LoginComponent } from './modules/geral/login/login.component';
import { IndexComponent } from './modules/geral/index/index.component';
import { FuncionarioModule } from './modules/cadastros/funcionario/funcionario.module';
import { LoginService } from './security/login.service';
import { AutenticacaoGuard } from './security/autenticacao.guard';
import { FuncaoModule } from './modules/cadastros/funcao/funcao.module';
import { AppRoutingRoutingModule } from './app.routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MeusDadosModule } from './modules/meus-dados/meus-dados.module';
import { MultaModule } from './modules/financeiro/multa/multa.module';

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    LoginComponent,
    LogoffComponent],
  imports: [
    BrowserModule,
    PluginsModule,
    FormsModule,
    HttpModule,
    AppRoutingRoutingModule,
    FuncaoModule,
    FuncionarioModule,
    ClienteModule,
    ProjetoModule,
    ViagemModule,
    ReciboModule,
    CategoriaModule,
    AcessoModule,
    PerfilModule,
    PoliticaModule,
    EmpresaModule,
    MinhasViagensModule,
    AdiantamentoModule,
    AprovacaoContasModule,
    ReciboModule,
    MeusDadosModule,
    SetorModule,
    CentroCustoModule,
    RelatoriosModule,
    MensagemModule,
    MultaModule,
    ConfiguracaoAprovacaoViagemModule
  ],
  providers: [
    AutenticacaoGuard,
    LoginService,
    TirarFotoService,
    DeviceListService,
    TituloPaginaService,
    { provide: LOCALE_ID, useValue: 'pt-BR' }],
  bootstrap: [AppComponent]
})
export class AppModule { }
